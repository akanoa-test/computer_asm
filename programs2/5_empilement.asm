// initialisation du pointeur de pile
MOV SP, 0
// a = 42
MOV R1, 42
// stockage de "a" à l'adresse pointé par la pile
STD R1, @SP
// incrémentation de SP
MOV Acc, SP
ADD 1
MOV SP, Acc

// b = 66
MOV R1, 66
// stockage de "b" à l'adresse pointé par la pile
STD R1, @SP
// incrémentation de SP
MOV Acc, SP
ADD 1
MOV SP, Acc

NOP
STP