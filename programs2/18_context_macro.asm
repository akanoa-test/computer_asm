// a = 1
MOV R1, 1
// b = 1
MOV R2, 2
// x = a
MOV R5, R1
// y = b
MOV R6, R2
// add(x, y) avec suvegarde du contexte
CALL 'add, R2
// c = add(x, y)
MOV R3, R7
// y = c
MOV R6, R3
// add(x, y)
CALL 'add, R2
// c = add(x, y)
MOV R3, R7
// y = b
MOV R6, R2
// add(x, y)
CALL 'add, R2
// a = add(x, y)
MOV R1, R7
NOP
STP
// fn add(x, y)
add: MOV R7, R5
// bruit
ADD2 R2, 2
ADD2 R7, R6
RET