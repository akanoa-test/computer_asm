// a = 1
MOV R1, 1
// b  = 2
MOV R2, 2
// persistence de l'adresse de retour
PUSH IP
// add(a, b)
JMP 'add
// c =, add(a, c)
MOV R3, Acc
// persistence de b
PUSH R2
// y = c
MOV R2, R3
// persistence de l'adresse de retour
PUSH IP
// add(a, c)
JMP 'add
// c = add(a, c)
MOV R3, Acc
// restauration de b
POP R2
// persistence de l'adresse de retour
PUSH IP
// add(a, b)
JMP 'add
// a = add(a, b)
MOV R1, Acc
NOP
STP
// fn add(x, y)
add: MOV Acc, R1
ADD R2
POP R7
ADD2 R7, 2
JMP @R7