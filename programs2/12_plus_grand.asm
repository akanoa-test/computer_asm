// a = 42
MOV R1, 42
// b = 12
MOV R2, 12
// comparaison a != b
MOV Acc, R1
SUB R2
JMN @6
// dans le bloc : a = b
MOV R1, R2
// fin de bloc
NOP
STP
