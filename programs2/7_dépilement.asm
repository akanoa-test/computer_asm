// a = 42
MOV R1, 42
// empilement de "a"
PUSH R1
// b = 66
MOV R1, 66
// empilement de "b"
PUSH R1

// décrémentation de SP
MOV Acc, SP
SUB 1
MOV SP, Acc
// dépilement de "b" dans R1
LOD R1, @SP

// décrémentation de SP
MOV Acc, SP
SUB 1
MOV SP, Acc
// dépilement de "a" dans R2
LOD R2, @SP

NOP
STP