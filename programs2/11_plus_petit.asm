// a = 12
MOV R1, 12
// b = 42
MOV R2, 42
// comparaison a != b
MOV Acc, R1
SUB R2
JMN @6
// dans le bloc : a = b
MOV R1, R2
// fin de bloc
NOP
STP
