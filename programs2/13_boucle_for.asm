// x = 5
MOV R1, 5
// end = 2
MOV R2, 2
// i = 0
MOV R3, 0
// i < 2
MOV Acc, R2
SUB R3
JMZ @9
// x+= x
ADD2 R1, R1
// i++
ADD2 R3, 1
// fin de la boucle
JMP @3
// hors de la boucle
NOP
STP
