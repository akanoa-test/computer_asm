NOP
NOP
// allocate 5 blocks => ptr1
ALLOC R1, 5
ALLOC R2, 10
// store at ptr1 a[0] = 0x1
MOV R3, 0x1
STDH R3, @R1
// store at ptr1 + 1 a[1] = 0x2
MOV R3, 0x2
STDH R3, @{R1 + 1}
// free ptr1
FREE @R1
NOP
NOP
// allocate 3 blocks => ptr2 == ptr1
ALLOC R1, 3
// store at ptr1 a[0] = 0x4
MOV R3, 0x4
STDH R3, @R1
// store at ptr1 + 2 a[2] = 0x6
MOV R3, 0x6
STDH R3, @{R1 + 2}
FREE @R1
FREE @R2
ALLOC R1, 11
NOP
NOP
STP