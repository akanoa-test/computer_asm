/*
fn main() {
   arr = [1, 2, 3]
   f(&arr)
}
fn f(&arr) {
   arr[1] = 5
}
*/
    MOV R1, 1
    PUSH R1

    MOV R10, {SP - 1}

    MOV R1, 2
    PUSH R1     // @{R10 + 1} <=> arr[1]

    MOV R1, 3
    PUSH R1
    MOV R8, 1

    CALL 'f
    LOD R4, @{R10 + 1}
    STP
f:  MOV R2, 5              //  tmp = 5
    STD R2, @{R10 + 1}     //  arr[1] = tmp <=> arr[1} = 5
    RET