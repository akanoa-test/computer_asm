// a = x =  1
MOV R1, 1
// b = y = 2
MOV R2, 2
// add(x, y)
CALL 'add
// c = add(a, b)
MOV R3, Acc
// préservation de b
PUSH R2
// y = c
MOV R2, R3
// add(a, c)
CALL 'add
// c = add(a, c)
MOV R3, Acc
// Restauration de b
POP R2
// add(a, b)
CALL 'add
// a = add(a, b)
MOV R1, Acc
NOP
STP
// fn add(x, y)
add: MOV Acc, R1
ADD R2
RET