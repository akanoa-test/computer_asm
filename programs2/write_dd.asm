/*
    write 0xaaa1 at address @158 in write mode High Bits
*/
    MOV R2, 158
    OUT R2, #0x00_01
    MOV R2, 0xaaa1
    OUT R2, #0x00_02
    MOV R2, 0b111
    OUT R2, #0x00_00
    STP
