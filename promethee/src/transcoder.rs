use crate::data::constants::{
    RamDataBusSize, MASK_DATUM, MASK_VALUE, MODIFIER_INDEX, OPCODE_INDEX, REGISTER_INDEX,
};
use crate::data::datum::{AddressDatum, ArithmeticDatum, InstructionDatum, MoveDatum};
use crate::data::instruction::Instruction;
use crate::data::OpCode;
use crate::mmu::Section;

macro_rules! build_binary {
    ($opcode: expr, $register:expr, $value:expr, $modifier:expr) => {
        (($opcode as RamDataBusSize) << OPCODE_INDEX)
            | ((*$register as RamDataBusSize) << REGISTER_INDEX)
            | (($modifier as RamDataBusSize) << MODIFIER_INDEX)
            | ((*$value as RamDataBusSize) & MASK_DATUM)
    };
}

impl Instruction {
    pub(crate) fn to_binary(&self) -> u32 {
        match self {
            Instruction::Move {
                register,
                datum: source,
            } => {
                ((OpCode::Move as u32) << OPCODE_INDEX)
                    | ((*register as u32) << REGISTER_INDEX)
                    | (source.to_binary() & MASK_DATUM)
            }
            Instruction::Load {
                datum,
                register,
                section,
            } => {
                let opcode = match section {
                    Section::Stack => OpCode::Load,
                    Section::Heap => OpCode::LoadHeap,
                    _ => unimplemented!("Loading for section {section:?} is not yet implemented"),
                };

                match datum {
                    AddressDatum::Address(address) => {
                        build_binary!(opcode, register, address, Modifier::Address)
                    }
                    AddressDatum::Register(register_datum) => {
                        build_binary!(opcode, register, register_datum, Modifier::Register)
                    }
                }
            }
            Instruction::Store {
                register,
                datum,
                section,
            } => {
                let opcode = match section {
                    Section::Stack => OpCode::Store,
                    Section::Heap => OpCode::StoreHeap,
                    _ => unimplemented!("Storing for section {section:?} is not yet implemented"),
                };

                match datum {
                    AddressDatum::Address(address) => {
                        build_binary!(opcode, register, address, Modifier::Address)
                    }
                    AddressDatum::Register(register_datum) => {
                        build_binary!(opcode, register, register_datum, Modifier::Register)
                    }
                }
            }
            Instruction::Add { datum } => match datum {
                ArithmeticDatum::Register(register) => {
                    build_binary!(OpCode::Add, &0, register, Modifier::Register)
                }
                ArithmeticDatum::Data(value) => {
                    build_binary!(OpCode::Add, &0, value, Modifier::Data)
                }
            },
            Instruction::Add2 {
                datum,
                register: dest_register,
            } => match datum {
                ArithmeticDatum::Register(register) => {
                    build_binary!(OpCode::Add2, dest_register, register, Modifier::Register)
                }
                ArithmeticDatum::Data(value) => {
                    build_binary!(OpCode::Add2, dest_register, value, Modifier::Data)
                }
            },
            Instruction::Sub { datum } => match datum {
                ArithmeticDatum::Register(register) => {
                    build_binary!(OpCode::Sub, &0, register, Modifier::Register)
                }
                ArithmeticDatum::Data(value) => {
                    build_binary!(OpCode::Sub, &0, value, Modifier::Data)
                }
            },
            Instruction::Sub2 {
                datum,
                register: dest_register,
            } => match datum {
                ArithmeticDatum::Register(register) => {
                    build_binary!(OpCode::Sub2, dest_register, register, Modifier::Register)
                }
                ArithmeticDatum::Data(value) => {
                    build_binary!(OpCode::Sub2, dest_register, value, Modifier::Data)
                }
            },
            Instruction::Output { register, bus } => {
                let value: u16 = bus.into();
                build_binary!(OpCode::Output, register, &value, Modifier::Lane)
            }
            Instruction::Jump { address } => match address {
                AddressDatum::Address(address) => {
                    build_binary!(OpCode::Jump, &0, address, Modifier::Address)
                }
                AddressDatum::Register(register) => {
                    build_binary!(OpCode::Jump, &0, register, Modifier::Register)
                }
            },
            Instruction::JumpNonZero { address } => match address {
                AddressDatum::Address(address) => {
                    build_binary!(OpCode::JumpNonZero, &0, address, Modifier::Address)
                }
                AddressDatum::Register(register) => {
                    build_binary!(OpCode::JumpNonZero, &0, register, Modifier::Register)
                }
            },
            Instruction::JumpZero { address } => match address {
                AddressDatum::Address(address) => {
                    build_binary!(OpCode::JumpZero, &0, address, Modifier::Address)
                }
                AddressDatum::Register(register) => {
                    build_binary!(OpCode::JumpZero, &0, register, Modifier::Register)
                }
            },
            Instruction::JumpNegative { address } => match address {
                AddressDatum::Address(address) => {
                    build_binary!(OpCode::JumpNegative, &0, address, Modifier::Address)
                }
                AddressDatum::Register(register) => {
                    build_binary!(OpCode::JumpNegative, &0, register, Modifier::Register)
                }
            },
            Instruction::Push { register } => {
                build_binary!(OpCode::Push, register, &0, Modifier::Register)
            }
            Instruction::Pop { register } => {
                build_binary!(OpCode::Pop, register, &0, Modifier::Register)
            }
            Instruction::Allocate { amount, register } => {
                build_binary!(OpCode::Allocate, register, amount, Modifier::Data)
            }
            Instruction::Reallocate { amount, register } => {
                build_binary!(OpCode::Reallocate, register, amount, Modifier::Data)
            }
            Instruction::Free { address } => match address {
                AddressDatum::Address(address) => {
                    build_binary!(OpCode::Free, &0, address, Modifier::Data)
                }
                AddressDatum::Register(register) => {
                    build_binary!(OpCode::Free, &0, register, Modifier::Register)
                }
            },
            Instruction::Call { address } => match address {
                AddressDatum::Address(address) => {
                    build_binary!(OpCode::Call, &0, address, Modifier::Address)
                }
                AddressDatum::Register(register) => {
                    build_binary!(OpCode::Call, &0, register, Modifier::Register)
                }
            },
            Instruction::Return => (OpCode::Return as u32) << OPCODE_INDEX,
            Instruction::Halt => (OpCode::Halt as u32) << OPCODE_INDEX,
            Instruction::Stop => (OpCode::Stop as u32) << OPCODE_INDEX,
            Instruction::Nop => (OpCode::Nop as u32) << OPCODE_INDEX,
        }
    }
}

#[repr(u8)]
enum Modifier {
    Register = 0,
    Data,
    Address,
    Lane,
}

impl InstructionDatum {
    fn to_binary(&self) -> RamDataBusSize {
        match self {
            InstructionDatum::Register(register) => *register as RamDataBusSize,
            InstructionDatum::Address(address) => {
                ((Modifier::Address as RamDataBusSize) << MODIFIER_INDEX)
                    | ((*address as RamDataBusSize) & MASK_VALUE)
            }
            InstructionDatum::Bus(device_id) => {
                ((Modifier::Lane as RamDataBusSize) << MODIFIER_INDEX)
                    | ((*device_id as RamDataBusSize) & MASK_VALUE)
            }
            InstructionDatum::Data(value) => {
                ((Modifier::Data as RamDataBusSize) << MODIFIER_INDEX)
                    | ((*value as RamDataBusSize) & MASK_VALUE)
            }
        }
    }
}

impl MoveDatum {
    fn to_binary(&self) -> RamDataBusSize {
        match self {
            MoveDatum::Register(register) => *register as RamDataBusSize,
            MoveDatum::Value(value) => {
                ((Modifier::Data as RamDataBusSize) << MODIFIER_INDEX)
                    | ((*value as RamDataBusSize) & MASK_VALUE)
            }
        }
    }
}

pub fn to_binary(instructions: &[Instruction]) -> Vec<RamDataBusSize> {
    instructions
        .into_iter()
        .map(|instruction| instruction.to_binary())
        .collect()
}

/**

OpCode | Register | Modifier | Value

Modifier
00 => Register
01 => Value
10 => Address
11 => Device

// Opcode

MOV  => 0000 0000
LOD  => 0000 0001
STD  => 0000 0010
ADD  => 0000 0011
JMP  => 0000 0100
JNZ  => 0000 0101
OUT  => 0000 0110
PUSH => 0000 0111
POP  => 0000 1000
CALL => 0000 1001
RET  => 0000 1010
HLT  => 1111 1111

                       24     18    16
                                   |
MOV ACC, 1   => 00000000 000000 01 0000000000000001
MOV R1, 4    => 00000000 000001 01 0000000000000100
ADD R1       => 00000011 000001 00 0000000000000000
STD ACC, @15 => 00000010 000000 10 0000000000001111
LOD R1, @15  => 00000001 000001 10 0000000000001111
MOV R2, R1   => 00000000 000010 00 0000000000000001
JMP @15      => 00000100 000000 10 0000000000001111
JNZ R1, @15  => 00000101 000001 10 0000000000001111
OUT R2, #12  => 00000110 000010 11 0000000000001100
PUSH R1      => 00000111 000001 00 0000000000000000
PUSH R1      => 00001000 000001 00 0000000000000000
CALL @7      => 00001001 000000 10 0000000000000111
RET          => 00001010 000000 00 0000000000000000
HLT          => 11111111 000000 00 0000000000000000
 */

#[cfg(test)]
mod tests {
    #![allow(clippy::unusual_byte_groupings)]

    use crate::cpu::{Bus, Lane};
    use crate::data::datum::{AddressDatum, ArithmeticDatum, InstructionDatum, MoveDatum};
    use crate::data::instruction::Instruction;
    use crate::data::register::Register;
    use crate::mmu::Section;

    #[test]
    fn direct_value_to_binary() {
        let expected = 0b0001_0000_0000_0000_0001;
        let result = InstructionDatum::Data(1).to_binary();
        assert_eq!(expected, result);

        let expected = 0b0001_0000_0000_0000_0100;
        let result = InstructionDatum::Data(4).to_binary();
        assert_eq!(expected, result);
    }

    #[test]
    fn mov_to_binary() {
        let instruction = Instruction::Move {
            register: Register::Acc,
            datum: MoveDatum::Value(1),
        };
        let expected = 0b00000000_000000_01_0000000000000001;

        assert_eq!(instruction.to_binary(), expected);

        let instruction = Instruction::Move {
            register: Register::R1,
            datum: MoveDatum::Value(4),
        };
        let expected = 0b00000000_000001_01_0000000000000100;

        assert_eq!(instruction.to_binary(), expected);

        let instruction = Instruction::Move {
            register: Register::R1,
            datum: MoveDatum::Register(Register::R2),
        };
        let expected = 0b00000000_000001_00_0000000000000010;

        assert_eq!(instruction.to_binary(), expected);
    }

    #[test]
    fn load_to_binary() {
        let instruction = Instruction::Load {
            register: Register::Acc,
            datum: AddressDatum::Address(15),
            section: Section::Stack,
        };
        let expected = 0b00000001_000000_10_0000000000001111;
        assert_eq!(instruction.to_binary(), expected);

        let instruction = Instruction::Load {
            register: Register::Acc,
            datum: AddressDatum::Address(15),
            section: Section::Heap,
        };
        let expected = 0b00010000_000000_10_0000000000001111;
        assert_eq!(instruction.to_binary(), expected);

        let instruction = Instruction::Load {
            register: Register::R1,
            datum: AddressDatum::Address(15),
            section: Section::Stack,
        };
        let expected = 0b00000001_000001_10_0000000000001111;
        assert_eq!(instruction.to_binary(), expected);
    }

    #[test]
    fn store_to_binary() {
        let instruction = Instruction::Store {
            register: Register::Acc,
            datum: AddressDatum::Address(15),
            section: Section::Stack,
        };
        let expected = 0b00000010_000000_10_0000000000001111;
        assert_eq!(instruction.to_binary(), expected);

        let instruction = Instruction::Store {
            register: Register::Acc,
            datum: AddressDatum::Address(15),
            section: Section::Heap,
        };
        let expected = 0b00010001_000000_10_0000000000001111;
        assert_eq!(instruction.to_binary(), expected);

        let instruction = Instruction::Store {
            register: Register::R1,
            datum: AddressDatum::Address(15),
            section: Section::Stack,
        };
        let expected = 0b00000010_000001_10_0000000000001111;
        assert_eq!(instruction.to_binary(), expected);
    }

    #[test]
    fn add_to_binary() {
        let instruction = Instruction::Add {
            datum: ArithmeticDatum::Register(Register::Acc),
        };
        let expected = 0b00000011_000000_00_0000000000000000;
        assert_eq!(instruction.to_binary(), expected);

        let instruction = Instruction::Add {
            datum: ArithmeticDatum::Register(Register::R1),
        };
        let expected = 0b00000011_000000_00_0000000000000001;
        assert_eq!(instruction.to_binary(), expected);

        let instruction = Instruction::Add {
            datum: ArithmeticDatum::Data(3),
        };
        let expected = 0b00000011_000000_01_0000000000000011;
        assert_eq!(instruction.to_binary(), expected);
    }

    #[test]
    fn sub_to_binary() {
        let instruction = Instruction::Sub {
            datum: ArithmeticDatum::Register(Register::Acc),
        };
        let expected = 0b00001011_000000_00_0000000000000000;
        assert_eq!(instruction.to_binary(), expected);

        let instruction = Instruction::Sub {
            datum: ArithmeticDatum::Register(Register::R1),
        };
        let expected = 0b00001011_000000_00_0000000000000001;
        assert_eq!(instruction.to_binary(), expected);

        let instruction = Instruction::Sub {
            datum: ArithmeticDatum::Data(3),
        };
        let expected = 0b00001011_000000_01_0000000000000011;
        assert_eq!(instruction.to_binary(), expected);
    }

    #[test]
    fn jmp_to_binary() {
        let instruction = Instruction::Jump {
            address: AddressDatum::Address(15),
        };
        let expected = 0b00000100_0000_0010_0000000000001111;
        assert_eq!(instruction.to_binary(), expected);

        let instruction = Instruction::Jump {
            address: AddressDatum::Register(Register::R1),
        };
        let expected = 0b00000100_0000_0000_0000000000000001;
        assert_eq!(
            instruction.to_binary(),
            expected,
            "{expected:032b} <=> {:032b}",
            instruction.to_binary()
        );

        let instruction = Instruction::Jump {
            address: AddressDatum::Address(15),
        };
        let expected = 0b00000100_0000_0010_0000000000001111;
        assert_eq!(
            instruction.to_binary(),
            expected,
            "{expected:032b} <=> {:032b}",
            instruction.to_binary()
        );
    }

    #[test]
    fn jnz_to_binary() {
        let instruction = Instruction::JumpNonZero {
            address: AddressDatum::Address(15),
        };
        let expected = 0b00000101_000000_10_0000000000001111;
        assert_eq!(instruction.to_binary(), expected, "1");

        let instruction = Instruction::JumpNonZero {
            address: AddressDatum::Address(15),
        };
        let expected = 0b00000101_000000_10_0000000000001111;
        assert_eq!(instruction.to_binary(), expected, "2");
    }

    #[test]
    fn output_to_binary() {
        let instruction = Instruction::Output {
            register: Register::Acc,
            bus: Bus(Lane::Lane1, 4),
        };

        let expected = 0b00000110_000000_11_00000001_00000100;

        assert_eq!(instruction.to_binary(), expected);
    }
}
