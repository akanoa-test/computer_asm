use crate::cpu::CpuError;
use crate::data::constants::*;
use crate::data::register::Register;
use num_enum::TryFromPrimitive;

#[derive(TryFromPrimitive)]
#[repr(u8)]
enum DatumKind {
    Register = 0,
    Data,
    Address,
    Device,
}

#[derive(Debug, PartialEq)]
pub enum InstructionDatum {
    Register(Register),
    Address(ValueSize),
    Bus(ValueSize),
    Data(ValueSize),
}

#[derive(Debug, PartialEq)]
pub enum ArithmeticDatum {
    Register(Register),
    Data(ValueSize),
}

#[derive(Debug, PartialEq)]
pub enum AddressDatum {
    Address(RamAddressBusSize),
    Register(Register),
}

#[derive(Debug, PartialEq)]
pub enum MoveDatum {
    Value(ValueSize),
    Register(Register),
}

impl TryFrom<&RamDataBusSize> for InstructionDatum {
    type Error = CpuError;

    // 00000000 0000 0001 0000000000000001

    fn try_from(value: &RamDataBusSize) -> Result<Self, Self::Error> {
        //dbg!(format!("{:024b}", value));

        let modifier = ((value >> MODIFIER_INDEX) & MASK_VALUE) as ModifierSize;
        let data = (value & MASK_VALUE) as ValueSize;

        let datum = match DatumKind::try_from(modifier).map_err(|_| CpuError::Transcription)? {
            DatumKind::Register => {
                let register = Register::try_from(data as RegisterSize)
                    .map_err(|_| CpuError::Transcription)?;
                InstructionDatum::Register(register)
            }
            DatumKind::Data => InstructionDatum::Data(data),
            DatumKind::Address => InstructionDatum::Address(data),
            DatumKind::Device => InstructionDatum::Bus(data),
        };
        Ok(datum)
    }
}

impl From<InstructionDatum> for ArithmeticDatum {
    fn from(value: InstructionDatum) -> Self {
        match value {
            InstructionDatum::Register(register) => ArithmeticDatum::Register(register),
            InstructionDatum::Data(value) => ArithmeticDatum::Data(value),
            InstructionDatum::Address(_) | InstructionDatum::Bus(_) => {
                unreachable!("Can't be convert to Arithmetic {:?}", value)
            }
        }
    }
}

impl From<InstructionDatum> for AddressDatum {
    fn from(value: InstructionDatum) -> Self {
        match value {
            InstructionDatum::Register(register) => AddressDatum::Register(register),
            InstructionDatum::Address(value) => AddressDatum::Address(value),
            InstructionDatum::Data(_) | InstructionDatum::Bus(_) => {
                unreachable!("Can't be convert to Arithmetic {:?}", value)
            }
        }
    }
}

impl From<InstructionDatum> for MoveDatum {
    fn from(value: InstructionDatum) -> Self {
        match value {
            InstructionDatum::Register(register) => MoveDatum::Register(register),
            InstructionDatum::Data(data) => MoveDatum::Value(data),
            _ => unreachable!("Can't be convert to Move Datum {:?}", value),
        }
    }
}

impl ToString for InstructionDatum {
    fn to_string(&self) -> String {
        match self {
            InstructionDatum::Register(register) => register.to_string(),
            InstructionDatum::Address(address) => format!("@{address}"),
            InstructionDatum::Bus(bus_id) => format!("#{bus_id}"),
            InstructionDatum::Data(data) => data.to_string(),
        }
    }
}

impl ToString for AddressDatum {
    fn to_string(&self) -> String {
        match self {
            AddressDatum::Register(register) => register.to_string(),
            AddressDatum::Address(address) => format!("@{address}"),
        }
    }
}

impl ToString for ArithmeticDatum {
    fn to_string(&self) -> String {
        match self {
            ArithmeticDatum::Register(register) => register.to_string(),
            ArithmeticDatum::Data(data) => data.to_string(),
        }
    }
}

impl ToString for MoveDatum {
    fn to_string(&self) -> String {
        match self {
            MoveDatum::Register(register) => register.to_string(),
            MoveDatum::Value(data) => data.to_string(),
        }
    }
}
