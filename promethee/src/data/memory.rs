use std::fmt::{Debug, Display, Formatter};

use serde::Serialize;

use crate::data::constants::{RamAddressBusSize, RamDataBusSize};

#[derive(Serialize, Clone, Debug, PartialEq)]
pub struct HeapBlock {
    pub(crate) id: usize,
    pub(crate) state: bool,
    pub(crate) size: usize,
    pub(crate) addresses: Vec<HeapAddress>,
}

#[derive(Serialize, Clone, PartialEq)]
pub struct HeapAddress {
    pub(crate) address: RamAddressBusSize,
    pub(crate) data: RamDataBusSize,
}

pub fn format_data(data: &RamDataBusSize) -> String {
    let byte0 = data >> 24;
    let byte1 = data >> 16 & 0xff;
    let byte2 = data >> 8 & 0xff;
    let byte3 = data & 0xff;
    format!("{:08b} {:08b} {:08b} {:08b}", byte0, byte1, byte2, byte3)
}

impl Display for HeapAddress {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            r#"HeapAddress {{ 
             address: {},
             data: {}
        }}"#,
            self.address,
            format_data(&self.data)
        )
    }
}

impl Debug for HeapAddress {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{self}")
    }
}

#[derive(Clone, Debug, PartialEq, Default)]
pub struct Header {
    pub(crate) size: u32,
    pub(crate) state: bool,
}

impl From<Header> for RamDataBusSize {
    fn from(value: Header) -> Self {
        value.size << 1 | (value.state as u32 & 1)
    }
}

impl From<RamDataBusSize> for Header {
    fn from(value: RamDataBusSize) -> Self {
        Self {
            size: value >> 1,
            state: value & 1 == 1,
        }
    }
}

impl From<&RamDataBusSize> for Header {
    fn from(value: &RamDataBusSize) -> Self {
        Self {
            size: value >> 1,
            state: value & 1 == 1,
        }
    }
}
