use num_enum::TryFromPrimitive;
use strum::Display;

pub mod constants;
pub mod datum;
pub mod instruction;
pub(crate) mod register;

pub mod memory;

/**
// Opcode

MOV  => 0000 0000
LOD  => 0000 0001
STD  => 0000 0010
ADD  => 0000 0011
JMP  => 0000 0100
JNZ  => 0000 0101
OUT  => 0000 0110
PUSH => 0000 0111
POP  => 0000 1000
CALL => 0000 1001
RET  => 0000 1010
HLT  => 1111 1111
 */
#[derive(TryFromPrimitive, PartialEq, Debug, Display)]
#[repr(u8)]
pub enum OpCode {
    Move = 0,
    #[strum(serialize = "LOD")]
    Load,
    #[strum(serialize = "STD")]
    Store,
    Add,
    Jump,
    JumpNonZero,
    Output,
    Push,
    Pop,
    Call,
    Return,
    Sub,
    JumpZero,
    Stop,
    Allocate,
    Free,
    #[strum(serialize = "LODH")]
    LoadHeap,
    #[strum(serialize = "STDH")]
    StoreHeap,
    Nop,
    Add2,
    Sub2,
    #[strum(serialize = "JMN")]
    JumpNegative,
    #[strum(serialize = "REALLOC")]
    Reallocate,
    Halt = 0b11111111,
}

#[macro_export]
macro_rules! get_operands {
    ($value: expr) => {{
        let register = ($value >> REGISTER_INDEX) & MASK_REGISTER;
        let register =
            Register::try_from(register as RegisterSize).map_err(|_| CpuError::Transcription)?;
        let datum = $value & MASK_DATUM;
        let datum = InstructionDatum::try_from(&(datum as RamDataBusSize))?;
        (register, datum)
    }};
}

#[macro_export]
macro_rules! get_address {
    ($datum: expr) => {{
        if let InstructionDatum::Address(address) = $datum {
            address
        } else {
            Err(CpuError::Transcription)?
        }
    }};
}

#[macro_export]
macro_rules! get_bus {
    ($datum: expr) => {{
        let bus = if let InstructionDatum::Bus(lane_id) = $datum {
            Bus::try_from(&lane_id).map_err(|_| CpuError::Transcription)?
        } else {
            Err(CpuError::Transcription)?
        };
        bus
    }};
}

#[cfg(test)]
mod tests {
    use crate::cpu::{Bus, CpuError, Lane};
    use crate::data::datum::{AddressDatum, ArithmeticDatum, InstructionDatum, MoveDatum};
    use crate::data::instruction::Instruction;
    use crate::data::register::Register;
    use crate::mmu::Section;
    use crate::parser::parse_program;
    use crate::transcoder::to_binary;

    #[test]
    fn datum_from_u32() {
        // Registers
        let bytes = &0x0_0000_u32;
        let expected = InstructionDatum::Register(Register::Acc);
        let datum: InstructionDatum = bytes.try_into().expect("");
        assert_eq!(expected, datum);

        let bytes = &0x0_0001_u32;
        let expected = InstructionDatum::Register(Register::R1);
        let datum: InstructionDatum = bytes.try_into().expect("");
        assert_eq!(expected, datum);

        let bytes = &0x0_00aa_u32;
        let datum: Result<InstructionDatum, CpuError> = bytes.try_into();
        assert!(datum.is_err());

        // Data
        let bytes = &0x1_0000_u32;
        let expected = InstructionDatum::Data(0);
        let datum: InstructionDatum = bytes.try_into().expect("");
        assert_eq!(expected, datum);

        let bytes = &0x1_0001_u32;
        let expected = InstructionDatum::Data(1);
        let datum: InstructionDatum = bytes.try_into().expect("");
        assert_eq!(expected, datum);

        let bytes = &0x1_000f_u32;
        let expected = InstructionDatum::Data(15);
        let datum: InstructionDatum = bytes.try_into().expect("");
        assert_eq!(expected, datum);

        let bytes = &0x1_aaaa_u32;
        let expected = InstructionDatum::Data(0xaaaa);
        let datum: InstructionDatum = bytes.try_into().expect("");
        assert_eq!(expected, datum);

        // Adresses
        let bytes = &0x2_0000_u32;
        let expected = InstructionDatum::Address(0);
        let datum: InstructionDatum = bytes.try_into().expect("");
        assert_eq!(expected, datum);

        let bytes = &0x2_000f_u32;
        let expected = InstructionDatum::Address(15);
        let datum: InstructionDatum = bytes.try_into().expect("");
        assert_eq!(expected, datum);

        // Device
        let bytes = &0x3_000c_u32;
        let expected = InstructionDatum::Bus(12);
        let datum: InstructionDatum = bytes.try_into().expect("");
        assert_eq!(expected, datum);
    }

    macro_rules! test_decode {
        ($instruction: literal, $expected: expr) => {{
            let instructions = parse_program($instruction).expect("to parse instruction");
            let instruction_bin = to_binary(&instructions)
                .into_iter()
                .next()
                .expect("one instruction");
            let result: Instruction = (&instruction_bin)
                .try_into()
                .expect("to convert binary representation");
            assert_eq!($expected, result);
        }};
    }

    #[test]
    fn instruction_from_bytes() {
        test_decode!(
            "MOV ACC, 1",
            Instruction::Move {
                register: Register::Acc,
                datum: MoveDatum::Value(1),
            }
        );

        let instructions = parse_program("MOV R1, 4").expect("to parse instruction");
        let instruction_bin = to_binary(&instructions)
            .into_iter()
            .next()
            .expect("one instruction");
        let result: Instruction = (&instruction_bin)
            .try_into()
            .expect("to convert binary representation");
        assert_eq!(
            (Instruction::Move {
                register: Register::R1,
                datum: MoveDatum::Value(4),
            }),
            result
        );

        test_decode!(
            "MOV R1, 4",
            Instruction::Move {
                register: Register::R1,
                datum: MoveDatum::Value(4),
            }
        );

        test_decode!(
            "MOV R1, 0xaaaa",
            Instruction::Move {
                register: Register::R1,
                datum: MoveDatum::Value(0xaaaa),
            }
        );

        test_decode!(
            "MOV R2, R1",
            Instruction::Move {
                register: Register::R2,
                datum: MoveDatum::Register(Register::R1),
            }
        );

        test_decode!(
            "LOD R1, @15",
            Instruction::Load {
                register: Register::R1,
                datum: AddressDatum::Address(15),
                section: Section::Stack
            }
        );

        test_decode!(
            "LOD R1, @ACC",
            Instruction::Load {
                register: Register::R1,
                datum: AddressDatum::Register(Register::Acc),
                section: Section::Stack
            }
        );

        test_decode!(
            "STD ACC, @12",
            Instruction::Store {
                register: Register::Acc,
                datum: AddressDatum::Address(12),
                section: Section::Stack
            }
        );

        test_decode!(
            "STD ACC, @R1",
            Instruction::Store {
                register: Register::Acc,
                datum: AddressDatum::Register(Register::R1),
                section: Section::Stack
            }
        );

        test_decode!(
            "ADD ACC",
            Instruction::Add {
                datum: ArithmeticDatum::Register(Register::Acc)
            }
        );

        test_decode!(
            "ADD R1",
            Instruction::Add {
                datum: ArithmeticDatum::Register(Register::R1)
            }
        );

        test_decode!(
            "ADD2 R1, 2",
            Instruction::Add2 {
                register: Register::R1,
                datum: ArithmeticDatum::Data(2)
            }
        );

        test_decode!(
            "ADD 0b11",
            Instruction::Add {
                datum: ArithmeticDatum::Data(0b11)
            }
        );

        test_decode!(
            "SUB R2",
            Instruction::Sub {
                datum: ArithmeticDatum::Register(Register::R2)
            }
        );

        test_decode!(
            "SUB 12",
            Instruction::Sub {
                datum: ArithmeticDatum::Data(12)
            }
        );

        test_decode!(
            "SUB2 R8, 12",
            Instruction::Sub2 {
                register: Register::R8,
                datum: ArithmeticDatum::Data(12)
            }
        );

        test_decode!(
            "PUSH R1",
            Instruction::Push {
                register: Register::R1
            }
        );

        test_decode!(
            "POP R3",
            Instruction::Pop {
                register: Register::R3
            }
        );

        test_decode!(
            "PUSH IP",
            Instruction::Push {
                register: Register::InstructionPointer
            }
        );

        test_decode!(
            "JMP @45",
            Instruction::Jump {
                address: AddressDatum::Address(45)
            }
        );
        test_decode!(
            "JMP @R1",
            Instruction::Jump {
                address: AddressDatum::Register(Register::R1)
            }
        );

        test_decode!(
            "JNZ @45",
            Instruction::JumpNonZero {
                address: AddressDatum::Address(45)
            }
        );
        test_decode!(
            "JNZ @R1",
            Instruction::JumpNonZero {
                address: AddressDatum::Register(Register::R1)
            }
        );

        // 000_001

        test_decode!(
            "OUT R1, #1",
            Instruction::Output {
                register: Register::R1,
                bus: Bus(Lane::Lane0, 1)
            }
        );

        test_decode!("HLT", Instruction::Halt);
    }
}
