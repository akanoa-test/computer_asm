use crate::cpu::{Bus, CpuError};
use crate::data::constants::*;
use crate::data::constants::{OpcodeSize, RamDataBusSize, OPCODE_INDEX};
use crate::data::datum::{AddressDatum, ArithmeticDatum, InstructionDatum, MoveDatum};
use crate::data::register::Register;
use crate::data::OpCode;
use crate::mmu::Section;
use crate::{get_bus, get_operands};

#[derive(Debug, PartialEq)]
pub enum Instruction {
    Move {
        register: Register,
        datum: MoveDatum,
    },
    Load {
        register: Register,
        datum: AddressDatum,
        section: Section,
    },
    Store {
        register: Register,
        datum: AddressDatum,
        section: Section,
    },
    Add {
        datum: ArithmeticDatum,
    },
    Add2 {
        register: Register,
        datum: ArithmeticDatum,
    },
    Sub {
        datum: ArithmeticDatum,
    },
    Sub2 {
        register: Register,
        datum: ArithmeticDatum,
    },
    Output {
        register: Register,
        bus: Bus,
    },
    Jump {
        address: AddressDatum,
    },
    JumpNonZero {
        address: AddressDatum,
    },
    JumpZero {
        address: AddressDatum,
    },
    JumpNegative {
        address: AddressDatum,
    },
    Call {
        address: AddressDatum,
    },
    Push {
        register: Register,
    },
    Pop {
        register: Register,
    },
    Allocate {
        register: Register,
        amount: RamAddressBusSize,
    },
    Reallocate {
        register: Register,
        amount: RamAddressBusSize,
    },
    Free {
        address: AddressDatum,
    },
    Nop,
    Return,
    Halt,
    Stop,
}

impl TryFrom<&u32> for Instruction {
    type Error = CpuError;

    fn try_from(value: &RamDataBusSize) -> Result<Self, Self::Error> {
        let opcode = (value >> OPCODE_INDEX) as OpcodeSize;

        let instruction = match OpCode::try_from(opcode).map_err(|_| CpuError::Transcription)? {
            OpCode::Move => {
                let (register, source) = get_operands!(value);
                Instruction::Move {
                    register,
                    datum: source.into(),
                }
            }
            OpCode::Load => {
                let (register, datum) = get_operands!(value);

                Instruction::Load {
                    register,
                    datum: datum.into(),
                    section: Section::Stack,
                }
            }
            OpCode::LoadHeap => {
                let (register, datum) = get_operands!(value);

                Instruction::Load {
                    register,
                    datum: datum.into(),
                    section: Section::Heap,
                }
            }
            OpCode::Store => {
                let (register, datum) = get_operands!(value);

                Instruction::Store {
                    register,
                    datum: datum.into(),
                    section: Section::Stack,
                }
            }
            OpCode::StoreHeap => {
                let (register, datum) = get_operands!(value);

                Instruction::Store {
                    register,
                    datum: datum.into(),
                    section: Section::Heap,
                }
            }
            OpCode::Add => {
                let (_, datum) = get_operands!(value);
                Instruction::Add {
                    datum: datum.into(),
                }
            }
            OpCode::Add2 => {
                let (register, datum) = get_operands!(value);
                Instruction::Add2 {
                    register,
                    datum: datum.into(),
                }
            }
            OpCode::Sub => {
                let (_, datum) = get_operands!(value);
                Instruction::Sub {
                    datum: datum.into(),
                }
            }
            OpCode::Sub2 => {
                let (register, datum) = get_operands!(value);
                Instruction::Sub2 {
                    register,
                    datum: datum.into(),
                }
            }
            OpCode::Jump => {
                let (_register, address) = get_operands!(value);

                Instruction::Jump {
                    address: address.into(),
                }
            }
            OpCode::JumpNonZero => {
                let (_register, address) = get_operands!(value);

                Instruction::JumpNonZero {
                    address: address.into(),
                }
            }
            OpCode::JumpZero => {
                let (_register, address) = get_operands!(value);

                Instruction::JumpZero {
                    address: address.into(),
                }
            }
            OpCode::JumpNegative => {
                let (_register, address) = get_operands!(value);

                Instruction::JumpNegative {
                    address: address.into(),
                }
            }
            OpCode::Push => {
                let (register, _datum) = get_operands!(value);
                Instruction::Push { register }
            }
            OpCode::Pop => {
                let (register, _datum) = get_operands!(value);
                Instruction::Pop { register }
            }
            OpCode::Output => {
                let (register, datum) = get_operands!(value);

                let bus = get_bus!(datum);
                Instruction::Output { register, bus }
            }
            OpCode::Halt => Instruction::Halt,
            OpCode::Stop => Instruction::Stop,
            OpCode::Allocate => {
                let register = (value >> REGISTER_INDEX) & MASK_REGISTER;
                let register = Register::try_from(register as RegisterSize)
                    .map_err(|_| CpuError::Transcription)?;
                let datum = value & MASK_DATUM;
                Instruction::Allocate {
                    register,
                    amount: datum as RamAddressBusSize,
                }
            }
            OpCode::Reallocate => {
                let register = (value >> REGISTER_INDEX) & MASK_REGISTER;
                let register = Register::try_from(register as RegisterSize)
                    .map_err(|_| CpuError::Transcription)?;
                let datum = value & MASK_DATUM;
                Instruction::Reallocate {
                    register,
                    amount: datum as RamAddressBusSize,
                }
            }
            OpCode::Free => {
                let (_register, datum) = get_operands!(value);
                Instruction::Free {
                    address: datum.into(),
                }
            }
            OpCode::Call => {
                let (_register, address) = get_operands!(value);
                Instruction::Call {
                    address: address.into(),
                }
            }
            OpCode::Nop => Instruction::Nop,
            OpCode::Return => Instruction::Return,
        };

        Ok(instruction)
    }
}

impl ToString for Instruction {
    fn to_string(&self) -> String {
        match self {
            Instruction::Move { register, datum } => {
                format!("MOV {register}, {}", datum.to_string())
            }
            Instruction::Load {
                register,
                datum,
                section,
            } => match section {
                Section::Stack => format!("LOD {register}, {}", datum.to_string()),
                Section::Heap => format!("LODH {register}, {}", datum.to_string()),
                _ => unimplemented!("Loading data in section {section:?} is not yet implemented"),
            },
            Instruction::Store {
                register,
                datum,
                section,
            } => match section {
                Section::Stack => format!("STD {register}, {}", datum.to_string()),
                Section::Heap => format!("STDH {register}, {}", datum.to_string()),
                _ => unimplemented!("Storing data in section {section:?} is not yet implemented"),
            },
            Instruction::Add { datum } => format!("ADD {}", datum.to_string()),
            Instruction::Add2 { datum, register } => {
                format!("ADD2 {register} {}", datum.to_string())
            }
            Instruction::Sub { datum } => format!("SUB {}", datum.to_string()),
            Instruction::Sub2 { datum, register } => {
                format!("SUB2 {register}, {}", datum.to_string())
            }
            Instruction::Output { register, bus } => format!("OUT {register}, {}", bus.to_string()),
            Instruction::Jump { address } => format!("JMP {}", address.to_string()),
            Instruction::JumpNonZero { address } => format!("JNZ @{}", address.to_string()),
            Instruction::JumpZero { address } => format!("JMZ @{}", address.to_string()),
            Instruction::JumpNegative { address } => format!("JMN @{}", address.to_string()),
            Instruction::Call { address } => format!("CALL {}", address.to_string()),
            Instruction::Push { register } => format!("PUSH {register}"),
            Instruction::Pop { register } => format!("POP {register}"),
            Instruction::Return => "RET".to_string(),
            Instruction::Halt => "HLT".to_string(),
            Instruction::Stop => "STP".to_string(),
            Instruction::Allocate { register, amount } => format!("ALLOC {register}, {amount}"),
            Instruction::Reallocate { register, amount } => format!("REALLOC {register}, {amount}"),
            Instruction::Free { address } => format!("FREE {}", address.to_string()),
            Instruction::Nop => "NOP".to_string(),
        }
    }
}
