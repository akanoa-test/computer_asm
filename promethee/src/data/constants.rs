pub const DATA_SIZE: usize = 32;
pub const OPCODE_INDEX: usize = 24;
pub const REGISTER_INDEX: usize = 18;
pub const MODIFIER_INDEX: usize = 16;
pub const LANE_INDEX: usize = 8;

pub const NUMBER_OF_REGISTERS: usize = 19;

pub const MASK_REGISTER: u32 = 0x3f;
pub const MASK_DATUM: u32 = 0x3_ffff;
pub const MASK_VALUE: u32 = 0xffff;
pub const MASK_LANE: u8 = 0xff;
pub const MASK_BUS: u8 = 0xff;

pub const MEMORY_OFFSET: RamAddressBusSize = 0x00;

pub const BLOCK_SIZE: RamAddressBusSize = 32;

pub const STACK_SECTION_LENGTH: RamAddressBusSize = 4;

pub const STACK_SIZE: RamAddressBusSize = STACK_SECTION_LENGTH * BLOCK_SIZE;

pub type RamAddressBusSize = u16;
pub type RamDataBusSize = u32;

pub type DeviceAddressBusSize = u16;
pub type DeviceDataBusSize = u16;

pub type ValueSize = u16;
pub type RegisterSize = u8;
pub type ModifierSize = u8;
pub type OpcodeSize = u8;
pub type LaneSize = u8;
pub type BusSize = u8;
