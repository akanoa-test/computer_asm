use num_enum::TryFromPrimitive;
use serde::Serialize;
use strum::{Display, EnumString, EnumVariantNames};

/*
   ACC
   R{1  -> 8  } working register
   R8           function parameters flags
   R{10 -> 13 } function parameters
   R14          return value flags
   R15          return register
*/
#[derive(
    Debug,
    PartialEq,
    Copy,
    Clone,
    TryFromPrimitive,
    EnumString,
    Display,
    EnumVariantNames,
    Serialize,
)]
#[repr(u8)]
#[strum(ascii_case_insensitive)]
#[strum(serialize_all = "UPPERCASE")]
pub enum Register {
    #[strum(serialize = "ACC")]
    Acc = 0,
    R1,
    R2,
    R3,
    R4,
    R5,
    R6,
    R7,
    R8,
    R9,
    R10,
    R11,
    R12,
    R13,
    R14,
    R15,
    #[strum(serialize = "SP")]
    StackPointer,
    #[strum(serialize = "PTR")]
    Pointer,
    #[strum(serialize = "IP")]
    InstructionPointer,
    #[strum(disabled)]
    Unknown = 255,
}
