use crate::allocator::Allocator;
use crate::commands::{Command, ExecutionCommand};
use crate::data::constants::{
    BusSize, DeviceAddressBusSize, DeviceDataBusSize, LaneSize, RamAddressBusSize, RamDataBusSize,
    DATA_SIZE, LANE_INDEX, MASK_BUS, MASK_LANE, NUMBER_OF_REGISTERS,
};
use crate::data::datum::{AddressDatum, ArithmeticDatum, MoveDatum};
use crate::data::instruction::Instruction;
use crate::data::register::Register;
use crate::devices::{Device, DeviceBehavior, DeviceError};
use crate::events::{Event, ProcessManagement, StackEvent};
use crate::mmu::{Mmu, Section};
use crate::process::{define_debug_symbols, Process};
use crate::program_registry::ProgramRegistry;
use crate::ram::RamError;
use crate::scheduler::Scheduler;
use crate::transcoder::to_binary;
use crossbeam_channel::{Receiver, Sender};
use eyre::{eyre, Context};
use log::{error, warn};
use nom::ToUsize;
use num_enum::TryFromPrimitive;
use std::collections::{HashMap, HashSet};
use std::fmt::Debug;
use std::thread::sleep;
use std::time::Duration;
use thiserror::Error;

//                bus 0_0
// --------------|-----------....---->    -|
// |             |   bus 0_1               |--- lane 0
// |             |-----------....---->    -|
// |             |
// |             |
// |     CPU     |   bus 1_0
// |             |-----------....---->    -|
// |             |   bus 1_1               |
// |             |-----------....---->     | --- lane 1
// |             |   bus 1_2               |
// |------------ |-----------....---->    -|

macro_rules! get_register {
    ($self:expr, $register: expr) => {
        $self.registers[$register as usize]
    };
}

#[derive(Hash, PartialEq, Eq, TryFromPrimitive, Debug, Clone, Copy)]
#[repr(u8)]
pub enum Lane {
    Lane0 = 0,
    Lane1,
}

#[derive(Debug, PartialEq)]
pub struct Bus(pub Lane, pub u8);

impl TryFrom<&u16> for Bus {
    type Error = CpuError;

    fn try_from(value: &u16) -> Result<Self, Self::Error> {
        let lane_id = (value >> LANE_INDEX) as LaneSize & MASK_LANE;
        let bus_id = *value as BusSize & MASK_BUS;
        let lane = Lane::try_from(lane_id).map_err(|_| CpuError::Transcription)?;
        Ok(Bus(lane, bus_id))
    }
}

impl From<&Bus> for u16 {
    fn from(value: &Bus) -> Self {
        let lane = value.0 as u16;
        let bus = value.1 as u16;
        (lane << LANE_INDEX) | (bus & 0b111)
    }
}

impl ToString for Bus {
    fn to_string(&self) -> String {
        format!("#{}_{}", self.0 as u8, self.1)
    }
}

#[derive(Error, Debug)]
pub enum CpuError {
    #[error("Unable to transcript program")]
    Transcription,
    #[error("Unable to run program")]
    Running,
    #[error("An error occurs on Ram execution {0:?}")]
    Ram(RamError),
    #[error("An error occurs on Device execution {0:?}")]
    Device(DeviceError),
}

impl From<DeviceError> for CpuError {
    fn from(value: DeviceError) -> Self {
        CpuError::Device(value)
    }
}

#[derive(Default, Clone, Copy)]
pub struct Flags {
    negative: bool,
    zero: bool,
}

pub struct Cpu<'a> {
    send_event: Sender<Event>,
    receive_command: Receiver<Command>,
    scheduler: Scheduler<Process>,
    mmu: Mmu<'a>,
    registers: [RamDataBusSize; NUMBER_OF_REGISTERS],
    execution_pointer: RamAddressBusSize,
    devices: HashMap<Lane, &'a mut Device>,
    tick: usize,
    frequency: usize,
    pause: bool,
    pid: usize,
    programs_registry: ProgramRegistry,
    allocator: Allocator<'a>,
    flags: Flags,
}

impl<'a> Cpu<'a> {
    pub fn new(
        mmu: Mmu<'a>,
        tx: Sender<Event>,
        rx: Receiver<Command>,
        programs_registry: ProgramRegistry,
    ) -> Self {
        Self {
            send_event: tx,
            receive_command: rx,
            mmu,
            registers: Default::default(),
            execution_pointer: 0,
            devices: Default::default(),
            scheduler: Scheduler::new(),
            tick: 0,
            frequency: 1,
            pause: false,
            pid: 0,
            programs_registry,
            allocator: Allocator::new(),
            flags: Default::default(),
        }
    }

    pub fn load_programs_from_path(&mut self, path: &str) -> eyre::Result<()> {
        self.programs_registry.load(path)
    }

    pub fn set_device(&mut self, lane_id: Lane, device: &'a mut Device) {
        self.devices.insert(lane_id, device);
    }

    /// Push a value into the stack
    fn push_stack(&mut self, data: RamDataBusSize) -> eyre::Result<()> {
        // Get the current virtual stack address
        // its origin is "0"
        let stack_pointer_virtual_address =
            get_register!(self, Register::StackPointer) as RamAddressBusSize;

        // To get the physical address we need to add the physical stack_address for
        // the current process
        let stack_pointer_physical_address = stack_pointer_virtual_address;

        // We store the data at the physical address
        self.mmu
            .store(stack_pointer_physical_address, data, &Section::Stack)?;

        // We increment the virtual stack pointer address
        get_register!(self, Register::StackPointer) += 1;

        self.send_event(Event::Process {
            pid: self.pid,
            event: ProcessManagement::Stack(StackEvent::Push(data.to_usize())),
        })?;

        Ok(())
    }

    /// Pop a from the stack
    fn pop_stack(&mut self) -> eyre::Result<&RamDataBusSize> {
        // Get the current virtual stack address
        // its origin is "0" minus 1
        let stack_pointer_virtual_address =
            get_register!(self, Register::StackPointer) as RamAddressBusSize - 1;

        // To get the physical address we need to add the physical stack_address for
        // the current process
        let stack_pointer_physical_address = stack_pointer_virtual_address;

        // We load the data stored at the physical address
        let data = self
            .mmu
            .load(&stack_pointer_physical_address, &Section::Stack)?;

        // We decrement the virtual stack pointer address
        get_register!(self, Register::StackPointer) -= 1;

        self.send_event(Event::Process {
            pid: self.pid,
            event: ProcessManagement::Stack(StackEvent::Pop),
        })?;

        Ok(data)
    }

    /// Check the value given update flags
    /// if value is 0 => Z flag = 1
    /// if value first bit is 1 => N flag = 1
    fn update_flags(&mut self, register_value: RamDataBusSize) -> eyre::Result<()> {
        // if the accumulate value equals 0
        self.flags.zero = register_value == 0;
        // if the MSB bit is 1
        self.flags.negative = (register_value >> (DATA_SIZE - 1)) == 1;
        let Flags { zero, negative } = self.flags;
        self.send_event.send(Event::Process {
            pid: self.pid,
            event: ProcessManagement::Flags { zero, negative },
        })?;
        Ok(())
    }

    /// Watch register change
    fn watch_register(&mut self, register: Register) -> eyre::Result<()> {
        let value = get_register!(self, register);

        self.send_event(Event::Process {
            pid: self.pid,
            event: ProcessManagement::Register {
                register,
                value: value.to_usize(),
            },
        })?;

        Ok(())
    }

    fn jump_to(&mut self, address: AddressDatum) -> eyre::Result<()> {
        let address = match address {
            AddressDatum::Address(address) => address as RamDataBusSize,
            AddressDatum::Register(register) => get_register!(self, register) as RamDataBusSize,
        };
        get_register!(self, Register::InstructionPointer) = (address - 1) as RamDataBusSize;
        Ok(())
    }

    fn update_memory(
        &mut self,
        address: RamAddressBusSize,
        value: RamDataBusSize,
        section: &Section,
    ) -> eyre::Result<()> {
        self.mmu.store(address, value, section)?;
        match section {
            Section::Stack => {
                self.send_event(Event::Process {
                    pid: self.pid,
                    event: ProcessManagement::Stack(StackEvent::Update {
                        index: address.to_usize(),
                        value: value.to_usize(),
                    }),
                })?;
            }
            Section::Heap => self.mmu.send_heap_state(self.pid)?,
            Section::Code => unimplemented!("storing for section {section:?} not yet implemented"),
        }

        Ok(())
    }

    fn switch_context(&mut self) -> eyre::Result<Option<()>> {
        match self.scheduler.next() {
            None => {
                self.mmu.clear_context();
                return Ok(None);
            }
            Some(next_process) => {
                self.registers = next_process.registers;
                self.pid = next_process.pid;
                self.flags = next_process.flags;
                self.mmu.set_context(self.pid);

                self.send_event(Event::SwitchContext(self.pid))?;
            }
        }

        Ok(Some(()))
    }

    fn send_event(&self, event: Event) -> eyre::Result<()> {
        self.send_event
            .send(event.clone())
            .wrap_err(eyre!("Unable to send event {event:?}"))
    }

    /// Loads a program in memory and add it the processus to scheduler
    pub fn exec_program(&mut self, uuid: &str) -> eyre::Result<usize> {
        // Get the instruction for the program
        let program = self
            .programs_registry
            .get(uuid)
            .ok_or(eyre!("Unable to get program {uuid}"))?;

        let instructions = program.get_instruction();

        let debug_symbols = define_debug_symbols(instructions, uuid);
        let pid = self.scheduler.next_pid();

        let code_length = instructions.len();

        // Add a new context to MMU for the pid
        self.mmu.add_context(pid, code_length)?;

        // Store the processus instructions to memory
        let instructions_vector = to_binary(instructions);

        self.mmu.set_context(pid);

        instructions_vector
            .into_iter()
            .enumerate()
            .try_for_each(|(i, datum)| {
                self.mmu
                    .store(i as DeviceAddressBusSize, datum, &Section::Code)?;
                Ok::<_, eyre::Error>(())
            })?;

        // Create a process
        let process = Process {
            registers: [0; NUMBER_OF_REGISTERS],
            execution_pointer: 0,
            pid,
            flags: Flags::default(),
            name: program.get_name().to_string(),
            breakpoints: HashSet::new(),
        };

        // Add process to scheduler
        self.scheduler.put(process);

        // Send event to interface
        self.send_event(Event::Process {
            pid,
            event: ProcessManagement::Add(debug_symbols),
        })?;

        sleep(Duration::from_millis(100));

        self.mmu.send_initialisation_events()?;

        Ok(pid)
    }

    fn kill_process(&mut self, pid: usize) -> eyre::Result<()> {
        dbg!(format!("kill pid {pid}"));
        self.scheduler.remove_by_pid(pid);
        self.mmu.remove_context(pid)?;
        self.send_event(Event::Process {
            pid,
            event: ProcessManagement::Killed,
        })?;

        if !self.scheduler.is_empty() {
            self.switch_context()?;
            if get_register!(self, Register::InstructionPointer) > 0 {
                get_register!(self, Register::InstructionPointer) -= 1;
            }
        } else {
            get_register!(self, Register::InstructionPointer) = 0
        }
        Ok(())
    }

    pub(crate) fn run(&mut self) -> eyre::Result<()> {
        sleep(Duration::from_millis(100));
        loop {
            if let Ok(command) = self.receive_command.try_recv() {
                match command {
                    Command::SetFrequency(frequency) => {
                        dbg!(frequency);
                        self.frequency = frequency
                    }
                    Command::RunProgram { uuid } => {
                        self.exec_program(&uuid)?;
                    }
                    Command::UpdateProgram { uuid, sources } => {
                        self.programs_registry.update_program(uuid, sources)?;
                    }
                    Command::Execution {
                        pid,
                        command: execution_command,
                    } => match execution_command {
                        ExecutionCommand::Stop => {
                            self.kill_process(pid)?;
                        }
                        ExecutionCommand::Resume => self.pause = false,
                        ExecutionCommand::Pause => self.pause = true,
                        ExecutionCommand::Step => {}
                        ExecutionCommand::Restart(name) => {
                            self.exec_program(&name)?;
                            if self.scheduler.is_empty() {
                                get_register!(self, Register::InstructionPointer) = 0;
                            }
                            sleep(Duration::from_millis(100));
                        }
                    },
                    Command::HeapAddress { .. } => todo!(),
                    Command::HeapContent(pid) => {
                        self.mmu.send_heap_state(pid)?;
                    }
                    Command::UpdateBreakpoints { breakpoints, pid } => {
                        let process = self.scheduler.get_mut_by_pid(pid);
                        if let Some(process) = process {
                            process.breakpoints = breakpoints;
                        }
                    }
                }
            }

            sleep(Duration::from_secs_f64(1_f64 / (self.frequency as f64)));

            if self.pause || self.scheduler.is_empty() {
                continue;
            }

            if self.tick % 2 == 0 {
                // save current program

                let current_program = self.scheduler.get_mut_by_pid(self.pid);
                if let Some(program) = current_program {
                    program.registers = self.registers;
                    program.flags = self.flags;
                }

                if self.switch_context()?.is_none() {
                    continue;
                }
            }

            if let Some(process) = self.scheduler.get_mut_by_pid(self.pid) {
                // get the breakpoint list
                let breakpoints = &process.breakpoints;
                if breakpoints.contains(
                    &(get_register!(self, Register::InstructionPointer) as RamAddressBusSize),
                ) {
                    dbg!(format!(
                        "pid {}: break {}",
                        self.pid,
                        get_register!(self, Register::InstructionPointer)
                    ));
                    self.pause = true;
                }
            }

            for device in self.devices.values_mut() {
                device.update()?;
            }

            if self.step()? {
                break;
            }

            get_register!(self, Register::InstructionPointer) += 1;
            self.tick += 1;
            self.send_event(Event::Tick(self.tick))?;
        }
        Ok(())
    }

    fn step(&mut self) -> eyre::Result<bool> {
        self.send_event(Event::Process {
            pid: self.pid,
            event: ProcessManagement::Execution(
                get_register!(self, Register::InstructionPointer) as usize
            ),
        })?;

        let instruction: Instruction = self
            .mmu
            // physical address
            .load(
                &(get_register!(self, Register::InstructionPointer) as RamAddressBusSize),
                &Section::Code,
            )?
            .try_into()
            .wrap_err(eyre!(
                "Unable to get instruction at address {}",
                &get_register!(self, Register::InstructionPointer)
            ))?;

        match instruction {
            Instruction::Move {
                register,
                datum: source,
            } => match source {
                MoveDatum::Register(register_source) => {
                    if register_source == register {
                        Err(eyre!(
                            "Unable to get instruction at address {}",
                            &get_register!(self, Register::InstructionPointer)
                        ))?
                    }
                    self.registers[register as usize] = self.registers[register_source as usize];
                    self.watch_register(register_source)?;
                    self.watch_register(register)?;
                }
                MoveDatum::Value(value) => {
                    self.registers[register as usize] = value as RamDataBusSize;
                    self.watch_register(register)?;
                }
            },
            Instruction::Load {
                register,
                datum,
                section,
            } => {
                let address = match datum {
                    AddressDatum::Address(address) => address,
                    AddressDatum::Register(register) => {
                        get_register!(self, register) as RamAddressBusSize
                    }
                };
                warn!("Load address {address}");
                let value = self.mmu.load(&address, &section)?;
                get_register!(self, register) = *value;
                self.watch_register(register)?;
            }
            Instruction::Store {
                register,
                datum,
                section,
            } => {
                let address = match datum {
                    AddressDatum::Address(address) => address,
                    AddressDatum::Register(register) => {
                        get_register!(self, register) as RamAddressBusSize
                    }
                };

                let value = get_register!(self, register);

                self.update_memory(address, value, &section)?;
            }
            Instruction::Add { datum } => match datum {
                ArithmeticDatum::Register(register) => {
                    get_register!(self, Register::Acc) += get_register!(self, register);
                    self.watch_register(Register::Acc)?;
                }
                ArithmeticDatum::Data(value) => {
                    get_register!(self, Register::Acc) += value as RamDataBusSize;
                    self.watch_register(Register::Acc)?;
                }
            },
            Instruction::Add2 {
                datum,
                register: dest_register,
            } => match datum {
                ArithmeticDatum::Register(register) => {
                    get_register!(self, dest_register) += get_register!(self, register);
                    self.watch_register(register)?;
                }
                ArithmeticDatum::Data(value) => {
                    get_register!(self, dest_register) += value as RamDataBusSize;
                    self.watch_register(dest_register)?;
                }
            },
            Instruction::Sub { datum } => {
                let operand = match datum {
                    ArithmeticDatum::Register(register) => get_register!(self, register) as i32,
                    ArithmeticDatum::Data(value) => value as i32,
                };
                let result = get_register!(self, Register::Acc).wrapping_add_signed(-operand);
                get_register!(self, Register::Acc) = result;
                self.update_flags(result)?;
                self.watch_register(Register::Acc)?;
            }
            Instruction::Sub2 {
                datum,
                register: dest_register,
            } => {
                let operand = match datum {
                    ArithmeticDatum::Register(register) => get_register!(self, register) as i32,
                    ArithmeticDatum::Data(value) => value as i32,
                };
                let result = get_register!(self, dest_register).wrapping_add_signed(-operand);
                get_register!(self, dest_register) = result;
                self.update_flags(result)?;
                self.watch_register(dest_register)?;
            }
            Instruction::Output { register, bus } => {
                let (lane, bus_id) = (bus.0, bus.1);
                let data = get_register!(self, register) as DeviceDataBusSize;
                self.devices
                    .get_mut(&lane)
                    .ok_or(CpuError::Transcription)?
                    .write_register(bus_id, &data)?;
            }
            Instruction::Jump { address } => {
                self.jump_to(address)?;
            }

            Instruction::JumpNonZero { address } => {
                if !self.flags.zero {
                    self.jump_to(address)?;
                }
            }
            Instruction::JumpZero { address } => {
                if self.flags.zero {
                    self.jump_to(address)?;
                }
            }
            Instruction::JumpNegative { address } => {
                if self.flags.negative {
                    self.jump_to(address)?;
                }
            }
            Instruction::Call { address } => {
                // Save current context
                let data = get_register!(self, Register::InstructionPointer) as RamDataBusSize;
                self.push_stack(data)?;

                self.jump_to(address)?;
            }
            Instruction::Return => {
                // Restore previous context
                let data = self.pop_stack()?;
                get_register!(self, Register::InstructionPointer) = *data as RamDataBusSize;
            }
            Instruction::Push { register } => {
                let data = get_register!(self, register);
                self.push_stack(data)?;
            }
            Instruction::Pop { register } => {
                let data = self.pop_stack()?;
                get_register!(self, register) = *data;
                self.watch_register(register)?;
            }
            Instruction::Allocate { amount, register } => {
                let address = self.allocator.allocate(&mut self.mmu, amount)?;
                get_register!(self, register) = address as RamDataBusSize;
                self.watch_register(register)?;
                self.mmu.send_heap_state(self.pid)?;
            }
            Instruction::Reallocate { amount, register } => {
                let pointer = get_register!(self, register) as RamAddressBusSize;
                let address = self.allocator.reallocate(&mut self.mmu, pointer, amount)?;
                get_register!(self, register) = address as RamDataBusSize;
                self.watch_register(register)?;
                self.mmu.send_heap_state(self.pid)?;
            }
            Instruction::Free { address } => {
                let address = match address {
                    AddressDatum::Address(address) => address,
                    AddressDatum::Register(register) => {
                        get_register!(self, register) as RamAddressBusSize
                    }
                };
                self.allocator.free(&mut self.mmu, &address)?;
                self.mmu.send_heap_state(self.pid)?;
            }
            Instruction::Halt => return Ok(true),
            Instruction::Nop => {}
            Instruction::Stop => {
                self.kill_process(self.pid)?;
            }
        }

        Ok(false)
    }
}

#[cfg(test)]
mod tests {
    use crate::data::constants::DATA_SIZE;

    #[test]
    fn wrapping_ops() {
        let x = 2_u32;
        let y = 4;
        let a = x.wrapping_add_signed(y);
        let b = a >> (DATA_SIZE - 1);
        println!("{a:032b}");
        println!("{b:032b}");
        println!("{}", b == 1);
    }
}
