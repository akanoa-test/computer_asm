use crate::data::constants::RamAddressBusSize;
use serde::Serialize;
use std::collections::HashSet;

#[derive(Serialize, Debug, Clone)]
pub enum ExecutionCommand {
    Stop,
    Resume,
    Pause,
    Step,
    Restart(String),
}

#[derive(Serialize, Debug, Clone)]
pub enum Command {
    SetFrequency(usize),
    Execution {
        pid: usize,
        command: ExecutionCommand,
    },
    HeapContent(usize),
    HeapAddress {
        pid: usize,
        address: usize,
    },
    UpdateBreakpoints {
        pid: usize,
        breakpoints: HashSet<RamAddressBusSize>,
    },
    RunProgram {
        uuid: String,
    },
    UpdateProgram {
        uuid: String,
        sources: String,
    },
}
