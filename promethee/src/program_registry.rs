use crossbeam_channel::Sender;
use std::collections::HashMap;
use std::fs;
use std::fs::File;
use std::io::Read;
use std::ops::Deref;

use eyre::{eyre, Context};
use serde::Serialize;

use crate::cpu::CpuError;
use crate::data::instruction::Instruction;
use crate::events::{Event, ProgramManagement};
use crate::parser::parse_program;

#[derive(Debug, Clone, Serialize)]
pub struct ProgramEvent {
    name: String,
    source: String,
}

#[derive(Debug, Clone, Serialize)]
pub struct ProgramsEvent(Vec<ProgramEvent>);

#[derive(Debug)]
pub struct Program {
    name: String,
    source: String,
    instructions: Vec<Instruction>,
}

impl Program {
    pub fn new(name: String, source: String, instructions: Vec<Instruction>) -> Self {
        Self {
            name,
            source,
            instructions,
        }
    }

    pub fn get_instruction(&self) -> &[Instruction] {
        &self.instructions
    }

    pub fn get_name(&self) -> &str {
        &self.name
    }
}

pub struct ProgramRegistry {
    programs: HashMap<String, Program>,
    sender: Sender<Event>,
}

impl ProgramRegistry {
    pub fn new(sender: Sender<Event>) -> Self {
        Self {
            programs: Default::default(),
            sender,
        }
    }

    pub fn update_program(&mut self, uuid: String, sources: String) -> eyre::Result<()> {
        let previous_program = self
            .programs
            .get(&uuid)
            .ok_or(eyre!("Unable to found program {uuid}"))?;

        let instructions = parse_program(&sources).wrap_err(CpuError::Transcription)?;

        self.programs.insert(
            uuid,
            Program::new(previous_program.name.to_string(), sources, instructions),
        );

        Ok(())
    }

    pub fn load(&mut self, path: &str) -> eyre::Result<()> {
        let paths = fs::read_dir(path)?;

        for path in paths {
            let program_path = path?.path();

            let program_name = program_path
                .file_name()
                .ok_or(eyre!("Unable to get program name"))?
                .to_string_lossy()
                .to_string();

            let mut program_file = File::open(&program_path)
                .wrap_err(eyre!("Unable to open program {program_path:?}"))?;

            let mut source_code = String::new();
            program_file
                .read_to_string(&mut source_code)
                .wrap_err(eyre!("Unable to get data from {program_path:?}"))?;

            let name = short_uuid::short!().to_string();

            let instructions = parse_program(&source_code).wrap_err(CpuError::Transcription)?;

            self.programs.insert(
                name.clone(),
                Program::new(program_name, source_code, instructions),
            );
        }
        self.send_event()?;

        Ok(())
    }

    pub fn send_event(&self) -> eyre::Result<()> {
        let program_events =
            self.programs
                .iter()
                .fold(HashMap::new(), |mut acc, (uuid, program)| {
                    let program_event = ProgramEvent {
                        name: program.name.to_string(),
                        source: program.source.to_string(),
                    };

                    acc.insert(uuid.to_string(), program_event);

                    acc
                });

        self.sender.send(Event::Program(ProgramManagement::Init {
            programs: program_events,
        }))?;
        Ok(())
    }
}

impl Deref for ProgramRegistry {
    type Target = HashMap<String, Program>;

    fn deref(&self) -> &Self::Target {
        &self.programs
    }
}
