use std::collections::HashMap;

#[derive(Debug)]
pub struct Scheduler<T> {
    cursor: usize,
    processes: HashMap<usize, T>,
    pids: Vec<usize>,
    next_pid: usize,
}

impl<T> Scheduler<T> {
    pub(crate) fn new() -> Self {
        Scheduler {
            cursor: 0,
            processes: HashMap::new(),
            pids: vec![],
            next_pid: 0,
        }
    }

    pub(crate) fn is_empty(&self) -> bool {
        self.processes.is_empty()
    }

    pub(crate) fn put(&mut self, value: T) -> usize {
        self.processes.insert(self.next_pid, value);
        self.pids.push(self.next_pid);
        self.next_pid += 1;
        self.next_pid - 1
    }

    pub(crate) fn remove_by_pid(&mut self, pid: usize) -> Option<T> {
        self.pids.retain(|element_pid| &pid != element_pid);
        self.processes.remove(&pid)
    }

    pub(crate) fn next(&mut self) -> Option<&T> {
        if self.pids.is_empty() {
            return None;
        }

        let index = if self.cursor != 0 {
            self.cursor % self.pids.len()
        } else {
            0
        };
        self.cursor += 1;

        let pid = self.pids[index];

        self.processes.get(&pid)
    }

    pub(crate) fn current_index(&mut self) -> usize {
        let cursor = if self.cursor > 0 {
            self.cursor - 1
        } else {
            return 0;
        };

        cursor % self.pids.len()
    }

    pub(crate) fn next_pid(&self) -> usize {
        self.next_pid
    }

    pub(crate) fn get_mut_by_pid(&mut self, pid: usize) -> Option<&mut T> {
        self.processes.get_mut(&pid)
    }
}

#[cfg(test)]
mod tests {
    use crate::scheduler::Scheduler;

    #[test]
    fn must_cycle() {
        let mut scheduler = Scheduler::new();
        scheduler.put(1);
        scheduler.put(2);

        assert_eq!(scheduler.next(), Some(&1));
        assert_eq!(scheduler.next(), Some(&2));
        assert_eq!(scheduler.next(), Some(&1));
        assert_eq!(scheduler.next(), Some(&2));
    }

    #[test]
    fn must_allow_append_new_element() {
        let mut scheduler = Scheduler::new();
        scheduler.put(1);

        assert_eq!(scheduler.next(), Some(&1));
        assert_eq!(scheduler.next(), Some(&1));
        assert_eq!(scheduler.next(), Some(&1));

        scheduler.put(2);

        assert_eq!(scheduler.next(), Some(&2));
        assert_eq!(scheduler.next(), Some(&1));
        assert_eq!(scheduler.next(), Some(&2));
        assert_eq!(scheduler.next(), Some(&1));
    }

    #[test]
    fn must_allow_to_remove_an_element() {
        let mut scheduler = Scheduler::new();
        let pid_1 = scheduler.put(1);
        let _pid_2 = scheduler.put(2);
        let _pid_3 = scheduler.put(3);

        assert_eq!(scheduler.next(), Some(&1));
        assert_eq!(scheduler.next(), Some(&2));
        assert_eq!(scheduler.next(), Some(&3));
        assert_eq!(scheduler.next(), Some(&1));

        scheduler.remove_by_pid(pid_1);

        assert_eq!(scheduler.next(), Some(&2));
        assert_eq!(scheduler.next(), Some(&3));
        assert_eq!(scheduler.next(), Some(&2));
    }
}
