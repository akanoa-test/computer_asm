use crate::data::constants::{DeviceAddressBusSize, RamAddressBusSize, RamDataBusSize};
use crate::data::instruction::Instruction;
use eyre::eyre;
use log::debug;
use std::collections::HashMap;
use std::fmt::{Debug, Formatter};
use thiserror::Error;

/*
|-------------------|_0
| ORIGIN 1          |
| ORIGIN 2          |   reserved ( no program can write data nor program loaded there )
|___________________| 0xff
|-------------------| MAX 1
|                   | stack + (heap)???
      program 1     |
.-------------------. ORIGIN 1

--------------------  MAX 2

 ORIGIN 1
.      program 2    .
.-------------------. ORIGIN 2
|-------------------| 0xff_ff

caller-saved lazy push

RAM (32 bits)

program 1 :
stack

sp : the address of the previous frame

frame 0 :
    sp :  0
    Acc => 0  R1 => 1  R2 => 0  R3 => 0  R4 => 0  R5 => 0  R6 => 0  R7 => 0  R8 => 0  R9 => 0  R10 => 0  R11 => 0  R12 => 0  R13 => 0  R14 => 0  R15 => 0
16 adresses x 16 bits => 8 addresses on RAM

frame 1 :
    sp :  v@MAX1
    Acc => 0  R1 => 0xff  R2 => 0  R3 => 0  R4 => 0  R5 => 0  R6 => 0  R7 => 0  R8 => 0  R9 => 0  R10 => 0  R11 => 0  R12 => 0  R13 => 0  R14 => 0  R15 => 0
16 adresses x 16 bits => 8 addresses on RAM

-------------------------- Origin (0)

           Stack

-------------🢃----------- sp

           Free

--------------------------
           DMZ (forbidden)
-------------------------- Origin + STACK_SIZE

           CODE

------------------------- END


v@0  MOV ACC, R1
v@1  MOV R1, 6
v@2  ADD R1
     🢃 reversed
v@2  ADD R1
v@1  MOV R1, 6     => MAX + 1
v@0  MOV ACC, R1   => MAX + 0


8 contexts max

 */

pub struct Ram {
    data: HashMap<DeviceAddressBusSize, RamDataBusSize>,
}

impl Debug for Ram {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let mut data = self
            .data
            .iter()
            .map(|(k, v)| (k, Instruction::try_from(v).unwrap_or(Instruction::Halt)))
            .collect::<Vec<(&DeviceAddressBusSize, Instruction)>>();
        data.sort_by_key(|(k, _)| **k);
        let mut lines = vec![format!("\n")];
        for (address, instruction) in data {
            lines.push(format!(
                "v@{:02}=> {:032b}  => {:?}\n",
                address,
                instruction.to_binary(),
                instruction,
            ))
        }
        write!(f, "{}", lines.join(""))
    }
}

impl Ram {
    pub fn new() -> Self {
        Ram {
            data: HashMap::default(),
        }
    }

    pub fn load(&self, address: &RamAddressBusSize) -> eyre::Result<&RamDataBusSize> {
        self.data
            .get(address)
            .ok_or(eyre!(RamError::NotFound(*address)))
    }

    pub fn load_or_init(
        &mut self,
        address: &RamAddressBusSize,
        default: RamDataBusSize,
    ) -> &RamDataBusSize {
        self.data.entry(*address).or_insert(default)
    }

    pub fn store(&mut self, address: RamAddressBusSize, data: RamDataBusSize) {
        debug!("Insert {data} at address {address}");
        self.data.insert(address, data);
    }
}

#[test]
fn store_and_retrieve_data() {
    let mut memory = Ram::new();

    memory.store(10, 0b10101010);
    assert_eq!(
        memory.load(&10).expect("Must found data at address"),
        &0b10101010
    );
}

#[derive(Debug, Error)]
pub enum RamError {
    #[error("Data not found at address {0:?}")]
    NotFound(u16),
}
