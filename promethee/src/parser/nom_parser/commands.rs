use crate::data::register::Register;
use crate::mmu::Section;
use crate::parser::data::instruction::Instruction;
use crate::parser::nom_parser::datum::address;
use crate::parser::nom_parser::operands::parse_number;
use crate::parser::nom_parser::{datum, operands};
use nom::branch::alt;
use nom::bytes::complete::{tag, tag_no_case, take_until};
use nom::character::complete::{multispace0, space1};
use nom::combinator::opt;
use nom::multi::{many0, separated_list1};
use nom::sequence::{delimited, pair, tuple};
use nom::{IResult, Parser};

pub fn parse_jnz(input: &str) -> IResult<&str, Instruction> {
    let (remain, (_, _, datum)) =
        tuple((tag_no_case("jnz"), space1, operands::parse_jump_value))(input)?;

    Ok((remain, Instruction::JumpNonZero { datum }))
}

pub fn parse_jmz(input: &str) -> IResult<&str, Instruction> {
    let (remain, (_, _, datum)) =
        tuple((tag_no_case("jmz"), space1, operands::parse_jump_value))(input)?;

    Ok((remain, Instruction::JumpZero { datum }))
}

pub fn parse_jmn(input: &str) -> IResult<&str, Instruction> {
    let (remain, (_, _, datum)) =
        tuple((tag_no_case("jmn"), space1, operands::parse_jump_value))(input)?;

    Ok((remain, Instruction::JumpNegative { datum }))
}

pub fn parse_out(input: &str) -> IResult<&str, Instruction> {
    let (remain, (_, _, register, _, _, datum)) = tuple((
        tag_no_case("out"),
        space1,
        operands::parse_datum_register,
        tag(","),
        space1,
        operands::parse_bus,
    ))(input)?;
    let device = datum.get_bus(input)?;
    let register = register.get_register(input)?;
    Ok((
        remain,
        Instruction::Output {
            bus: device,
            register,
        },
    ))
}

pub fn parse_store(input: &str) -> IResult<&str, Instruction> {
    let (remain, (_, _, register, _, _, datum)) = tuple((
        tag_no_case("std"),
        space1,
        operands::parse_register,
        tag(","),
        space1,
        address::parse_address_datum,
    ))(input)?;
    Ok((
        remain,
        Instruction::Store {
            datum,
            register,
            section: Section::Stack,
        },
    ))
}

pub fn parse_store_heap(input: &str) -> IResult<&str, Instruction> {
    let (remain, (_, _, register, _, _, datum)) = tuple((
        tag_no_case("stdh"),
        space1,
        operands::parse_register,
        tag(","),
        space1,
        address::parse_address_datum,
    ))(input)?;
    Ok((
        remain,
        Instruction::Store {
            datum,
            register,
            section: Section::Heap,
        },
    ))
}

pub fn parse_load(input: &str) -> IResult<&str, Instruction> {
    let (remain, (_, _, register, _, _, datum)) = tuple((
        tag_no_case("lod"),
        space1,
        operands::parse_register,
        tag(","),
        space1,
        address::parse_address_datum,
    ))(input)?;

    Ok((
        remain,
        Instruction::Load {
            datum,
            register,
            section: Section::Stack,
        },
    ))
}

pub fn parse_load_heap(input: &str) -> IResult<&str, Instruction> {
    let (remain, (_, _, register, _, _, datum)) = tuple((
        tag_no_case("lodh"),
        space1,
        operands::parse_register,
        tag(","),
        space1,
        address::parse_address_datum,
    ))(input)?;

    Ok((
        remain,
        Instruction::Load {
            datum,
            register,
            section: Section::Heap,
        },
    ))
}

pub fn parse_allocate(input: &str) -> IResult<&str, Instruction> {
    let (remain, (_, _, register, _, _, amount)) = tuple((
        tag_no_case("alloc"),
        space1,
        operands::parse_register,
        tag(","),
        space1,
        parse_number,
    ))(input)?;

    Ok((remain, Instruction::Allocate { amount, register }))
}

pub fn parse_reallocate(input: &str) -> IResult<&str, Instruction> {
    let (remain, (_, _, register, _, _, amount)) = tuple((
        tag_no_case("realloc"),
        space1,
        operands::parse_register,
        tag(","),
        space1,
        parse_number,
    ))(input)?;

    Ok((remain, Instruction::Reallocate { amount, register }))
}

pub fn parse_free(input: &str) -> IResult<&str, Instruction> {
    let (remain, (_, _, address)) =
        tuple((tag_no_case("free"), space1, address::parse_address_datum))(input)?;

    Ok((remain, Instruction::Free { address }))
}

pub fn parse_add(input: &str) -> IResult<&str, Instruction> {
    let (remain, (_, _, datum)) =
        tuple((tag_no_case("add"), space1, datum::parse_arithmetic_datum))(input)?;

    Ok((remain, Instruction::Add { datum }))
}

pub fn parse_add2(input: &str) -> IResult<&str, Instruction> {
    let (remain, (_, _, register, _, _, datum)) = tuple((
        tag_no_case("add2"),
        space1,
        operands::parse_register,
        tag(","),
        space1,
        datum::parse_arithmetic_datum,
    ))(input)?;

    Ok((remain, Instruction::Add2 { datum, register }))
}

pub fn parse_sub(input: &str) -> IResult<&str, Instruction> {
    let (remain, (_, _, datum)) =
        tuple((tag_no_case("sub"), space1, datum::parse_arithmetic_datum))(input)?;

    Ok((remain, Instruction::Sub { datum }))
}

pub fn parse_sub2(input: &str) -> IResult<&str, Instruction> {
    let (remain, (_, _, register, _, _, datum)) = tuple((
        tag_no_case("sub2"),
        space1,
        operands::parse_register,
        tag(","),
        space1,
        datum::parse_arithmetic_datum,
    ))(input)?;

    Ok((remain, Instruction::Sub2 { datum, register }))
}

pub fn parse_push(input: &str) -> IResult<&str, Instruction> {
    let (remain, (_, _, datum)) =
        tuple((tag_no_case("push"), space1, operands::parse_datum_register))(input)?;

    let register = datum.get_register(input)?;

    Ok((remain, Instruction::Push { register }))
}

pub fn parse_pop(input: &str) -> IResult<&str, Instruction> {
    let (remain, (_, _, datum)) =
        tuple((tag_no_case("pop"), space1, operands::parse_datum_register))(input)?;

    let register = datum.get_register(input)?;

    Ok((remain, Instruction::Pop { register }))
}

pub fn parse_return(input: &str) -> IResult<&str, Instruction> {
    let (remain, _) = tag_no_case("ret")(input)?;

    Ok((remain, Instruction::Return))
}

pub fn parse_nop(input: &str) -> IResult<&str, Instruction> {
    let (remain, _) = tag_no_case("nop")(input)?;

    Ok((remain, Instruction::Nop))
}

pub fn parse_stop(input: &str) -> IResult<&str, Instruction> {
    let (remain, _) = tag_no_case("stp")(input)?;

    Ok((remain, Instruction::Stop))
}

pub fn parse_halt(input: &str) -> IResult<&str, Instruction> {
    let (remain, _) = tag_no_case("hlt")(input)?;

    Ok((remain, Instruction::Halt))
}

pub fn parse_jmp(input: &str) -> IResult<&str, Instruction> {
    let (remain, (_, _, datum)) =
        tuple((tag_no_case("jmp"), space1, operands::parse_jump_value))(input)?;

    Ok((remain, Instruction::Jump { datum }))
}

pub fn parse_mov(input: &str) -> IResult<&str, Instruction> {
    let (remain, (_, _, register, _, _, datum)) = tuple((
        tag_no_case("mov"),
        space1,
        take_until(",").and_then(operands::parse_register),
        tag(","),
        space1,
        operands::parse_move_datum,
    ))(input)?;

    Ok((remain, Instruction::Move { register, datum }))
}

pub fn parse_call(input: &str) -> IResult<&str, Instruction> {
    let (remain, (_, _, label)) =
        tuple((tag_no_case("call"), space1, operands::parse_label))(input)?;

    Ok((remain, Instruction::Call { label }))
}

pub fn parse_instruction(input: &str) -> IResult<&str, Instruction> {
    let chunk1 = alt((
        parse_mov,
        parse_load,
        parse_store,
        parse_jmp,
        parse_jnz,
        parse_jmz,
        parse_jmn,
        parse_out,
        parse_halt,
        parse_add,
        parse_add2,
    ));

    delimited(
        multispace0,
        alt((
            chunk1,
            parse_sub,
            parse_sub2,
            parse_push,
            parse_pop,
            parse_call,
            parse_return,
            parse_stop,
            parse_allocate,
            parse_reallocate,
            parse_free,
            parse_load_heap,
            parse_store_heap,
            parse_nop,
        )),
        multispace0,
    )(input)
}

fn parse_call_with_context_inner(input: &str) -> IResult<&str, (String, Vec<Register>)> {
    let parser = tuple((
        tag_no_case("call"),
        space1,
        operands::parse_label,
        opt(pair(tag(","), space1)), // context delimiter
        many0(separated_list1(
            pair(tag(","), space1),
            operands::parse_register,
        )),
    ));

    let (remain, (_, _, label, _, non_flatten_registers)) =
        delimited(multispace0, parser, multispace0)(input)?;

    let registers = non_flatten_registers
        .into_iter()
        .flatten()
        .collect::<Vec<Register>>();

    Ok((remain, (label, registers)))
}

pub fn parse_call_with_context(input: &str) -> IResult<&str, Option<(String, Vec<Register>)>> {
    opt(parse_call_with_context_inner)(input)
}
