use crate::data::constants::RamAddressBusSize;
use crate::data::register::Register;
use crate::data::OpCode;
use crate::parser::data::symbols::ArithmeticSymbol;
use crate::parser::nom_parser::macros::parse_expression;
use crate::parser::nom_parser::operands::parse_register;
use crate::parser::nom_parser::parse_label_symbol;
use nom::branch::alt;
use nom::bytes::complete::{tag, tag_no_case};
use nom::character::complete::{multispace0, space0, space1};
use nom::combinator::{map_res, opt};
use nom::sequence::{delimited, preceded, tuple};
use nom::IResult;
use tuple_utils::Merge;

// LOD ACC, @{SP - 1}
fn parse_load_store_expression_value(
    input: &str,
) -> IResult<&str, (Register, ArithmeticSymbol, u16)> {
    let (remain, result) = preceded(tag("@"), parse_expression)(input)?;

    Ok((remain, result))
}

fn parse_load_store_macro_inner(
    input: &str,
) -> IResult<&str, (Register, Register, ArithmeticSymbol, RamAddressBusSize)> {
    map_res(
        tuple((
            space1,
            parse_register,
            space0,
            tag(","),
            space1,
            parse_load_store_expression_value,
        )),
        |(_, register, _, _, _, (register_expr, symbol, value))| {
            Ok::<_, nom::error::Error<&str>>((register, register_expr, symbol, value))
        },
    )(input)
}

pub fn parse_load_store_macro(
    input: &str,
) -> IResult<
    &str,
    Option<(
        OpCode,
        Option<String>,
        Register,
        Register,
        ArithmeticSymbol,
        RamAddressBusSize,
    )>,
> {
    let (_, (line, maybe_label)) = parse_label_symbol(input)?;

    let parse_load_macro = map_res(
        preceded(tag_no_case("lod"), parse_load_store_macro_inner),
        |x| Ok::<_, nom::error::Error<&str>>((OpCode::Load, maybe_label.clone()).merge(x)),
    );

    let parse_load_heap_macro = map_res(
        preceded(tag_no_case("lodh"), parse_load_store_macro_inner),
        |x| Ok::<_, nom::error::Error<&str>>((OpCode::LoadHeap, maybe_label.clone()).merge(x)),
    );

    let parse_store_macro = map_res(
        preceded(tag_no_case("std"), parse_load_store_macro_inner),
        |x| Ok::<_, nom::error::Error<&str>>((OpCode::Store, maybe_label.clone()).merge(x)),
    );

    let parse_store_heap_macro = map_res(
        preceded(tag_no_case("stdh"), parse_load_store_macro_inner),
        |x| Ok::<_, nom::error::Error<&str>>((OpCode::StoreHeap, maybe_label.clone()).merge(x)),
    );

    let (remain, x) = delimited(
        multispace0,
        opt(alt((
            parse_load_heap_macro,
            parse_load_macro,
            parse_store_heap_macro,
            parse_store_macro,
        ))),
        multispace0,
    )(line)?;

    Ok((remain, x))
}

#[cfg(test)]
mod tests {
    use crate::data::register::Register;
    use crate::data::OpCode;
    use crate::parser::data::symbols::ArithmeticSymbol;
    use crate::parser::nom_parser::macros::store_load::{
        parse_load_store_expression_value, parse_load_store_macro,
    };

    #[test]
    fn test_parse_expression() {
        let line = "@{SP - 1}";
        let expected = (Register::StackPointer, ArithmeticSymbol::Minus, 1);
        let result = parse_load_store_expression_value(line);
        assert_eq!(result, Ok(("", expected)))
    }

    #[test]
    fn test_parse_load_macro() {
        let line = "LOD R1, @{SP - 1}";
        let expected = (
            OpCode::Load,
            None,
            Register::R1,
            Register::StackPointer,
            ArithmeticSymbol::Minus,
            1,
        );
        let result = parse_load_store_macro(line).expect("To parse load macro");
        assert_eq!(result, ("", Some(expected)));

        let line = "f1:   LOD R1, @{SP - 1}";
        let expected = (
            OpCode::Load,
            Some("f1".to_string()),
            Register::R1,
            Register::StackPointer,
            ArithmeticSymbol::Minus,
            1,
        );
        let result = parse_load_store_macro(line).expect("To parse load macro");
        assert_eq!(result, ("", Some(expected)));

        let line = "f1:   LODH R1, @{SP - 1}";
        let expected = (
            OpCode::LoadHeap,
            Some("f1".to_string()),
            Register::R1,
            Register::StackPointer,
            ArithmeticSymbol::Minus,
            1,
        );
        let result = parse_load_store_macro(line).expect("To parse load macro");
        assert_eq!(result, ("", Some(expected)))
    }

    #[test]
    fn test_parse_store_macro() {
        let line = "STD R1, @{SP - 1}";
        let expected = (
            OpCode::Store,
            None,
            Register::R1,
            Register::StackPointer,
            ArithmeticSymbol::Minus,
            1,
        );
        let result = parse_load_store_macro(line).expect("To parse load macro");
        assert_eq!(result, ("", Some(expected)));

        let line = "STDH R1, @{SP - 1}";
        let expected = (
            OpCode::StoreHeap,
            None,
            Register::R1,
            Register::StackPointer,
            ArithmeticSymbol::Minus,
            1,
        );
        let result = parse_load_store_macro(line).expect("To parse load macro");
        assert_eq!(result, ("", Some(expected)));

        let line = "STD ACC, @{SP - 1}\nRET";
        let expected = (
            OpCode::Store,
            None,
            Register::Acc,
            Register::StackPointer,
            ArithmeticSymbol::Minus,
            1,
        );
        let result = parse_load_store_macro(line).expect("To parse load macro");
        assert_eq!(result, ("", Some(expected)));

        let line = "f1:   STD ACC, @{R2 + 4}";
        let expected = (
            OpCode::Store,
            Some("f1".to_string()),
            Register::Acc,
            Register::R2,
            ArithmeticSymbol::Plus,
            4,
        );
        let result = parse_load_store_macro(line).expect("To parse load macro");
        assert_eq!(result, ("", Some(expected)));

        let line = "f1:   STDH ACC, @{R2 + 4}";
        let expected = (
            OpCode::StoreHeap,
            Some("f1".to_string()),
            Register::Acc,
            Register::R2,
            ArithmeticSymbol::Plus,
            4,
        );
        let result = parse_load_store_macro(line).expect("To parse load macro");
        assert_eq!(result, ("", Some(expected)))
    }
}
