use crate::data::register::Register;
use crate::parser::data::symbols::ArithmeticSymbol;
use crate::parser::nom_parser::macros::parse_expression;
use crate::parser::nom_parser::operands::parse_register;
use crate::parser::nom_parser::parse_label_symbol;
use nom::bytes::complete::{tag, tag_no_case};
use nom::character::complete::{multispace0, space0, space1};
use nom::combinator::{map_res, opt};
use nom::sequence::{delimited, tuple};
use nom::IResult;

pub fn parse_move_macro(
    input: &str,
) -> IResult<&str, Option<(Option<String>, Register, Register, ArithmeticSymbol, u16)>> {
    let (_, (line, maybe_label)) = parse_label_symbol(input)?;

    let parser = map_res(
        tuple((
            tag_no_case("mov"),
            space1,
            parse_register,
            space0,
            tag(","),
            space1,
            parse_expression,
        )),
        |(_, _, dest_register, _, _, _, (src_register, symbol, value))| {
            Ok::<_, nom::error::Error<&str>>((
                maybe_label.clone(),
                dest_register,
                src_register,
                symbol,
                value,
            ))
        },
    );

    let (remain, x) = delimited(multispace0, opt(parser), multispace0)(line)?;
    Ok((remain, x))
}

#[cfg(test)]
mod tests {
    use crate::data::register::Register;
    use crate::parser::data::symbols::ArithmeticSymbol;
    use crate::parser::nom_parser::macros::r#move::parse_move_macro;

    #[test]
    fn test_parse_macro() {
        // without label
        let line = "    MOV R1, {SP - 1}\n";
        let expected = (
            None,
            Register::R1,
            Register::StackPointer,
            ArithmeticSymbol::Minus,
            1,
        );
        let result = parse_move_macro(line).expect("To parse move macro");
        assert_eq!(result, ("", Some(expected)));

        // with label
        let line = "f: MOV R1, {SP - 1}";
        let expected = (
            Some("f".to_string()),
            Register::R1,
            Register::StackPointer,
            ArithmeticSymbol::Minus,
            1,
        );
        let result = parse_move_macro(line).expect("To parse move macro");
        assert_eq!(result, ("", Some(expected)));

        let line = "f: LOD R1, @{SP - 1}";
        let result = parse_move_macro(line).expect("To parse move macro");
        assert_eq!(
            result,
            ("LOD R1, @{SP - 1}", None),
            "must left untouched non MOV macro"
        )
    }
}
