use crate::parser::nom_parser::operands::parse_number;
use eyre::Context;
use nom::branch::alt;
use nom::bytes::complete::{tag, tag_no_case, take_until};
use nom::character::complete::{alphanumeric1, newline, space0, space1};
use nom::combinator::{opt, peek};
use nom::multi::many0;
use nom::sequence::{delimited, preceded, separated_pair, tuple};
use nom::IResult;
use regex::{Captures, Regex, Replacer};
use std::collections::HashMap;
use std::ops::{Deref, DerefMut};

#[derive(Debug, PartialEq)]
pub struct DataSegments(HashMap<String, DataSegment>);

impl Deref for DataSegments {
    type Target = HashMap<String, DataSegment>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for DataSegments {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl From<HashMap<String, DataSegment>> for DataSegments {
    fn from(value: HashMap<String, DataSegment>) -> Self {
        DataSegments(value)
    }
}

impl Replacer for DataSegments {
    fn replace_append(&mut self, caps: &Captures<'_>, dst: &mut String) {
        let key = &caps[1];
        if let Some(value) = self.get(key) {
            match value {
                DataSegment::Number(data) => dst.push_str(&format!("{data}")),
                DataSegment::String(data) => dst.push_str(data),
            }
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum DataSegment {
    Number(u16),
    String(String),
}

pub fn parse_code_segment(input: &str) -> IResult<&str, String> {
    let end = take_until("~data:");

    let (remain, code) = preceded(tag_no_case("~code:"), end)(input)?;

    Ok((remain, code.to_string()))
}

fn parse_data_string(input: &str) -> IResult<&str, DataSegment> {
    let (remain, data) = delimited(tag("\""), alphanumeric1, tag("\""))(input)?;
    Ok((remain, DataSegment::String(data.to_string())))
}

fn parse_data_number(input: &str) -> IResult<&str, DataSegment> {
    let (remain, data) = parse_number(input)?;
    Ok((remain, DataSegment::Number(data)))
}

pub fn parse_data_segment(input: &str) -> IResult<&str, DataSegments> {
    let (remain, _) = tag("~data:")(input)?;
    let (remain, data) = many0(separated_pair(
        preceded(tuple((newline, space0)), alphanumeric1),
        space1,
        alt((parse_data_number, parse_data_string)),
    ))(remain)?;
    let map = HashMap::from_iter(
        data.into_iter()
            .map(|(name, data)| (name.to_string(), data)),
    );
    let data_segments = DataSegments(map);
    Ok((remain, data_segments))
}

pub fn replace_alias(input: &str, mut data_segments: DataSegments) -> eyre::Result<String> {
    let re = Regex::new(r"\$([\w_]+)").wrap_err("Unable to create the replacement regex")?;
    let result = re.replace_all(input, data_segments.by_ref());

    Ok(result.to_string())
}

pub(crate) fn maybe_parse_segments(input: &str) -> IResult<&str, Option<String>> {
    opt(peek(parse_code_segment))(input)
}

#[cfg(test)]
mod tests {
    use crate::parser::nom_parser::macros::segments::{
        parse_code_segment, parse_data_segment, replace_alias, DataSegment,
    };
    use std::collections::HashMap;

    #[test]
    fn get_data_segment() {
        let program = r#"~code:
    start:  MOV R1, $name
            PUSH R1
            STP
~data:
    age 0x45
    name "test""#;

        let expected_code = r#"
    start:  MOV R1, $name
            PUSH R1
            STP
"#;
        let expected_data = r#"~data:
    age 0x45
    name "test""#;
        let result = parse_code_segment(program).expect("Should parse code segment");
        assert_eq!(result, (expected_data, expected_code.to_string()));
    }

    #[test]
    fn parse_data_segment_with_string() {
        let data = r#"~data:
    age 0x45
    name "test"
    zipcode 56280"#;

        let expected_data = HashMap::from([
            ("age".to_string(), DataSegment::Number(0x45)),
            ("name".to_string(), DataSegment::String("test".to_string())),
            ("zipcode".to_string(), DataSegment::Number(56280)),
        ])
        .into();
        let result = parse_data_segment(data).expect("Unable to parse segment");
        assert_eq!(result, ("", expected_data));
    }

    #[test]
    fn test_replace_alias() {
        let line = "start: MOV R2, $age $register";
        let aliases = HashMap::from([
            ("age".to_string(), DataSegment::Number(0x45)),
            ("register".to_string(), DataSegment::Number(0b101)),
        ]);
        let expected = "start: MOV R2, 69 5".to_string();
        let result = replace_alias(line, aliases.into()).expect("Should replace value");
        assert_eq!(expected, result)
    }
}
