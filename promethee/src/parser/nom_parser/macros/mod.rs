use crate::data::register::Register;
use crate::parser::data::symbols::ArithmeticSymbol;
use crate::parser::nom_parser::operands::{parse_number, parse_register};
use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::space1;
use nom::combinator::map_res;
use nom::sequence::{delimited, tuple};
use nom::IResult;

pub mod r#move;
pub mod store_load;

pub mod segments;

fn parse_expression(input: &str) -> IResult<&str, (Register, ArithmeticSymbol, u16)> {
    let p = map_res(
        tuple((
            parse_register,
            space1,
            parse_arithmetic_symbol,
            space1,
            parse_number,
        )),
        |(register, _, symbol, _, value)| {
            Ok::<_, nom::error::Error<&str>>((register, symbol, value))
        },
    );

    delimited(tag("{"), p, tag("}"))(input)
}

pub fn parse_arithmetic_symbol(input: &str) -> IResult<&str, ArithmeticSymbol> {
    alt((
        map_res(tag("+"), |_| {
            Ok::<ArithmeticSymbol, nom::error::Error<&str>>(ArithmeticSymbol::Plus)
        }),
        map_res(tag("-"), |_| {
            Ok::<ArithmeticSymbol, nom::error::Error<&str>>(ArithmeticSymbol::Minus)
        }),
    ))(input)
}
