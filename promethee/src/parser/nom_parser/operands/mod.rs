use crate::data::register::Register;
use crate::parser::data::datum::{InstructionDatum, JumpDatum, MoveDatum};
use nom::branch::alt;
use nom::bytes::complete::{tag, tag_no_case};
use nom::character::complete::{alpha1, alphanumeric1, digit1};
use nom::combinator::{map_res, opt, recognize};
use nom::multi::many1;
use nom::sequence::{pair, preceded, tuple};
use nom::IResult;
use std::str::FromStr;

fn parse_decimal_number(input: &str) -> IResult<&str, u16> {
    map_res(parse_value_digit, |y| y.parse::<u16>())(input)
}

fn parse_binary_number(input: &str) -> IResult<&str, u16> {
    map_res(
        tuple((opt(tag("0b")), parse_value_digit)),
        |(_, y): (Option<&str>, String)| u16::from_str_radix(&y, 2),
    )(input)
}

fn parse_hexa_number(input: &str) -> IResult<&str, u16> {
    map_res(
        tuple((opt(tag("0x")), parse_value_digit)),
        |(_, y): (Option<&str>, String)| u16::from_str_radix(&y, 16),
    )(input)
}

fn parse_value_digit(input: &str) -> IResult<&str, String> {
    let variants = alt((digit1, tag("_"), alpha1));
    let (remain, parts) = many1(variants)(input)?;
    let digits = parts
        .iter()
        .filter_map(|v| (*v != "_").then_some(*v))
        .collect::<Vec<&str>>()
        .join("");
    Ok((remain, digits))
}

pub fn parse_number(input: &str) -> IResult<&str, u16> {
    alt((parse_decimal_number, parse_binary_number, parse_hexa_number))(input)
}

fn parse_address(input: &str) -> IResult<&str, InstructionDatum> {
    let (remain, address) = preceded(tag("@"), parse_number)(input)?;
    Ok((remain, InstructionDatum::Address(address)))
}

pub fn parse_bus(input: &str) -> IResult<&str, InstructionDatum> {
    let (remain, bus_id) = preceded(tag("#"), parse_number)(input)?;
    Ok((remain, InstructionDatum::Bus(bus_id)))
}

pub fn parse_label_value(input: &str) -> IResult<&str, String> {
    map_res(many1(alt((alphanumeric1, tag("_"), tag("-")))), |vec| {
        Ok::<String, ()>(vec.join(""))
    })(input)
}

pub fn parse_label(input: &str) -> IResult<&str, String> {
    let (remain, label) = preceded(tag("'"), parse_label_value)(input)?;
    Ok((remain, label))
}

fn parse_direct_value(input: &str) -> IResult<&str, InstructionDatum> {
    let (remain, value) = parse_number(input)?;
    Ok((remain, InstructionDatum::Data(value)))
}

pub fn parse_register(input: &str) -> IResult<&str, Register> {
    let parse_numeric_register = recognize(pair(tag_no_case("r"), digit1));
    let parse_accumulator = tag_no_case("acc");
    let parse_stack_pointer = tag_no_case("sp");
    let parse_ptr = tag_no_case("ptr");
    let parse_instruction_pointer = tag_no_case("ip");

    map_res(
        alt((
            parse_numeric_register,
            parse_accumulator,
            parse_stack_pointer,
            parse_ptr,
            parse_instruction_pointer,
        )),
        Register::from_str,
    )(input)
}

pub fn parse_datum_register(input: &str) -> IResult<&str, InstructionDatum> {
    // ACC
    // R1 R2 ...

    map_res(parse_register, |register| {
        Ok::<InstructionDatum, ()>(InstructionDatum::Register(register))
    })(input)
}

pub fn parse_value(input: &str) -> IResult<&str, InstructionDatum> {
    let (remain, datum) = alt((
        parse_address,
        parse_datum_register,
        parse_bus,
        parse_direct_value,
    ))(input)?;
    Ok((remain, datum))
}

fn parse_jump_address(input: &str) -> IResult<&str, u16> {
    let (remain, address) = preceded(tag("@"), parse_number)(input)?;
    Ok((remain, address))
}

fn parse_jump_register(input: &str) -> IResult<&str, Register> {
    let (remain, register) = preceded(tag("@"), parse_register)(input)?;
    Ok((remain, register))
}

pub fn parse_jump_value(input: &str) -> IResult<&str, JumpDatum> {
    let parse_address = map_res(parse_jump_address, |address| {
        Ok::<JumpDatum, nom::error::Error<&str>>(JumpDatum::Address(address))
    });
    let parse_jump_label = map_res(parse_label, |label| {
        Ok::<JumpDatum, nom::error::Error<&str>>(JumpDatum::Label(label))
    });
    let parse_jump_register = map_res(parse_jump_register, |register| {
        Ok::<JumpDatum, nom::error::Error<&str>>(JumpDatum::Register(register))
    });
    alt((parse_address, parse_jump_label, parse_jump_register))(input)
}

pub fn parse_move_datum(input: &str) -> IResult<&str, MoveDatum> {
    let parse_register_value = map_res(parse_register, |register| {
        Ok::<_, nom::error::Error<&str>>(MoveDatum::Register(register))
    });
    let parse_value_direct = map_res(parse_number, |value| {
        Ok::<_, nom::error::Error<&str>>(MoveDatum::Value(value))
    });
    alt((parse_register_value, parse_value_direct))(input)
}
