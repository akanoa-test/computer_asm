use crate::parser::data::datum::ArithmeticDatum;
use crate::parser::nom_parser::operands;
use nom::branch::alt;
use nom::combinator::map_res;
use nom::IResult;

pub mod address;

pub fn parse_arithmetic_datum(input: &str) -> IResult<&str, ArithmeticDatum> {
    let parse_register_value = map_res(operands::parse_register, |register| {
        Ok::<ArithmeticDatum, nom::error::Error<&str>>(ArithmeticDatum::Register(register))
    });
    let parse_value_direct = map_res(operands::parse_number, |value| {
        Ok::<ArithmeticDatum, nom::error::Error<&str>>(ArithmeticDatum::Value(value))
    });
    alt((parse_register_value, parse_value_direct))(input)
}
