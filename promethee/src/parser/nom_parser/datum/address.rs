use crate::parser::data::datum::AddressDatum;
use crate::parser::nom_parser::operands;
use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::sequence::preceded;
use nom::IResult;

fn parse_address_numeric_value(input: &str) -> IResult<&str, AddressDatum> {
    let (remain, address) = preceded(tag("@"), operands::parse_number)(input)?;
    Ok((remain, AddressDatum::Address(address)))
}

fn parse_address_register_value(input: &str) -> IResult<&str, AddressDatum> {
    let (remain, register) = preceded(tag("@"), operands::parse_register)(input)?;
    Ok((remain, AddressDatum::Register(register)))
}

pub fn parse_address_datum(input: &str) -> IResult<&str, AddressDatum> {
    alt((parse_address_register_value, parse_address_numeric_value))(input)
}
