use crate::parser::data::instruction::Instruction;
use nom::bytes::complete::{tag, take_till, take_until};
use nom::character::complete::{multispace0, space1};
use nom::character::is_newline;
use nom::combinator::{all_consuming, opt};
use nom::multi::many1;
use nom::sequence::{delimited, tuple};
use nom::IResult;

pub mod commands;
pub mod datum;
pub mod macros;
mod operands;

pub(super) fn parse_program(input: &str) -> IResult<&str, Vec<Instruction>> {
    all_consuming(many1(commands::parse_instruction))(input)
}

pub(super) fn parse_label_symbol(input: &str) -> IResult<&str, (&str, Option<String>)> {
    let parser = tuple((
        opt(tuple((operands::parse_label_value, tag(":"), space1))),
        take_till(|c| is_newline(c as u8)),
    ));

    let (remain1, (symbol, line)) = delimited(multispace0, parser, multispace0)(input)?;

    Ok((remain1, (line, symbol.map(|(s, _, _)| s.to_string()))))
}

pub(super) fn parse_comment_inline(input: &str) -> IResult<&str, &str> {
    let (remain, maybe_cleaned_line) = opt(take_until("//"))(input)?;
    let result = match maybe_cleaned_line {
        Some(cleaned_line) => (remain, cleaned_line),
        None => ("", input),
    };
    Ok(result)
}

#[cfg(test)]
mod tests {
    use crate::cpu::{Bus, Lane};
    use crate::data::register::Register;
    use crate::mmu::Section;
    use crate::parser::data::datum::{AddressDatum, ArithmeticDatum, JumpDatum, MoveDatum};
    use crate::parser::data::instruction::Instruction;
    use crate::parser::nom_parser::commands::{
        parse_add, parse_add2, parse_call, parse_call_with_context, parse_free, parse_halt,
        parse_jmp, parse_jmz, parse_jnz, parse_load, parse_load_heap, parse_mov, parse_out,
        parse_return, parse_store, parse_store_heap, parse_sub, parse_sub2,
    };
    use crate::parser::nom_parser::operands::{parse_label, parse_number};
    use crate::parser::nom_parser::{parse_comment_inline, parse_label_symbol, parse_program};

    #[test]
    fn test_parse_value() {
        assert_eq!(parse_number("10"), Ok(("", 10)));
        assert_eq!(parse_number("1_0"), Ok(("", 10)));
        assert_eq!(parse_number("0b10"), Ok(("", 0b10)));
        assert_eq!(parse_number("0b110_001"), Ok(("", 0b110_001)));
        assert_eq!(parse_number("0xA8"), Ok(("", 0xA8)));
        assert_eq!(parse_number("0x01_02"), Ok(("", 0x01_02)));
    }

    #[test]
    fn test_parse_label() {
        assert_eq!(parse_label("'halt"), Ok(("", "halt".to_string())));

        assert_eq!(parse_label("'halt_2"), Ok(("", "halt_2".to_string())));
    }

    #[test]
    fn test_parse_comment_inline() {
        let line = "MOV R10, R1 ";
        let result = parse_comment_inline(line);
        assert_eq!(result, Ok(("", "MOV R10, R1 ")));

        let line = "MOV R10, R1       // set a as f1 param";
        let result = parse_comment_inline(line);
        assert_eq!(result, Ok(("// set a as f1 param", "MOV R10, R1       ")));

        let line = "f1: MOV ACC, 2        // b = 2";
        let result = parse_comment_inline(line);
        assert_eq!(result, Ok(("// b = 2", "f1: MOV ACC, 2        ")));
    }

    #[test]
    fn test_parse_mov() {
        let line = "MOV ACC, 1";
        let result = parse_mov(line);
        assert_eq!(
            result,
            Ok((
                "",
                Instruction::Move {
                    datum: MoveDatum::Value(1),
                    register: Register::Acc
                }
            ))
        );

        let line = "MOV R2, 0b1_1_0000_0000_0000_01";
        let result = parse_mov(line);
        assert_eq!(
            result,
            Ok((
                "",
                Instruction::Move {
                    datum: MoveDatum::Value(0b1100_0000_0000_0001),
                    register: Register::R2
                }
            ))
        );

        let line = "MOV R1, 4";
        let result = parse_mov(line);
        assert_eq!(
            result,
            Ok((
                "",
                Instruction::Move {
                    datum: MoveDatum::Value(4),
                    register: Register::R1
                }
            ))
        );

        let line = "mov R2, R1";
        let result = parse_mov(line);
        assert_eq!(
            result,
            Ok((
                "",
                Instruction::Move {
                    datum: MoveDatum::Register(Register::R1),
                    register: Register::R2
                }
            ))
        );
    }

    #[test]
    fn test_jump() {
        let line = "JMP @15";
        let result = parse_jmp(line);
        assert_eq!(
            result,
            Ok((
                "",
                Instruction::Jump {
                    datum: JumpDatum::Address(15)
                }
            ))
        );

        let line = "JMP 'label";
        let result = parse_jmp(line);
        assert_eq!(
            result,
            Ok((
                "",
                Instruction::Jump {
                    datum: JumpDatum::Label("label".to_string())
                }
            ))
        );

        let line = "JMP @R7";
        let result = parse_jmp(line);
        assert_eq!(
            result,
            Ok((
                "",
                Instruction::Jump {
                    datum: JumpDatum::Register(Register::R7)
                }
            ))
        );

        let line = "JMP #0";
        let result = parse_jnz(line);
        assert!(result.is_err());

        let line = "JMP 10";
        let result = parse_jnz(line);
        assert!(result.is_err());
    }

    #[test]
    fn test_jump_non_zero() {
        let line = "JNZ @15";
        let result = parse_jnz(line);
        assert_eq!(
            result,
            Ok((
                "",
                Instruction::JumpNonZero {
                    datum: JumpDatum::Address(15),
                }
            ))
        );

        let line = "JNZ #0";
        let result = parse_jnz(line);
        assert!(result.is_err());

        let line = "JNZ 10";
        let result = parse_jnz(line);
        assert!(result.is_err());

        let line = "JNZ 'test";
        let result = parse_jnz(line);
        assert_eq!(
            result,
            Ok((
                "",
                Instruction::JumpNonZero {
                    datum: JumpDatum::Label("test".to_string()),
                }
            ))
        );

        let line = "JNZ #0";
        let result = parse_jnz(line);
        assert!(result.is_err());

        let line = "JNZ 10";
        let result = parse_jnz(line);
        assert!(result.is_err());

        let line = "JMZ 'test";
        let result = parse_jmz(line);
        assert_eq!(
            result,
            Ok((
                "",
                Instruction::JumpZero {
                    datum: JumpDatum::Label("test".to_string()),
                }
            ))
        );

        let line = "JMZ @15";
        let result = parse_jmz(line);
        assert_eq!(
            result,
            Ok((
                "",
                Instruction::JumpZero {
                    datum: JumpDatum::Address(15),
                }
            ))
        );
    }

    #[test]
    fn test_out() {
        let line = "OUT R1, #1";
        let result = parse_out(line);
        assert_eq!(
            result,
            Ok((
                "",
                Instruction::Output {
                    register: Register::R1,
                    bus: Bus(Lane::Lane0, 1)
                }
            ))
        );

        let line = "OUT R666, #12";
        let result = parse_out(line);
        assert!(result.is_err());

        let line = "OUT R1, #0fffff";
        let result = parse_out(line);
        assert!(result.is_err());
    }

    #[test]
    fn test_parse_store() {
        let line = "STD ACC, @15";
        let result = parse_store(line);
        assert_eq!(
            result,
            Ok((
                "",
                Instruction::Store {
                    datum: AddressDatum::Address(15),
                    register: Register::Acc,
                    section: Section::Stack
                }
            ))
        );

        let line = "STDH ACC, @15";
        let result = parse_store_heap(line);
        assert_eq!(
            result,
            Ok((
                "",
                Instruction::Store {
                    datum: AddressDatum::Address(15),
                    register: Register::Acc,
                    section: Section::Heap
                }
            ))
        );
    }

    #[test]
    fn test_parse_load() {
        let line = "LOD ACC, @15";
        let result = parse_load(line);
        assert_eq!(
            result,
            Ok((
                "",
                Instruction::Load {
                    datum: AddressDatum::Address(15),
                    register: Register::Acc,
                    section: Section::Stack
                }
            ))
        );

        let line = "LODH ACC, @15";
        let result = parse_load_heap(line);
        assert_eq!(
            result,
            Ok((
                "",
                Instruction::Load {
                    datum: AddressDatum::Address(15),
                    register: Register::Acc,
                    section: Section::Heap
                }
            ))
        );

        let line = "LOD ACC, @ACC";
        let result = parse_load(line);
        assert_eq!(
            result,
            Ok((
                "",
                Instruction::Load {
                    datum: AddressDatum::Register(Register::Acc),
                    register: Register::Acc,
                    section: Section::Stack
                }
            ))
        );

        let line = "LODH ACC, @ACC";
        let result = parse_load_heap(line);
        assert_eq!(
            result,
            Ok((
                "",
                Instruction::Load {
                    datum: AddressDatum::Register(Register::Acc),
                    register: Register::Acc,
                    section: Section::Heap
                }
            ))
        );
    }

    #[test]
    fn test_parse_add() {
        let line = "ADD R1";
        let result = parse_add(line);
        assert_eq!(
            result,
            Ok((
                "",
                Instruction::Add {
                    datum: ArithmeticDatum::Register(Register::R1)
                }
            ))
        );

        let line = "ADD 0x42";
        let result = parse_add(line);
        assert_eq!(
            result,
            Ok((
                "",
                Instruction::Add {
                    datum: ArithmeticDatum::Value(0x42)
                }
            ))
        );
    }

    #[test]
    fn test_parse_add2() {
        let line = "ADD2 R1, R2";
        let result = parse_add2(line);
        assert_eq!(
            result,
            Ok((
                "",
                Instruction::Add2 {
                    register: Register::R1,
                    datum: ArithmeticDatum::Register(Register::R2)
                }
            ))
        );

        let line = "ADD2 R8, 0x42";
        let result = parse_add2(line);
        assert_eq!(
            result,
            Ok((
                "",
                Instruction::Add2 {
                    register: Register::R8,
                    datum: ArithmeticDatum::Value(0x42)
                }
            ))
        );
    }

    #[test]
    fn test_parse_sub() {
        let line = "SUB R1";
        let result = parse_sub(line);
        assert_eq!(
            result,
            Ok((
                "",
                Instruction::Sub {
                    datum: ArithmeticDatum::Register(Register::R1)
                }
            ))
        );

        let line = "SUB 0xff";
        let result = parse_sub(line);
        assert_eq!(
            result,
            Ok((
                "",
                Instruction::Sub {
                    datum: ArithmeticDatum::Value(0xff)
                }
            ))
        );
    }

    #[test]
    fn test_parse_sub2() {
        let line = "SUB2 R4, R1";
        let result = parse_sub2(line);
        assert_eq!(
            result,
            Ok((
                "",
                Instruction::Sub2 {
                    register: Register::R4,
                    datum: ArithmeticDatum::Register(Register::R1)
                }
            ))
        );

        let line = "SUB2 R5, 0xff";
        let result = parse_sub2(line);
        assert_eq!(
            result,
            Ok((
                "",
                Instruction::Sub2 {
                    register: Register::R5,
                    datum: ArithmeticDatum::Value(0xff)
                }
            ))
        );
    }

    #[test]
    fn test_call() {
        let line = "CALL 'label";
        let result = parse_call(line);
        assert_eq!(
            result,
            Ok((
                "",
                Instruction::Call {
                    label: "label".to_string(),
                }
            ))
        );

        let line = "CALL 10";
        let result = parse_call(line);
        assert!(result.is_err());

        let line = "CALL @10";
        let result = parse_call(line);
        assert!(result.is_err());

        let line = "CALL #10";
        let result = parse_call(line);
        assert!(result.is_err());
    }

    #[test]
    fn test_parse_halt() {
        let line = "HLT";
        let result = parse_halt(line);
        assert_eq!(result, Ok(("", Instruction::Halt)));
    }

    #[test]
    fn test_parse_return() {
        let line = "RET";
        let result = parse_return(line);
        assert_eq!(result, Ok(("", Instruction::Return)));
    }

    #[test]
    fn test_parse_free() {
        let line = "FREE @R4";
        let result = parse_free(line);
        assert_eq!(
            result,
            Ok((
                "",
                Instruction::Free {
                    address: AddressDatum::Register(Register::R4),
                }
            ))
        );

        let line = "FREE @45";
        let result = parse_free(line);
        assert_eq!(
            result,
            Ok((
                "",
                Instruction::Free {
                    address: AddressDatum::Address(45),
                }
            ))
        )
    }

    #[test]
    fn test_parse_call_with_context() {
        let program = r#"CALL 'f2, ACC, R1, R2
        MOV R1, R2"#;
        let expected = (
            "f2".to_string(),
            vec![Register::Acc, Register::R1, Register::R2],
        );
        let result =
            parse_call_with_context(program).expect("Should parse call instruction with context");
        assert_eq!(("MOV R1, R2", Some(expected)), result);

        let program = r#"           CALL 'f2, ACC, R1, R2
        MOV R1, R2"#;
        let expected = (
            "f2".to_string(),
            vec![Register::Acc, Register::R1, Register::R2],
        );
        let result =
            parse_call_with_context(program).expect("Should parse call instruction with context");
        assert_eq!(("MOV R1, R2", Some(expected)), result);

        let program = r#"CALL 'f2  // my comment
        MOV R1, R2"#;
        let expected = ("f2".to_string(), vec![]);
        let result = parse_call_with_context(program)
            .expect("Should parse call instruction without context");
        assert_eq!(
            ("// my comment\n        MOV R1, R2", Some(expected)),
            result
        );

        let program = r#"MOV R2, 0x42  // my comment
        MOV R1, R2"#;
        let result = parse_call_with_context(program)
            .expect("Should parse call instruction without context");
        assert_eq!((program, None), result);
    }

    #[test]
    fn test_parse_program() {
        let assembly = r#"
        MOV ACC, 1
        MOV R1, 4
        ADD R1
        STD ACC, @15
        STDH ACC, @15
        CALL 'block
        LOD R1, @15
        LODH R1, @15
        MOV R2, R1
        OUT R2, #0x01_02
        JMP @8
        JNZ @4
        JMZ @4
        ALLOC R4, 5
        REALLOC R4, 5
        FREE @R4
        FREE @45
        HLT
        RET
        NOP
        "#;

        let result = parse_program(assembly);
        let expected = vec![
            Instruction::Move {
                register: Register::Acc,
                datum: MoveDatum::Value(1),
            },
            Instruction::Move {
                register: Register::R1,
                datum: MoveDatum::Value(4),
            },
            Instruction::Add {
                datum: ArithmeticDatum::Register(Register::R1),
            },
            Instruction::Store {
                register: Register::Acc,
                datum: AddressDatum::Address(15),
                section: Section::Stack,
            },
            Instruction::Store {
                register: Register::Acc,
                datum: AddressDatum::Address(15),
                section: Section::Heap,
            },
            Instruction::Call {
                label: "block".to_string(),
            },
            Instruction::Load {
                register: Register::R1,
                datum: AddressDatum::Address(15),
                section: Section::Stack,
            },
            Instruction::Load {
                register: Register::R1,
                datum: AddressDatum::Address(15),
                section: Section::Heap,
            },
            Instruction::Move {
                register: Register::R2,
                datum: MoveDatum::Register(Register::R1),
            },
            Instruction::Output {
                register: Register::R2,
                bus: Bus(Lane::Lane1, 2),
            },
            Instruction::Jump {
                datum: JumpDatum::Address(8),
            },
            Instruction::JumpNonZero {
                datum: JumpDatum::Address(4),
            },
            Instruction::JumpZero {
                datum: JumpDatum::Address(4),
            },
            Instruction::Allocate {
                register: Register::R4,
                amount: 5,
            },
            Instruction::Reallocate {
                register: Register::R4,
                amount: 5,
            },
            Instruction::Free {
                address: AddressDatum::Register(Register::R4),
            },
            Instruction::Free {
                address: AddressDatum::Address(45),
            },
            Instruction::Halt,
            Instruction::Return,
            Instruction::Nop,
        ];
        assert_eq!(Ok(("", expected)), result);
    }

    #[test]
    fn test_get_label() {
        let line = "halt:      HLT";
        let result = parse_label_symbol(line);
        assert_eq!(result, Ok(("", ("HLT", Some("halt".to_string())))));

        let line = "HLT";
        let result = parse_label_symbol(line);
        assert_eq!(result, Ok(("", ("HLT", None))));

        let line = r#"halt:      HLT
        MOV R1, 1
        "#;
        let result = parse_label_symbol(line);
        assert_eq!(
            result,
            Ok(("MOV R1, 1\n        ", ("HLT", Some("halt".to_string()))))
        );
    }
}
