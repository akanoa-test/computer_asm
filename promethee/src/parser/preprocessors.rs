use crate::data::constants::RamAddressBusSize;
use crate::parser::data::symbols::ArithmeticSymbol;
use crate::parser::nom_parser::commands::parse_call_with_context;
use crate::parser::nom_parser::macros::r#move::parse_move_macro;
use crate::parser::nom_parser::macros::segments;
use crate::parser::nom_parser::macros::segments::{DataSegment, DataSegments};
use crate::parser::nom_parser::macros::store_load::parse_load_store_macro;
use crate::parser::nom_parser::parse_comment_inline;
use crate::parser::{nom_parser, ParserError};
use eyre::{eyre, WrapErr};
use regex::Regex;
use std::collections::HashMap;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum PreprocessorError {
    #[error("Unable to get label : {0}")]
    LabelNotFound(String),
    #[error("Nom error occurs: {0:?}")]
    Nom(nom::Err<nom::error::Error<String>>),
    #[error("Unable to preprocess labels")]
    Labels,
}

impl From<PreprocessorError> for ParserError {
    fn from(error: PreprocessorError) -> Self {
        ParserError::Preprocessor(error)
    }
}

/// This preprocessor cleans up comments
/// ```asm
/// label: MOV R1, 12 // my comment
///```
/// into
/// ```asm
/// label: MOV R1, 12
/// ```
pub fn preprocessor_cleanup_inline_comments(input: &str) -> eyre::Result<String> {
    let program = input
        .lines()
        .filter(|line| !line.trim().is_empty())
        .try_fold(vec![], |mut lines, line| {
            let (_, line_without_comment) = parse_comment_inline(line)
                .map_err(|err| PreprocessorError::Nom(err.to_owned()))
                .wrap_err(eyre!("Unable to parse comment from line {}", line))?;
            lines.push(line_without_comment.to_string());
            Ok::<_, eyre::Error>(lines)
        })?;
    let program = program.join("\n");
    Ok(program)
}

/// This preprocessor remove comment blocks
/// ```asm
/// /*
/// my comment
/// */
/// label : MOV ACC,1
/// /*
/// comment 2
/// */
/// LOD R1, ACC
/// ```
/// into
/// ```asm
///
/// label : MOV ACC,1
///
/// LOD R1, ACC
/// ```
pub(super) fn preprocessor_cleanup_comment_blocks(input: &str) -> eyre::Result<String> {
    // https://stackoverflow.com/a/17579177
    let regex = Regex::new(r"(?m)/\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+/")
        .wrap_err("Unable to create regex")?;

    let result = regex.replace_all(input, "");

    Ok(result.to_string())
}

/// This preprocessor normalize code if segments are absent
///
///```asm
/// start: MOV R1, $name
///        PUSH R1
///        STP
///```
/// into
/// ```asm
/// ~code:
///  start:  MOV R1, $name
///          PUSH R1
///          STP
/// ~data:
/// ```
pub fn preprocessor_normalize_segments(input: &str) -> eyre::Result<String> {
    let (code, maybe_code_segment) = segments::maybe_parse_segments(input)
        .map_err(|err| err.to_owned())
        .wrap_err("Unable to parse segments")?;
    let mut code = code.to_string();
    if maybe_code_segment.is_none() {
        code = format!("~code:{code}\n~data:\n")
    }
    Ok(code)
}

/// This preprocessor removes segments and gather symbols
/// ```asm
/// ~code:
///  start:  MOV R1, $name
///          PUSH R1
///          STP
/// ~data:
///     age 0x45
///     name "test"
/// ```
/// into
///```asm
/// start: MOV R1, $name
///        PUSH R1
///        STP
///```
///
pub fn preprocessor_get_segments(input: &str) -> eyre::Result<(String, DataSegments)> {
    let (data_segment, code) = segments::parse_code_segment(input).map_err(|err| err.to_owned())?;
    let (_remain, data) =
        segments::parse_data_segment(data_segment).map_err(|err| err.to_owned())?;
    Ok((code.to_string(), data))
}

/// This preprocessor expands
/// ```asm
/// start: MOV R1, $age
///        PUSH R1
///        STP
/// ```
/// with symbol table
/// ```asm
/// age => 0x45
/// ```
/// into
/// ```asm
/// start: MOV R1, 0x45
///        PUSH R1
///        STP
/// ```
/// todo: Implement the preprocessor
fn preprocessor_expand_symbols(
    input: &str,
    symbols: &HashMap<String, DataSegment>,
) -> eyre::Result<String> {
    let program = input
        .lines()
        .filter(|line| !line.trim().is_empty())
        .try_fold(vec![], |mut lines, line| {
            Ok::<Vec<String>, eyre::Error>(lines)
        })?;
    let program = program.join("\n");
    Ok(program)
}

/// This preprocessor expands
/// ```asm
/// CALL 'f2, R1, R2
/// ```
/// into
/// ```asm
/// PUSH R1
/// PUSH R2
/// CALL 'f2
/// POP R2
/// POP R1
/// ```
pub fn preprocessor_expand_call_macro(input: &str) -> eyre::Result<String> {
    let program = input
        .lines()
        .filter(|line| !line.trim().is_empty())
        .try_fold(vec![], |mut lines, line| {
            let (_remain, may_call_line) = parse_call_with_context(line)
                .map_err(|err| PreprocessorError::Nom(err.to_owned()))
                .wrap_err("Unable to expand call macro")?;

            if let Some((label, context)) = may_call_line {
                context.iter().for_each(|register| {
                    let line = format!("PUSH {}", register);
                    lines.push(line);
                });

                lines.push(format!("CALL '{}", label));

                context.iter().rev().for_each(|register| {
                    let line = format!("POP {}", register);
                    lines.push(line);
                });
            } else {
                lines.push(line.to_string())
            }

            Ok::<Vec<String>, eyre::Error>(lines)
        })?;
    let program = program.join("\n");
    Ok(program)
}

///
/// ```asm
/// LOD ACC, @{SP - 1}
/// STD R1,  @{SP - 1}
/// ```
/// into
/// ```asm
/// PUSH ACC
/// MOV ACC, SP
/// SUB 1
/// LOD ACC, @ACC
/// POP ACC
///
/// PUSH ACC
/// MOV ACC, SP
/// SUB 1
/// STD R1, @ACC
/// POP ACC
/// ```
pub fn preprocessor_expand_load_store_macro(input: &str) -> eyre::Result<String> {
    let program = input
        .lines()
        .filter(|line| !line.trim().is_empty())
        .try_fold(vec![], |mut lines, line| {
            let (_remain, maybe_expandable_line) = parse_load_store_macro(line)
                .map_err(|err| PreprocessorError::Nom(err.to_owned()))
                .wrap_err("Unable to expand load/store macro")?;

            if let Some((opcode, label, dest_register, src_register, symbol, offset)) =
                maybe_expandable_line
            {
                let label = label
                    .map(|label| format!("{}: ", label))
                    .unwrap_or("".to_string());

                lines.push(format!("{}MOV PTR, {}", label, src_register));
                lines.push(format!("PUSH ACC"));
                lines.push(format!("MOV ACC, PTR"));

                match symbol {
                    ArithmeticSymbol::Plus => lines.push(format!("ADD {}", offset)),
                    ArithmeticSymbol::Minus => lines.push(format!("SUB {}", offset)),
                }
                lines.push(format!("MOV PTR, ACC"));
                lines.push(format!("POP ACC"));
                lines.push(format!("{} {}, @PTR", opcode, dest_register));
            } else {
                lines.push(line.to_string())
            }

            Ok::<Vec<String>, eyre::Error>(lines)
        })?;
    let program = program.join("\n");
    Ok(program)
}

/// Expands move macro
/// ```asm
/// f: MOV R1, {SP - 1}
///    MOV R2, {SP + 2}
/// ```
/// into
/// ```asm
/// f: PUSH ACC
///    MOV ACC, SP
///    SUB 1
///    MOV R1, ACC
///    POP ACC
///
///    PUSH ACC
///    MOV ACC, SP
///    ADD 2
///    MOV R2, ACC
///    POP ACC
/// ```
pub fn preprocessor_expand_move_macro(input: &str) -> eyre::Result<String> {
    let program = input
        .lines()
        .filter(|line| !line.trim().is_empty())
        .try_fold(vec![], |mut lines, line| {
            let (_remain, maybe_expandable_line) = parse_move_macro(line)
                .map_err(|err| PreprocessorError::Nom(err.to_owned()))
                .wrap_err("Unable to expand move macro")?;

            if let Some((maybe_label, dest_register, src_register, symbol, offset)) =
                maybe_expandable_line
            {
                let label = maybe_label
                    .map(|label| format!("{}: ", label))
                    .unwrap_or("".to_string());

                lines.push(format!("PUSH ACC"));
                lines.push(format!("{}MOV ACC, {}", label, src_register));

                let offset = offset + 1; // add a postincrement because PUSH ACC
                match symbol {
                    ArithmeticSymbol::Plus => lines.push(format!("ADD {}", offset)),
                    ArithmeticSymbol::Minus => lines.push(format!("SUB {}", offset)),
                }
                lines.push(format!("MOV {}, ACC", dest_register));
                lines.push(format!("POP ACC"));
            } else {
                lines.push(line.to_string())
            }

            Ok::<Vec<String>, eyre::Error>(lines)
        })?;

    let program = program.join("\n");
    Ok(program)
}

/// This preprocessor consumes a program
/// Extract the label definition
/// and returns the code without definition
///
/// It turns
/// ```asm
///         MOV R1, 2
/// label1: MOV R2, 0x42
/// label2: ADD 1
/// ```
/// into
/// ```asm
/// MOV R1, 2
/// MOV R2, 0x42
/// ADD 1
/// ```
/// And creates the mapping (name -> line)
/// ```asm
/// label1 -> 1
/// label2 -> 2
/// ```
pub fn preprocessor_label(
    input: &str,
) -> eyre::Result<(HashMap<String, RamAddressBusSize>, String)> {
    let (labels, lines) = input
        .lines()
        .filter(|line| !line.trim().is_empty())
        .enumerate()
        .try_fold(
            (HashMap::new(), Vec::new()),
            |(mut labels, mut lines), (line_number, line)| {
                // Cleanup labels
                // label: MOV R1, 12 => MOV R1, 12
                let (_, (line_without_label, maybe_label)) = nom_parser::parse_label_symbol(line)
                    .map_err(|err| PreprocessorError::Nom(err.to_owned()))
                    .wrap_err(eyre!("Unable to parse line label from line {}", line))?;
                if let Some(label) = maybe_label {
                    labels.insert(label, line_number as RamAddressBusSize);
                }
                lines.push(line_without_label.trim().to_string());
                Ok::<(HashMap<String, u16>, Vec<String>), eyre::Error>((labels, lines))
            },
        )?;
    let lines = lines.join("\n");

    Ok((labels, lines))
}

#[cfg(test)]
mod tests {
    use crate::parser::preprocessors::{
        preprocessor_cleanup_comment_blocks, preprocessor_expand_call_macro,
        preprocessor_normalize_segments,
    };

    #[test]
    fn test_expand_macros() {
        let assembly = r#"
           MOV R1, 1
           JNZ R1, 'branch
           JMP     'branch
           CALL 'f, ACC, R1, R2
halt:      HLT
branch:    MOV ACC, 1
           ADD R1
           JMP 'halt
f:         MOV R1, 0xff
           RET
    "#;

        let expected = r#"           MOV R1, 1
           JNZ R1, 'branch
           JMP     'branch
PUSH ACC
PUSH R1
PUSH R2
CALL 'f
POP R2
POP R1
POP ACC
halt:      HLT
branch:    MOV ACC, 1
           ADD R1
           JMP 'halt
f:         MOV R1, 0xff
           RET"#;
        let result = preprocessor_expand_call_macro(assembly).expect("Should have expand macros");
        assert_eq!(result, expected.to_string())
    }

    #[test]
    fn strip_comment_blocks() {
        let data = r#"/*
fn main() {
    a = 100
    f(&a) // a = 77
}

fn f(&a) {
   b = 12
   *a -= 11 - b
}
*/
    MOV R1, 100         // a = 100
/*
    write 0xaaa1 at address @158 in write mode High Bits
*/
    MOV R10, SP
    CALL 'f, R1         // f(&a)
    HLT                 // a = 77
"#;

        let result = preprocessor_cleanup_comment_blocks(data).expect("Remove comment blocks");
        println!("{result}");
    }

    #[test]
    fn normalize_code_without_segments() {
        let program = r#"
    start:  MOV R1, $name
            PUSH R1
            STP"#;

        let expected_data = r#"~code:
    start:  MOV R1, $name
            PUSH R1
            STP
~data:
"#;

        let result = preprocessor_normalize_segments(program).expect("Unable to parse segments");
        assert_eq!(result, expected_data)
    }

    #[test]
    fn normalize_code_with_segments() {
        let program = r#"~code:
    start:  MOV R1, $name
            PUSH R1
            STP
~data:
    age 0x45
    name "test""#;

        let expected_data = r#"~code:
    start:  MOV R1, $name
            PUSH R1
            STP
~data:
    age 0x45
    name "test""#;

        let result = preprocessor_normalize_segments(program).expect("Unable to parse segments");
        assert_eq!(result, expected_data)
    }
}
