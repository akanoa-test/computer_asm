#[derive(Debug, PartialEq)]
pub enum ArithmeticSymbol {
    Plus,
    Minus,
}
