use crate::cpu::Bus;
use crate::data::constants::{RamAddressBusSize, ValueSize};
use crate::data::register::Register;
use nom::error::ErrorKind;

#[derive(Debug, PartialEq)]
pub enum InstructionDatum {
    Register(Register),
    Address(ValueSize),
    Bus(ValueSize),
    Data(ValueSize),
    Label(String),
}

#[derive(Debug, PartialEq)]
pub enum JumpDatum {
    Address(ValueSize),
    Label(String),
    Register(Register)
}

#[derive(Debug, PartialEq)]
pub enum ArithmeticDatum {
    Value(ValueSize),
    Register(Register),
}

#[derive(Debug, PartialEq)]
pub enum MoveDatum {
    Value(ValueSize),
    Register(Register),
    Exepression(Register, ArithmeticSymbol, u16),
}

#[derive(Debug, PartialEq)]
pub enum AddressDatum {
    Address(RamAddressBusSize),
    Register(Register),
}

impl InstructionDatum {
    pub(crate) fn get_register(
        self,
        input: &str,
    ) -> Result<Register, nom::Err<nom::error::Error<&str>>> {
        match self {
            InstructionDatum::Register(register_id) => match register_id {
                Register::Unknown => None,
                register => Some(register),
            },
            _ => None,
        }
        .ok_or(nom::Err::Failure(nom::error::make_error(
            input,
            ErrorKind::Fail,
        )))
    }

    pub(crate) fn get_bus(self, input: &str) -> Result<Bus, nom::Err<nom::error::Error<&str>>> {
        match self {
            InstructionDatum::Bus(lane_id) => match Bus::try_from(&lane_id) {
                Ok(bus) => Some(bus),
                Err(_) => None,
            },
            _ => None,
        }
        .ok_or(nom::Err::Failure(nom::error::make_error(
            input,
            ErrorKind::Fail,
        )))
    }
}

use crate::data::datum::AddressDatum as LibAddressDatum;
use crate::data::datum::ArithmeticDatum as LibArithmeticDatum;
use crate::data::datum::InstructionDatum as LibInstructionDatum;
use crate::data::datum::MoveDatum as LibMoveDatum;
use crate::parser::data::symbols::ArithmeticSymbol;

impl From<InstructionDatum> for LibInstructionDatum {
    fn from(value: InstructionDatum) -> Self {
        match value {
            InstructionDatum::Register(register) => LibInstructionDatum::Register(register),
            InstructionDatum::Address(address) => LibInstructionDatum::Address(address),
            InstructionDatum::Bus(bus) => LibInstructionDatum::Bus(bus),
            InstructionDatum::Data(data) => LibInstructionDatum::Data(data),
            InstructionDatum::Label(_) => unreachable!("Label not exist in instruction set values"),
        }
    }
}

impl From<ArithmeticDatum> for LibArithmeticDatum {
    fn from(value: ArithmeticDatum) -> Self {
        match value {
            ArithmeticDatum::Value(value) => LibArithmeticDatum::Data(value),
            ArithmeticDatum::Register(register) => LibArithmeticDatum::Register(register),
        }
    }
}

impl From<AddressDatum> for LibAddressDatum {
    fn from(value: AddressDatum) -> Self {
        match value {
            AddressDatum::Address(value) => LibAddressDatum::Address(value),
            AddressDatum::Register(register) => LibAddressDatum::Register(register),
        }
    }
}

impl From<MoveDatum> for LibMoveDatum {
    fn from(value: MoveDatum) -> Self {
        match value {
            MoveDatum::Value(value) => LibMoveDatum::Value(value),
            MoveDatum::Register(register) => LibMoveDatum::Register(register),
            _ => {
                unreachable!("A loaded program can't have arithmetic macro")
            }
        }
    }
}
