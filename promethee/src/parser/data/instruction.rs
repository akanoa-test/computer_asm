use crate::cpu::Bus;
use crate::data::constants::RamAddressBusSize;
use crate::data::register::Register;
use crate::parser::data::datum::{AddressDatum, ArithmeticDatum, JumpDatum, MoveDatum};

#[derive(Debug, PartialEq)]
pub enum Instruction {
    Move {
        register: Register,
        datum: MoveDatum,
    },
    Load {
        register: Register,
        datum: AddressDatum,
        section: Section,
    },
    Store {
        register: Register,
        datum: AddressDatum,
        section: Section,
    },
    Add {
        datum: ArithmeticDatum,
    },
    Add2 {
        register: Register,
        datum: ArithmeticDatum,
    },
    Sub {
        datum: ArithmeticDatum,
    },
    Sub2 {
        register: Register,
        datum: ArithmeticDatum,
    },
    Output {
        register: Register,
        bus: Bus,
    },
    Jump {
        datum: JumpDatum,
    },
    JumpNonZero {
        datum: JumpDatum,
    },
    JumpZero {
        datum: JumpDatum,
    },
    JumpNegative {
        datum: JumpDatum,
    },
    Call {
        label: String,
    },
    Push {
        register: Register,
    },
    Pop {
        register: Register,
    },
    Allocate {
        register: Register,
        amount: RamAddressBusSize,
    },
    Reallocate {
        register: Register,
        amount: RamAddressBusSize,
    },
    Free {
        address: AddressDatum,
    },
    Return,
    Halt,
    Stop,
    Nop,
}

use crate::data::instruction::Instruction as LibInstruction;
use crate::mmu::Section;

impl From<Instruction> for crate::data::instruction::Instruction {
    fn from(value: Instruction) -> Self {
        match value {
            Instruction::Move { register, datum } => LibInstruction::Move {
                register,
                datum: datum.into(),
            },
            Instruction::Load {
                register,
                datum,
                section,
            } => LibInstruction::Load {
                register,
                datum: datum.into(),
                section,
            },
            Instruction::Store {
                register,
                datum,
                section,
            } => LibInstruction::Store {
                register,
                datum: datum.into(),
                section,
            },
            Instruction::Add { datum } => LibInstruction::Add {
                datum: datum.into(),
            },
            Instruction::Add2 { datum, register } => LibInstruction::Add2 {
                register: register.into(),
                datum: datum.into(),
            },
            Instruction::Sub { datum } => LibInstruction::Sub {
                datum: datum.into(),
            },
            Instruction::Sub2 { datum, register } => LibInstruction::Sub2 {
                register: register.into(),
                datum: datum.into(),
            },
            Instruction::Output { register, bus } => LibInstruction::Output { register, bus },
            Instruction::Push { register } => LibInstruction::Push { register },
            Instruction::Pop { register } => LibInstruction::Pop { register },
            Instruction::Return => LibInstruction::Return,
            Instruction::Nop => LibInstruction::Nop,
            Instruction::Halt => LibInstruction::Halt,
            Instruction::Stop => LibInstruction::Stop,
            Instruction::Allocate { amount, register } => {
                LibInstruction::Allocate { amount, register }
            }
            Instruction::Reallocate { amount, register } => {
                LibInstruction::Reallocate { amount, register }
            }
            Instruction::Free { address } => LibInstruction::Free {
                address: address.into(),
            },
            Instruction::Call { .. }
            | Instruction::Jump { .. }
            | Instruction::JumpNonZero { .. }
            | Instruction::JumpNegative { .. }
            | Instruction::JumpZero { .. } => unreachable!("Not handle by into() {:?}", value),
        }
    }
}
