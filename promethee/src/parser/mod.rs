use crate::data::constants::RamAddressBusSize;
use crate::data::instruction::Instruction;
use crate::parser::data::datum::{AddressDatum, JumpDatum};
use crate::parser::data::instruction::Instruction as ParserInstruction;
use crate::parser::nom_parser::macros::segments::replace_alias;
use crate::parser::preprocessors::{
    preprocessor_cleanup_comment_blocks, preprocessor_cleanup_inline_comments,
    preprocessor_expand_call_macro, preprocessor_expand_load_store_macro,
    preprocessor_expand_move_macro, preprocessor_get_segments, preprocessor_normalize_segments,
};
use eyre::WrapErr;
use preprocessors::PreprocessorError;
use std::collections::HashMap;
use thiserror::Error;

mod data;
mod nom_parser;

mod preprocessors;

#[derive(Error, Debug)]
enum PostprocessorError {
    #[error("Unable to post process labels")]
    Labels,
}

#[derive(Error, Debug)]
enum ParserError {
    #[error("Nom error occurs: {0:?}")]
    Nom(nom::Err<nom::error::Error<String>>),
    #[error("A preprocessor error occurs : {0}")]
    Preprocessor(PreprocessorError),
}

pub fn parse_program(input: &str) -> eyre::Result<Vec<Instruction>> {
    let assembly_after_comment_blocks_cleanup = preprocessor_cleanup_comment_blocks(input)?;
    let assembly_after_inline_comment_cleanup =
        preprocessor_cleanup_inline_comments(&assembly_after_comment_blocks_cleanup)?;
    let assembly_after_segment_normalization =
        preprocessor_normalize_segments(&assembly_after_inline_comment_cleanup)?;
    let (code_segment, data_segments) =
        preprocessor_get_segments(&assembly_after_segment_normalization)?;
    let aliased_code = replace_alias(&code_segment, data_segments)?;
    // expand symbol
    let assembly_after_call_macro_expansion = preprocessor_expand_call_macro(&aliased_code)?;
    let assembly_after_storing_macro_expansion =
        preprocessor_expand_load_store_macro(&assembly_after_call_macro_expansion)?;
    let assembly_after_move_macro_expansion =
        preprocessor_expand_move_macro(&assembly_after_storing_macro_expansion)?;
    let (labels, program) = preprocessors::preprocessor_label(&assembly_after_move_macro_expansion)
        .wrap_err(PreprocessorError::Labels)?;
    let (_, instructions) = nom_parser::parse_program(&program)
        .map_err(|err| ParserError::Nom(err.to_owned()))
        .wrap_err("Unable to parse program")?;
    let converted_instructions =
        postprocessor(instructions, labels).wrap_err(PostprocessorError::Labels)?;
    Ok(converted_instructions)
}

fn postprocessor(
    instructions: Vec<ParserInstruction>,
    labels: HashMap<String, RamAddressBusSize>,
) -> eyre::Result<Vec<Instruction>> {
    let mut result = vec![];
    for instruction in instructions.into_iter() {
        match instruction {
            ParserInstruction::Jump { datum } => {
                let address = post_process_jump_labels(datum, &labels)?;
                let instruction = Instruction::Jump {
                    address: address.into(),
                };
                result.push(instruction)
            }
            ParserInstruction::JumpZero { datum } => {
                let address = post_process_jump_labels(datum, &labels)?;
                let instruction = Instruction::JumpZero {
                    address: address.into(),
                };
                result.push(instruction)
            }
            ParserInstruction::JumpNonZero { datum } => {
                let address = post_process_jump_labels(datum, &labels)?;
                let instruction = Instruction::JumpNonZero {
                    address: address.into(),
                };
                result.push(instruction)
            }
            ParserInstruction::JumpNegative { datum } => {
                let address = post_process_jump_labels(datum, &labels)?;
                let instruction = Instruction::JumpNegative {
                    address: address.into(),
                };
                result.push(instruction)
            }
            ParserInstruction::Call { label, .. } => {
                let datum = JumpDatum::Label(label);
                let address = post_process_jump_labels(datum, &labels)?;
                let instruction = Instruction::Call {
                    address: address.into(),
                };
                result.push(instruction)
            }
            instruction => result.push(instruction.into()),
        }
    }

    Ok(result)
}

fn post_process_jump_labels(
    jump_datum: JumpDatum,
    labels: &HashMap<String, RamAddressBusSize>,
) -> eyre::Result<AddressDatum> {
    let address = match jump_datum {
        JumpDatum::Address(address) => address,
        JumpDatum::Register(register) => return Ok(AddressDatum::Register(register)),
        JumpDatum::Label(label) => {
            let address = labels
                .get(&label)
                .ok_or(PreprocessorError::LabelNotFound(label.to_string()))
                .wrap_err("Unable to get the label from labels map")?;
            *address
        }
    };
    Ok(AddressDatum::Address(address))
}

#[cfg(test)]
mod tests {
    use crate::data::datum::{ArithmeticDatum, MoveDatum};
    use crate::data::instruction::Instruction;
    use crate::data::register::Register;
    use crate::parser::data::datum::AddressDatum;
    use crate::parser::parse_program;
    use crate::parser::preprocessors::preprocessor_label;
    use std::collections::HashMap;

    #[test]
    fn test_preprocessor_labels() {
        let assembly = r#"
           MOV R1, 1
           JNZ R1, 'branch
           JMP     'branch
halt:      HLT
branch:    MOV ACC, 1
           ADD R1
           JMP 'halt
    "#;

        let expected_assembly = r#"MOV R1, 1
JNZ R1, 'branch
JMP     'branch
HLT
MOV ACC, 1
ADD R1
JMP 'halt"#
            .to_string();

        let expected_labels_table =
            HashMap::from([("halt".to_string(), 3), ("branch".to_string(), 4)]);
        let result =
            preprocessor_label(assembly).expect("Must preprocess the assembly with labels");
        assert_eq!(result, (expected_labels_table, expected_assembly))
    }

    #[test]
    fn test_parse_with_preprocessor_with_data_segment() {
        let assembly = r#"
~code:
    MOV ACC, $age
    ADD $add
    SUB $value
    MOV R1, ACC
    PUSH R1
    STP
~data:
    age 0x45
    value 0b110
    add 10
    "#;
    }
    #[test]
    fn test_parse_with_preprocessor_without_data_segment() {
        let assembly = r#"
           MOV R1, 1
           JNZ 'branch
           JMP     'branch
           CALL 'f, ACC, R1, R2
halt:      HLT
branch:    MOV ACC, 1
           ADD R1
           JMP 'halt
f:         MOV R1, 0xff
           RET
    "#;
        let call_expanded_command_number = 6;
        let branch_macrofy_line = 5;
        let halt_mcrofy_line = 4;
        let f_macrofy_line = 8;
        let expected = vec![
            Instruction::Move {
                register: Register::R1,
                datum: MoveDatum::Value(1),
            },
            Instruction::JumpNonZero {
                address: AddressDatum::Address(branch_macrofy_line + call_expanded_command_number)
                    .into(),
            },
            Instruction::Jump {
                address: AddressDatum::Address(branch_macrofy_line + call_expanded_command_number)
                    .into(),
            },
            Instruction::Push {
                register: Register::Acc,
            },
            Instruction::Push {
                register: Register::R1,
            },
            Instruction::Push {
                register: Register::R2,
            },
            Instruction::Call {
                address: AddressDatum::Address(f_macrofy_line + call_expanded_command_number)
                    .into(),
            },
            Instruction::Pop {
                register: Register::R2,
            },
            Instruction::Pop {
                register: Register::R1,
            },
            Instruction::Pop {
                register: Register::Acc,
            },
            Instruction::Halt,
            Instruction::Move {
                register: Register::Acc,
                datum: MoveDatum::Value(1),
            },
            Instruction::Add {
                datum: ArithmeticDatum::Register(Register::R1),
            },
            Instruction::Jump {
                address: AddressDatum::Address(halt_mcrofy_line + call_expanded_command_number)
                    .into(),
            },
            Instruction::Move {
                register: Register::R1,
                datum: MoveDatum::Value(0xff),
            },
            Instruction::Return,
        ];

        let result = parse_program(assembly).expect("Must parse the program successfully");
        assert_eq!(result, expected)
    }

    #[test]
    fn replace_data_segment() {
        let assembly = r#"~code:
    start:  MOV R1, $age
            PUSH R1
            STP
~data:
    age 0x45
    name "test""#;

        let expected = vec![
            Instruction::Move {
                register: Register::R1,
                datum: MoveDatum::Value(0x45),
            },
            Instruction::Push {
                register: Register::R1,
            },
            Instruction::Stop,
        ];

        let result = parse_program(assembly).expect("Must parse the program successfully");
        assert_eq!(result, expected)
    }
}
