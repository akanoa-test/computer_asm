use crate::commands::Command;
use crate::cpu::{Cpu, Lane};
use crate::devices::disk::DiskDevice;
use crate::devices::Device;
use crate::events::Event;
use crate::mmu::Mmu;
use crate::program_registry::ProgramRegistry;
use crate::ram::Ram;
use crossbeam_channel::Receiver;
use futures_lite::Stream;
use std::thread;
use std::thread::{sleep, JoinHandle};
use std::time::Duration;

pub struct Computer {
    rx: Receiver<Event>,
}

impl Computer {
    pub fn new(
        // send_event_to_backend: Sender<Event>,
        rx_backend: Receiver<Command>,
    ) -> (Self, JoinHandle<eyre::Result<()>>) {
        let (tx, rx) = crossbeam_channel::unbounded::<Event>();

        let a = thread::spawn(move || {
            let tx_cpu = tx.clone();
            let tx_mmu = tx.clone();
            let tx_disk = tx.clone();
            let tx_registry = tx.clone();

            let mut memory = Ram::new();
            let mmu = Mmu::new(&mut memory, Some(tx_mmu));
            let mut dd = DiskDevice::default();
            dd.set_sender(tx_disk);
            let mut disk = Device::Disk(dd);

            let programs_registry = ProgramRegistry::new(tx_registry);

            let mut cpu = Cpu::new(mmu, tx_cpu, rx_backend, programs_registry);
            cpu.set_device(Lane::Lane0, &mut disk);

            sleep(Duration::from_secs(2));

            // cpu.register_program("write_dd", "p1")?;
            // cpu.register_program("write_dd2", "p2")?;
            // cpu.register_program("add", "p3")?;
            // cpu.register_program("ref_array_mutation", "p4")?;
            // cpu.register_program("allocate", "p5")?;
            // cpu.register_program("call_function", "p6")?;
            // cpu.register_program("data", "p7")?;

            // cpu.exec_program("p1")?;
            // cpu.exec_program("p2")?;
            // cpu.exec_program("p3")?;
            // cpu.exec_program("p5")?;
            //cpu.exec_program("p7")?;
            cpu.load_programs_from_path("./programs2")?;

            cpu.run()
        });

        (Computer { rx }, a)
    }

    pub fn run(&mut self) -> impl Stream<Item = Event> + '_ {
        async_stream::stream! {
            for event in self.rx.iter() {

                yield event;
            }
        }
    }
}
