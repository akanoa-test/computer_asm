use std::collections::{HashMap, VecDeque};

use serde::Serialize;
use strum::AsRefStr;

use crate::data::constants::DeviceAddressBusSize;
use crate::data::memory::HeapBlock;
use crate::data::register::Register;
use crate::mmu::{Block, Section};
use crate::process::ProcessDebugData;
use crate::program_registry::ProgramEvent;

#[derive(Serialize, Clone, Debug)]
#[serde(rename_all = "snake_case")]
pub enum PeripheralEvent {
    RegisterUpdate {
        register: String,
        value: usize,
    },
    DiskWrite {
        address: DeviceAddressBusSize,
        data: u32,
    },
}

#[derive(Serialize, Clone, Debug)]
#[serde(rename_all = "snake_case")]
pub enum StackEvent {
    Push(usize),
    Pop,
    Update { index: usize, value: usize },
}

#[derive(Serialize, Clone, Debug)]
#[serde(rename_all = "snake_case")]
pub enum HeapEvent {
    HeapState(Vec<HeapBlock>),
}

#[derive(Serialize, Clone, Debug)]
#[serde(rename_all = "snake_case")]
pub enum MmuEvent {
    Reserve {
        blocks: VecDeque<Block>,
        section: Section,
    },
    Update {
        sections: HashMap<Section, VecDeque<Block>>,
    },
}

#[derive(Serialize, Clone, Debug)]
#[serde(rename_all = "snake_case")]
pub enum ProcessManagement {
    Add(ProcessDebugData),
    Execution(usize),
    Stack(StackEvent),
    Heap(HeapEvent),
    Register { register: Register, value: usize },
    Flags { zero: bool, negative: bool },
    Mmu(MmuEvent),
    Killed,
}

#[derive(Serialize, Clone, Debug)]
#[serde(rename_all = "snake_case")]
pub enum ProgramManagement {
    Init {
        programs: HashMap<String, ProgramEvent>,
    },
}

#[derive(Serialize, Clone, Debug)]
#[serde(rename_all = "snake_case")]
#[derive(AsRefStr)]
#[strum(serialize_all = "snake_case")]
pub enum Event {
    Tick(usize),
    Peripheral(PeripheralEvent),
    SwitchContext(usize),
    Program(ProgramManagement),
    Process {
        pid: usize,
        event: ProcessManagement,
    },
}
