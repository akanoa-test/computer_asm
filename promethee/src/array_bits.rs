/*
MOV R1, 4 => 0000 0001 01 000100
opcode = 0
register = 1
modifier = 1
value = 4

opcode << X | register << Y | modifier << Z | ( value & 0b111_111 )

 0b000100
&
 0b111111
 --------
 0b000100
 */
#[test]
fn build_data() {
    let opcode = 0b100;
    let register = 5;
    let modifier = 3;
    let value = 12;
    // 0b1000001

    let v: u16 = (opcode << 12) | (register << 8) | (modifier << 6) | (value & 0b111_111);

    let op = v >> 12;
    let reg = (v >> 8) & 0b1111;
    let modi = (v >> 6) & 0b11;
    let val = v & 0b111_111;

    dbg!(format!("{:016b}", v));
    dbg!(format!("{:04b}", op));
    dbg!(format!("{:04b}", reg));
    dbg!(format!("{:02b}", modi));
    dbg!(format!("{:06b}", val));
}
