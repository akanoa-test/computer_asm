use crate::data::constants::{BusSize, DeviceAddressBusSize, DeviceDataBusSize};
use crate::devices::{DeviceBehavior, DeviceError};
use crate::events::{Event, PeripheralEvent};
use crossbeam_channel::Sender;
use nom::ToUsize;
use num_enum::TryFromPrimitive;
use std::collections::{HashMap, HashSet};
use strum::Display;

const HALF_REGISTER_SIZE: u32 = 16;

type DiskMemorySize = u32;

// X0 (16 bits)
// 0 0 0
// ^ ^ ^
// | | |- low or high register Xh / Xl
// | |- RW => R = 0 && W = 1
// |- Enable = 1
//
// X1 (16 bit) address
//
// X2 (32 bits)
//
// X2h               X2l
// 00000000000000000 00000000000000000
//
//                      X0
//           control (RW/Enable)
// |-----|------------------------------>|------------|
// | CPU |        X1 Address             |     DD     |
// |     |------------------------------>|            |
// |     |                               |  X0 X1 X2  |
// |-----|<----------------------------->|------------|
//                data (16 bits)
//                     X2

fn get_flag(bytes: &u16, index: usize) -> u8 {
    ((bytes >> index) & 1) as u8
}

/**
bytes & ~(1 << (index - 1))

bytes | (1 << (index - 1));
*/
fn set_flag(bytes: &mut u16, index: usize, state: bool) {
    let mask = (1 << (index - 1)) as u16;
    if state {
        *bytes |= mask;
    } else {
        *bytes &= !mask;
    }
}

#[derive(TryFromPrimitive, Clone, Copy, Display)]
#[repr(u8)]
#[strum(ascii_case_insensitive)]
#[strum(serialize_all = "UPPERCASE")]
enum Register {
    X0 = 0,
    X1,
    X2,
}

#[derive(TryFromPrimitive)]
#[repr(u8)]
enum Mode {
    Read = 0,
    Write,
}

#[derive(TryFromPrimitive, Debug)]
#[repr(u8)]
enum Enable {
    Off = 0,
    On,
}

#[derive(TryFromPrimitive)]
#[repr(u8)]
enum Significance {
    Low = 0,
    High,
}

#[derive(Default)]
pub struct DiskDevice {
    send_event_to_backend: Option<Sender<Event>>,
    data_buffer: DeviceDataBusSize,
    address_buffer: DeviceDataBusSize,
    control: DeviceDataBusSize,
    data: HashMap<DeviceAddressBusSize, DiskMemorySize>,
    watched_addresses: HashSet<DeviceAddressBusSize>,
}

impl DiskDevice {
    pub fn set_sender(&mut self, sender: Sender<Event>) {
        self.send_event_to_backend = Some(sender)
    }

    fn send_event(&self, event: Event) -> Result<(), DeviceError> {
        if let Some(sender) = self.send_event_to_backend.as_ref() {
            sender.send(event).map_err(DeviceError::DebugSend)?
        }
        Ok(())
    }
}

impl DeviceBehavior for DiskDevice {
    fn update(&mut self) -> Result<(), DeviceError> {
        let enable = Enable::try_from(get_flag(&self.control, 2))
            .map_err(|_| DeviceError::RunError("Unable to get Enable flag".to_string()))?;

        if let Enable::Off = &enable {
            //println!("Nothing to do skip");
            return Ok(());
        }

        let mode = Mode::try_from(get_flag(&self.control, 1))
            .map_err(|_| DeviceError::RunError("Unable to get Mode flag".to_string()))?;

        let signifiance = Significance::try_from(get_flag(&self.control, 0))
            .map_err(|_| DeviceError::RunError("Unable to get Significance flag".to_string()))?;

        match mode {
            Mode::Read => todo!(),
            Mode::Write => {
                let address = self.address_buffer;

                let data = match signifiance {
                    Significance::Low => self.data_buffer as DiskMemorySize,
                    Significance::High => {
                        ((self.data_buffer as u32) << HALF_REGISTER_SIZE) as DiskMemorySize
                    }
                };

                //println!("  -- Writing data on disk --");

                self.send_event(Event::Peripheral(PeripheralEvent::DiskWrite {
                    address,
                    data,
                }))?;

                self.data.insert(address, data as DiskMemorySize);
                self.watched_addresses.insert(address);
                set_flag(&mut self.control, 3, false);
            }
        }

        Ok(())
    }

    fn write_register(
        &mut self,
        index: BusSize,
        data: &DeviceDataBusSize,
    ) -> Result<(), DeviceError> {
        let register =
            Register::try_from(index).map_err(|_| DeviceError::RegisterNotFound(index))?;

        self.send_event(Event::Peripheral(PeripheralEvent::RegisterUpdate {
            register: register.to_string(),
            value: data.to_usize(),
        }))?;

        match register {
            Register::X0 => self.control = *data,
            Register::X1 => self.address_buffer = *data as DeviceAddressBusSize,
            Register::X2 => self.data_buffer = *data,
        }

        Ok(())
    }

    fn print_state(&self) {
        println!("Data Drive");
        println!("  X0 : {:03b}", self.control);
        println!("  X1 : {:016b}", self.address_buffer);
        println!("  X2 : {:032b}", self.data_buffer);
        println!("  Data:");
        for address in &self.watched_addresses {
            println!(
                "       @{} => {:032b}",
                address,
                self.data.get(address).unwrap_or(&0)
            );
        }
    }
}
