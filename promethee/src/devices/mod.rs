use crate::data::constants::{BusSize, DeviceDataBusSize};
use crate::events::Event;
use crossbeam_channel::SendError;
use disk::DiskDevice;
use enum_dispatch::enum_dispatch;
use gpu::GpuDevice;
use thiserror::Error;

pub mod disk;
pub mod gpu;

#[derive(Debug, Error)]
pub enum DeviceError {
    #[error("The register {0} can't be found")]
    RegisterNotFound(BusSize),
    #[error("An error occurs during device run : {0}")]
    RunError(String),
    #[error("Unable to send debug : {0:?}")]
    DebugSend(SendError<Event>),
}

#[enum_dispatch(DeviceBehavior)]
pub enum Device {
    Disk(DiskDevice),
    Gpu(GpuDevice),
}

#[enum_dispatch]
pub trait DeviceBehavior {
    fn update(&mut self) -> Result<(), DeviceError>;
    fn write_register(
        &mut self,
        index: BusSize,
        data: &DeviceDataBusSize,
    ) -> Result<(), DeviceError>;
    fn print_state(&self);
}
