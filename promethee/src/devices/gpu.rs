use crate::data::constants::{BusSize, DeviceDataBusSize};
use crate::devices::{DeviceBehavior, DeviceError};

pub struct GpuDevice;

impl DeviceBehavior for GpuDevice {
    fn update(&mut self) -> Result<(), DeviceError> {
        //println!("GPU update");
        Ok(())
    }

    fn write_register(
        &mut self,
        _index: BusSize,
        _data: &DeviceDataBusSize,
    ) -> Result<(), DeviceError> {
        todo!()
    }

    fn print_state(&self) {
        println!("GPU")
    }
}
