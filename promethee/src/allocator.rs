use crate::data::constants::{RamAddressBusSize, BLOCK_SIZE};
use crate::data::memory::Header;
use crate::mmu::{Mmu, Section};
use eyre::eyre;
use log::debug;
use std::marker::PhantomData;

pub struct Allocator<'a> {
    _marker: PhantomData<&'a ()>,
}

impl<'a> Allocator<'a> {
    pub fn new() -> Self {
        Self {
            _marker: PhantomData,
        }
    }
}

struct HeapScanner<'a> {
    mmu: &'a Mmu<'a>,
    current_address_block: RamAddressBusSize,
}

impl Iterator for HeapScanner<'_> {
    type Item = eyre::Result<(Header, RamAddressBusSize)>;

    fn next(&mut self) -> Option<Self::Item> {
        let current_header: Header = self
            .mmu
            .load(&self.current_address_block, &Section::Heap)
            .ok()?
            .into();
        match current_header.size {
            0 => None,
            _ => {
                let actual_block_address = self.current_address_block;
                self.current_address_block += current_header.size as RamAddressBusSize;
                Some(Ok((current_header, actual_block_address)))
            }
        }
    }
}

// Split a block in two blocks
fn split(
    cur: RamAddressBusSize,
    needed: u32,
    actual_header: &Header,
) -> Vec<(RamAddressBusSize, Header)> {
    let next_block_address = cur + needed as RamAddressBusSize;
    let delta = actual_header.size - needed;

    vec![
        // Create the first block with the needed size
        (
            cur,
            Header {
                size: needed,
                state: true,
            },
        ),
        (
            next_block_address,
            Header {
                size: delta,
                state: false,
            },
        ),
    ]
}

/// Merge as many blocks as needed
fn merge(
    mmu: &mut Mmu,
    cur: RamAddressBusSize,
    needed: u32,
) -> Option<Vec<(RamAddressBusSize, Header)>> {
    let mut accumulated_size = 0;
    let mut blocks_acc = vec![];

    let mut scanner = HeapScanner {
        mmu,
        current_address_block: cur,
    };

    let mut index = 0;

    while let Some(Ok((header, current_address))) = scanner.next() {
        if index > 0 && header.state {
            return None;
        }

        accumulated_size += header.size;
        blocks_acc.push((header, current_address));
        index += 1;
        if accumulated_size >= needed {
            break;
        }
    }

    let (to_split_header, to_split_address) =
        blocks_acc.pop().expect("Unable to pop block to split");
    let already_reclaimed_space: u32 = blocks_acc
        .iter()
        .map(|(block_header, _)| block_header.size)
        .sum();
    let left_to_reclaim_space = needed - already_reclaimed_space;
    let mut split_block = split(to_split_address, left_to_reclaim_space, &to_split_header);
    let end_block = split_block.pop().expect("Unable to pop split block ");
    let start_block = (
        cur,
        Header {
            size: needed,
            state: true,
        },
    );

    Some(match end_block {
        (_, header) if header.size == 0 => {
            vec![start_block]
        }
        _ => vec![start_block, end_block],
    })
}

fn check_for_orphan_next_block(scanner: &mut HeapScanner<'_>) -> eyre::Result<bool> {
    match scanner.next() {
        None => Ok(false),
        Some(next) => {
            let (next_header, _) = next?;
            if next_header.size == 1 {
                return Ok(true);
            }
            Ok(false)
        }
    }
}

impl<'a> Allocator<'a> {
    pub fn reallocate(
        &self,
        mmu: &mut Mmu<'a>,
        pointer: RamAddressBusSize,
        amount: RamAddressBusSize,
    ) -> eyre::Result<RamAddressBusSize> {
        let result = merge(mmu, pointer - 1, amount as u32 + 1);

        Ok(match result {
            None => {
                // we must relocate data elsewhere
                let new_pointer = self.allocate(mmu, amount)?;

                let block_header_raw =
                    *mmu.load_or_init(&(pointer - 1), &Section::Heap, Header::default().into())?;
                let mut source_header: Header = block_header_raw.into();

                // copy previous block to new one
                for i in 0..(source_header.size - 1) as u16 {
                    let source_data = mmu.load(&(pointer + i), &Section::Heap)?;
                    mmu.store(&new_pointer + i, *source_data, &Section::Heap)?;
                }

                // free previous block
                source_header.state = false;
                mmu.store(pointer - 1, source_header.into(), &Section::Heap)?;

                new_pointer
            }
            Some(headers) => {
                for (address, header) in headers {
                    mmu.store(address, header.into(), &Section::Heap)?;
                }
                pointer
            }
        })
    }

    pub fn allocate(
        &self,
        mmu: &mut Mmu<'a>,
        amount: RamAddressBusSize,
    ) -> eyre::Result<RamAddressBusSize> {
        let heap_size = mmu.get_heap_size()?;
        // todo : repair the growing algorithm
        if heap_size < amount {
            let nb = amount.div_ceil(BLOCK_SIZE);
            mmu.grow(nb as usize)?;
        }

        let needed = amount as RamAddressBusSize + 1;

        // Search a range of blocks greater or equal to the needed amount
        // If not found, create a new block at the end of the memory
        // If found the cursor is ensured to point to a block span large enough
        let mut cur: RamAddressBusSize = 0;
        'outer: loop {
            // Init with the first block
            let mut block_pointer = cur;
            let mut acc = 0;
            // Search for address available
            while acc < needed {
                // read the current block
                let block_header_raw =
                    *mmu.load_or_init(&block_pointer, &Section::Heap, Header::default().into())?;
                let block_header: Header = block_header_raw.into();
                debug!("{block_header:?}");

                if block_header.size == 0 {
                    // If the block is virtual
                    // then use it
                    cur = block_pointer;
                    break 'outer;
                } else if !block_header.state {
                    // if the block is free
                    // get the available size of the block
                    let free_size = block_header.size as RamAddressBusSize;
                    acc += free_size;
                    block_pointer += free_size;
                } else {
                    cur = block_pointer + block_header.size as RamAddressBusSize;
                    continue 'outer;
                }
            }
            if cur > acc {
                cur -= acc;
            }
            break;
        }

        #[derive(Debug)]
        enum BlockSize {
            // Not real block, just the end of the known memory
            Virtual,
            // The block will be split
            Greater,
            // The block will be merged with the next or more than the next block
            Lower,
            // The block fit the required size
            Equal,
        }

        let mut old_header: Header = mmu.load_or_init(&cur, &Section::Heap, 0)?.into();

        let block_size = match old_header.size as u16 {
            0 => BlockSize::Virtual,
            old_header_size if old_header_size == needed => BlockSize::Equal,
            old_header_size if old_header_size < needed => BlockSize::Lower,
            _ => BlockSize::Greater,
        };

        let headers = match block_size {
            BlockSize::Virtual => {
                // We are at the end of the memory, we can create the block at the required size
                vec![(
                    cur,
                    Header {
                        size: needed as u32,
                        state: true,
                    },
                )]
            }
            BlockSize::Equal => {
                let mut scanner = HeapScanner {
                    mmu,
                    current_address_block: cur,
                };

                // Reuse the actual block
                old_header.size = needed as u32 + check_for_orphan_next_block(&mut scanner)? as u32;
                old_header.state = true;

                vec![(cur, old_header)]
            }
            BlockSize::Greater => {
                // The block is greater and can be split in two least blocks

                // We calculate the next block address by adding the current pointer with the needed
                // size. We are safe to do this, because the working span is guaranteed of being at least the needed size
                let delta = old_header.size - needed as u32;

                match delta {
                    // If the split result create an orphan block, don't split the block
                    1 => vec![(
                        cur,
                        Header {
                            size: needed as u32 + delta,
                            state: true,
                        },
                    )],
                    _ => {
                        let next_block_address = cur + needed as RamAddressBusSize;
                        vec![
                            (
                                cur,
                                Header {
                                    size: needed as u32,
                                    state: true,
                                },
                            ),
                            (
                                next_block_address,
                                Header {
                                    size: delta,
                                    state: false,
                                },
                            ),
                        ]
                    }
                }
            }
            BlockSize::Lower => {
                // Must merge as many blocks as needed
                // the working span is ensured to be as large as needed and all
                // blocks in the span are guaranteed to be free
                merge(mmu, cur, needed as u32).ok_or(eyre!("Unable to merge non free blocks"))?
            }
        };

        for (address, header) in headers {
            mmu.store(address, header.into(), &Section::Heap)?;
        }

        Ok(cur + 1)
    }

    pub fn free(&self, mmu: &mut Mmu, address: &RamAddressBusSize) -> eyre::Result<()> {
        let address = address - 1;

        let header = *mmu.load_or_init(&address, &Section::Heap, Header::default().into())?;
        let mut header: Header = header.into();
        header.state = false;
        mmu.store(address, header.into(), &Section::Heap)
    }
}

#[cfg(test)]
mod tests {
    use crate::allocator::Allocator;
    use crate::data::constants::RamDataBusSize;
    use crate::data::memory::{format_data, Header};
    use crate::mmu::{Mmu, Section};
    use crate::ram::Ram;

    #[test]
    fn block_size_2_close() {
        let header = Header {
            size: 2,
            state: true,
        };
        let data: RamDataBusSize = header.clone().into();
        let result: Header = data.into();
        assert_eq!(result, header)
    }

    #[test]
    fn block_size_2_open() {
        let header = Header {
            size: 2,
            state: false,
        };
        let data: RamDataBusSize = header.clone().into();
        let result: Header = data.into();
        assert_eq!(result, header)
    }

    macro_rules! check_alloc {
        ($mmu:expr, expected pointer: $expected_ptr:expr, pointer: $ptr:expr, $header:expr) => {{
            let header_address = *$mmu
                .load(&($ptr - 1), &Section::Heap)
                .expect("Should get the block header");

            let header: Header = header_address.into();

            assert_eq!($ptr, $expected_ptr);
            assert_eq!(header, $header);
        }};
    }

    macro_rules! check_memory {
        ($mmu:expr, [$(address: $address:expr, $header:expr);*]) => {
            $(
                let header_raw_data = *$mmu
                    .load_or_init(&$address, &Section::Heap, 0)
                    .expect("Should get the block header");

                let header: Header = header_raw_data.into();
                assert_eq!(header, $header);
            )*
        };
        ($mmu:expr, [$(address: $address:expr, data: $data:expr, $header:expr);*]) => {
            $(
                let header_raw_data = *$mmu
                    .load_or_init(&$address, &Section::Heap, 0)
                    .expect("Should get the block header");

                let header: Header = header_raw_data.into();
                assert_eq!(header, $header);

                if header.size > 1 {

                    let scan = $mmu
                        .scan_block(&header, $address as u16 + 1)
                        .expect("unable to scan the block");

                    scan.iter().zip($data).for_each(|(actual, expected)| {

                        assert_eq!(
                            actual.data,
                            expected,
                            "   actual {} \n expected {} \n scan: {scan:?}",
                            format_data(&actual.data),
                            format_data(&expected)
                        )
                    });
                }

            )*
        };
    }

    #[test]
    fn allocate_blocks() {
        env_logger::init();
        let mut ram = Ram::new();
        let mut mmu = Mmu::new(&mut ram, None);
        mmu.add_context(1, 35).expect("Should init context");
        mmu.set_context(1);
        let allocator = Allocator::new();
        let address1 = allocator
            .allocate(&mut mmu, 2)
            .expect("Must allocate 2 block");

        let address2 = allocator
            .allocate(&mut mmu, 3)
            .expect("Must allocate 3 block");

        let address3 = allocator
            .allocate(&mut mmu, 1)
            .expect("Must allocate 1 block");

        let address4 = allocator
            .allocate(&mut mmu, 5)
            .expect("Must allocate 5 block");

        check_alloc!(mmu, expected pointer: 1, pointer: address1, Header {
            size: 3,
            state: true
        });

        check_alloc!(mmu, expected pointer: 4, pointer: address2, Header {
            size: 4,
            state: true
        });

        check_alloc!(mmu, expected pointer: 8, pointer: address3, Header {
            size: 2,
            state: true
        });

        check_alloc!(mmu, expected pointer: 10, pointer: address4, Header {
            size: 6,
            state: true
        });
    }

    #[test]
    fn free_blocks() {
        // create a context
        // allocate 2 blocks => ptr1
        // allocate 3 blocks => ptr2
        // allocate 1 block  => ptr3
        // allocate 5 blocks => ptr4
        // free ptr2
        // allocate 3 blocks => ptr5 and ptr2 == ptr5
        // allocate 8 blocks => ptr6
        let mut ram = Ram::new();
        let mut mmu = Mmu::new(&mut ram, None);
        mmu.add_context(1, 35).expect("Should init context");
        mmu.set_context(1);
        let allocator = Allocator::new();
        let address1 = allocator
            .allocate(&mut mmu, 2)
            .expect("Must allocate 2 block");

        let address2 = allocator
            .allocate(&mut mmu, 3)
            .expect("Must allocate 3 block");

        let address3 = allocator
            .allocate(&mut mmu, 1)
            .expect("Must allocate 1 block");

        let address4 = allocator
            .allocate(&mut mmu, 5)
            .expect("Must allocate 5 block");

        allocator
            .free(&mut mmu, &address2)
            .expect("Should free block 2");

        check_alloc!(mmu, expected pointer: 1, pointer: address1, Header {
            size: 3,
            state: true
        });

        check_alloc!(mmu, expected pointer: 4, pointer: address2, Header {
            size: 4,
            state: false
        });

        check_alloc!(mmu, expected pointer: 8, pointer: address3, Header {
            size:2,
            state: true
        });

        check_alloc!(mmu, expected pointer: 10, pointer: address4, Header {
            size: 6,
            state: true
        });

        let address5 = allocator
            .allocate(&mut mmu, 3)
            .expect("Must allocate 3 block");

        assert_eq!(address2, address5, "Should allocate the free block");
        check_alloc!(mmu, expected pointer: 4, pointer: address5, Header {
            size: 4,
            state: true
        });

        allocator
            .free(&mut mmu, &address2)
            .expect("Should free block 2");

        let address6 = allocator
            .allocate(&mut mmu, 8)
            .expect("Must allocate 8 block");

        check_alloc!(mmu, expected pointer: 16, pointer: address6, Header {
            size: 9,
            state: true
        });
    }

    #[test]
    fn test_reuse_old_blocks() {
        // create a context
        // allocate 4 blocks => ptr1
        // store at ptr1
        // free ptr1
        // allocate 2 blocks => ptr2 and ptr1 == ptr2 and a free block have been created
        // allocate 4 blocks => ptr3
        let mut ram = Ram::new();
        let mut mmu = Mmu::new(&mut ram, None);
        mmu.add_context(1, 35).expect("Should init context");
        mmu.set_context(1);
        let allocator = Allocator::new();
        let address1 = allocator
            .allocate(&mut mmu, 4)
            .expect("Must allocate 4 block");

        mmu.store(address1, 255, &Section::Heap)
            .expect("Should store data");

        allocator
            .free(&mut mmu, &address1)
            .expect("Should free block");

        check_alloc!(mmu, expected pointer: 1, pointer: address1, Header {
            size: 5,
            state: false
        });

        let address2 = allocator
            .allocate(&mut mmu, 2)
            .expect("Must allocate 2 block");

        assert_eq!(address1, address2);
        check_alloc!(mmu, expected pointer: 1, pointer: address2, Header {
            size: 3,
            state: true
        });

        let address3 = allocator
            .allocate(&mut mmu, 4)
            .expect("Must allocate 4 block");

        check_alloc!(mmu, expected pointer: 1, pointer: address2, Header {
            size: 3,
            state: true
        });

        check_alloc!(mmu, expected pointer: 4, pointer: address2 + 3, Header {
            size: 2,
            state: false
        });

        check_alloc!(mmu, expected pointer: 6, pointer: address3, Header {
            size: 5,
            state: true
        });
    }

    #[test]
    fn test_reuse_old_blocks2() {
        // create a context
        // allocate 5 blocks => ptr1
        // store at ptr1
        // store at ptr1 + 1
        // free ptr1
        // allocate 3 blocks => ptr2 and ptr1 == ptr2 and a free block have been created
        // allocate 4 blocks => ptr3
        let mut ram = Ram::new();
        let mut mmu = Mmu::new(&mut ram, None);
        mmu.add_context(1, 35).expect("Should init context");
        mmu.set_context(1);
        let allocator = Allocator::new();
        let address1 = allocator
            .allocate(&mut mmu, 5)
            .expect("Must allocate 5 block");

        mmu.store(address1, 0x1, &Section::Heap)
            .expect("Should store data");

        mmu.store(address1 + 1, 0x2, &Section::Heap)
            .expect("Should store data");

        allocator
            .free(&mut mmu, &address1)
            .expect("Should free block");

        check_alloc!(mmu, expected pointer: 1, pointer: address1, Header {
            size: 6,
            state: false
        });

        let address2 = allocator
            .allocate(&mut mmu, 3)
            .expect("Must allocate 3 block");

        assert_eq!(address1, address2, "Should reuse block");
        check_alloc!(mmu, expected pointer: 1, pointer: address2, Header {
            size: 4,
            state: true
        });
        check_alloc!(mmu, expected pointer: 5, pointer: address2 + 4, Header {
            size: 2,
            state: false
        });

        mmu.store(address2, 0x4, &Section::Heap)
            .expect("Should store data");

        mmu.store(address2 + 2, 0x6, &Section::Heap)
            .expect("Should store data");
    }

    #[test]
    fn test_use_previous_block_to_create_a_new_one() {
        // create a context
        // allocate 5 blocks  => ptr1
        // allocate 10 blocks => ptr2
        // free ptr1
        // allocate 3 blocks => ptr1
        // free ptr1
        // free ptr2
        // allocate 11 blocks => ptr1
        let mut ram = Ram::new();
        let mut mmu = Mmu::new(&mut ram, None);
        mmu.add_context(1, 35).expect("Should init context");
        mmu.set_context(1);
        let allocator = Allocator::new();

        // allocate 5 blocks  => ptr1
        let ptr1 = allocator
            .allocate(&mut mmu, 5)
            .expect("Unable to allocate 5 blocks of data");

        check_memory!(mmu, [
            address: 0,
            Header {
                size: 6,
                state: true
            };
            address: 6,
            Header {
                size:0,
                state: false
            }
        ]);

        // allocate 10 blocks => ptr2
        let ptr2 = allocator
            .allocate(&mut mmu, 10)
            .expect("Unable to allocate 10 blocks of data");

        check_memory!(mmu, [
            address: 0,
            Header {
                size: 6,
                state: true
            };
            address: 6,
            Header {
                size:11,
                state: true
            }
        ]);

        // free ptr1
        allocator
            .free(&mut mmu, &ptr1)
            .expect("Unable to free ptr1 blocks[5]");

        check_memory!(mmu, [
            // Only the flag has been updated
            address: 0,
            Header {
                size: 6,
                state: false
            };
            // The next block is unchanged
            address: 6,
            Header {
                size:11,
                state: true
            }
        ]);

        // allocate 3 blocks => ptr1
        let ptr1 = allocator
            .allocate(&mut mmu, 3)
            .expect("Unable tp allocate 3 blocks of data");

        check_memory!(mmu, [
           // Split the previous 6 block in 4 + 2 blocks
            address: 0,
            Header {
                size: 4,
                state: true
            };
            address: 4,
            Header {
                size: 2,
                state: false
            };
            // The next block is unchanged
            address: 6,
            Header {
                size:11,
                state: true
            }
        ]);

        // free ptr1
        allocator
            .free(&mut mmu, &ptr1)
            .expect("Unable to free ptr1 blocks[3]");

        check_memory!(mmu, [
           // Free first block
            address: 0,
            Header {
                size: 4,
                state: false
            };
            address: 4,
            Header {
                size: 2,
                state: false
            };
            // The next block is unchanged
            address: 6,
            Header {
                size:11,
                state: true
            }
        ]);

        // free ptr2
        allocator
            .free(&mut mmu, &ptr2)
            .expect("Unable to free ptr1 blocks[10]");

        check_memory!(mmu, [
            address: 0,
            Header {
                size: 4,
                state: false
            };
            address: 4,
            Header {
                size: 2,
                state: false
            };
            // Free second block
            address: 6,
            Header {
                size:11,
                state: false
            }
        ]);

        // allocate 11 blocks => ptr1
        let ptr1 = allocator
            .allocate(&mut mmu, 11)
            .expect("Unable tp allocate 11 blocks of data");

        check_memory!(mmu, [
            address: 0,
            Header {
                size: 12,
                state: true
            };
            // Free second block
            address: 12,
            Header {
                size:5,
                state: false
            }
        ]);
    }

    #[test]
    fn test_should_not_left_orphan_blocks() {
        // create a context
        // allocate 4 blocks => ptr1
        // store at ptr1
        // free ptr1
        // allocate 3 blocks => ptr2 and ptr1 == ptr2 but the real allocation will be 4
        // allocate 4 blocks => ptr3
        let mut ram = Ram::new();
        let mut mmu = Mmu::new(&mut ram, None);
        mmu.add_context(1, 35).expect("Should init context");
        mmu.set_context(1);
        let allocator = Allocator::new();
        let address1 = allocator
            .allocate(&mut mmu, 4)
            .expect("Must allocate 4 block");

        mmu.store(address1, 255, &Section::Heap)
            .expect("Should store data");

        allocator
            .free(&mut mmu, &address1)
            .expect("Should free block");

        check_alloc!(mmu, expected pointer: 1, pointer: address1, Header {
            size: 5,
            state: false
        });

        let address2 = allocator
            .allocate(&mut mmu, 3)
            .expect("Must allocate 3 block");

        assert_eq!(
            address1, address2,
            "Must reuse the block but not left orphan"
        );
        check_alloc!(mmu, expected pointer: 1, pointer: address2, Header {
            size: 5,
            state: true
        });

        let address3 = allocator
            .allocate(&mut mmu, 4)
            .expect("Must allocate 4 block");

        check_alloc!(mmu, expected pointer: 1, pointer: address2, Header {
            size: 5,
            state: true
        });

        check_alloc!(mmu, expected pointer: 6, pointer: address3, Header {
            size: 5,
            state: true
        });
    }

    #[test]
    fn reallocation_no_copy() {
        // create context
        // allocate 5 blocks => ptr1
        // allocate 2 blocks => ptr2
        // free ptr1
        // allocate 3 => ptr1
        // write some data at:
        //   ptr1   => 1
        //   ptr1+1 => 2
        //   ptr1+2 => 3
        // realloc 5 => ptr1

        let mut ram = Ram::new();
        let mut mmu = Mmu::new(&mut ram, None);
        mmu.add_context(1, 35).expect("Should init context");
        mmu.set_context(1);
        let allocator = Allocator::new();

        // allocate 5 blocks  => ptr1
        let ptr1 = allocator
            .allocate(&mut mmu, 5)
            .expect("Unable to allocate 5 blocks of data");

        check_memory!(mmu, [
            address: 0,
            data: vec![0,0,0,0,0,0],
            Header {
                size: 6,
                state: true
            };
            address: 6,
            data: vec![],
            Header {
                size:0,
                state: false
            }
        ]);

        // allocate 2 blocks  => ptr2
        let ptr2 = allocator
            .allocate(&mut mmu, 2)
            .expect("Unable to allocate 2 blocks of data");

        check_memory!(mmu, [
            address: 0,
            data: vec![0,0,0,0,0,0],
            Header {
                size: 6,
                state: true
            };
            address: 6,
            data: vec![0,0],
            Header {
                size: 3,
                state: true
            };
            address: 9,
            data: vec![],
            Header {
                size:0,
                state: false
            }
        ]);

        // free ptr1
        allocator
            .free(&mut mmu, &ptr1)
            .expect("Unable to free ptr1");

        check_memory!(mmu, [
            address: 0,
            data: vec![0,0,0,0,0,0],
            Header {
                size: 6,
                state: false
            };
            address: 6,
            data: vec![0,0],
            Header {
                size: 3,
                state: true
            };
            address: 9,
            data: vec![],
            Header {
                size:0,
                state: false
            }
        ]);

        // allocate 3 blocks  => ptr1
        let ptr1 = allocator
            .allocate(&mut mmu, 3)
            .expect("Unable to allocate 3 blocks of data");

        // write some data at:
        //   ptr1   => 1
        mmu.store(ptr1, 1, &Section::Heap)
            .expect("Unable to store at ptr1[0]");
        //   ptr1+1 => 2
        mmu.store(ptr1 + 1, 2, &Section::Heap)
            .expect("Unable to store at ptr1[1]");
        //   ptr1+2 => 3
        mmu.store(ptr1 + 2, 3, &Section::Heap)
            .expect("Unable to store at ptr1[2]");

        check_memory!(mmu, [
            address: 0,
            data: vec![1,2,3],
            Header {
                size: 4,
                state: true
            };
            address: 4,
            data: vec![0],
            Header {
                size: 2,
                state: false
            };
            address: 6,
            data: vec![0,0],
            Header {
                size: 3,
                state: true
            };
            address: 9,
            data: vec![],
            Header {
                size:0,
                state: false
            }
        ]);

        // realloc 5 blocks => ptr1
        let new_ptr1 = allocator
            .reallocate(&mut mmu, ptr1, 5)
            .expect("Unable to reallocate 5 blocks");

        assert_eq!(new_ptr1, ptr1, "The block address shouldn't have changed");

        check_memory!(mmu, [
            address: 0,
            data: vec![1,2,3,4,0],
            Header {
                size: 6,
                state: true
            };
            address: 6,
            data: vec![0,0],
            Header {
                size: 3,
                state: true
            };
            address: 9,
            data: vec![],
            Header {
                size:0,
                state: false
            }
        ]);
    }

    #[test]
    fn reallocation_copy() {
        // create context
        // allocate 5 blocks => ptr1
        // allocate 2 blocks => ptr2
        // free ptr1
        // allocate 3 => ptr1
        // write some data at:
        //   ptr1   => 1
        //   ptr1+1 => 2
        //   ptr1+2 => 3
        // realloc 7 => ptr1

        let mut ram = Ram::new();
        let mut mmu = Mmu::new(&mut ram, None);
        mmu.add_context(1, 35).expect("Should init context");
        mmu.set_context(1);
        let allocator = Allocator::new();

        // allocate 5 blocks  => ptr1
        let ptr1 = allocator
            .allocate(&mut mmu, 5)
            .expect("Unable to allocate 5 blocks of data");

        check_memory!(mmu, [
            address: 0,
            data: vec![0,0,0,0,0,0],
            Header {
                size: 6,
                state: true
            };
            address: 6,
            data: vec![],
            Header {
                size:0,
                state: false
            }
        ]);

        // allocate 2 blocks  => ptr2
        let ptr2 = allocator
            .allocate(&mut mmu, 2)
            .expect("Unable to allocate 2 blocks of data");

        check_memory!(mmu, [
            address: 0,
            data: vec![0,0,0,0,0,0],
            Header {
                size: 6,
                state: true
            };
            address: 6,
            data: vec![0,0],
            Header {
                size: 3,
                state: true
            };
            address: 9,
            data: vec![],
            Header {
                size:0,
                state: false
            }
        ]);

        // free ptr1
        allocator
            .free(&mut mmu, &ptr1)
            .expect("Unable to free ptr1");

        check_memory!(mmu, [
            address: 0,
            data: vec![0,0,0,0,0,0],
            Header {
                size: 6,
                state: false
            };
            address: 6,
            data: vec![0,0],
            Header {
                size: 3,
                state: true
            };
            address: 9,
            data: vec![],
            Header {
                size:0,
                state: false
            }
        ]);

        // allocate 3 blocks  => ptr1
        let ptr1 = allocator
            .allocate(&mut mmu, 3)
            .expect("Unable to allocate 3 blocks of data");

        // write some data at:
        //   ptr1   => 1
        mmu.store(ptr1, 1, &Section::Heap)
            .expect("Unable to store at ptr1[0]");
        //   ptr1+1 => 2
        mmu.store(ptr1 + 1, 2, &Section::Heap)
            .expect("Unable to store at ptr1[1]");
        //   ptr1+2 => 3
        mmu.store(ptr1 + 2, 3, &Section::Heap)
            .expect("Unable to store at ptr1[2]");

        check_memory!(mmu, [
            address: 0,
            data: vec![1,2,3],
            Header {
                size: 4,
                state: true
            };
            address: 4,
            data: vec![0],
            Header {
                size: 2,
                state: false
            };
            address: 6,
            data: vec![0,0],
            Header {
                size: 3,
                state: true
            };
            address: 9,
            data: vec![],
            Header {
                size:0,
                state: false
            }
        ]);

        // realloc 7 blocks => ptr1
        let new_ptr1 = allocator
            .reallocate(&mut mmu, ptr1, 7)
            .expect("Unable to reallocate 5 blocks");

        assert_ne!(new_ptr1, ptr1, "The block address should have changed");

        check_memory!(mmu, [
            address: 0,
            data: vec![1,2,3],
            Header {
                size: 4,
                state: false
            };
            address: 4,
            data: vec![0],
            Header {
                size: 2,
                state: false
            };
            address: 6,
            data: vec![0,0],
            Header {
                size: 3,
                state: true
            };
            address: 9,
            data: vec![1,2,3,0,0,0,0,0],
            Header {
                size: 8,
                state: true
            }
        ]);

        // allocate 5 blocks  => ptr3
        let ptr3 = allocator
            .allocate(&mut mmu, 5)
            .expect("Unable to allocate 5 blocks of data");

        check_memory!(mmu, [
            address: 0,
            data: vec![1,2,3,4,0],
            Header {
                size: 6,
                state: true
            };
            address: 6,
            data: vec![0,0],
            Header {
                size: 3,
                state: true
            };
            address: 9,
            data: vec![1,2,3,0,0,0,0,0],
            Header {
                size: 8,
                state: true
            }
        ]);
    }
}
