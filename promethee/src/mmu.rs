use crate::data::constants::{RamAddressBusSize, RamDataBusSize, BLOCK_SIZE, STACK_SECTION_LENGTH};
use crate::data::memory::{Header, HeapAddress, HeapBlock};
use crate::events::{Event, HeapEvent, MmuEvent, ProcessManagement};
use crate::ram::Ram;
use crossbeam_channel::Sender;
use eyre::{eyre, Context as EyreContext, ContextCompat};
use log::{debug, trace, warn};
use nom::ToUsize;
use serde::Serialize;
use std::collections::{HashMap, VecDeque};
use std::fmt::{Debug, Formatter};

/// Represents
/// a memory block mapping
#[derive(Debug, PartialEq, Serialize, Clone)]
pub struct Block {
    physical_address: RamAddressBusSize,
    virtual_address: RamAddressBusSize,
}

impl Block {
    pub fn new(virtual_address: RamAddressBusSize, physical_address: RamAddressBusSize) -> Self {
        Block {
            physical_address,
            virtual_address,
        }
    }
}

#[derive(Debug, Clone, Serialize, PartialEq, Eq, Hash)]
#[serde(rename_all = "snake_case")]
pub enum Section {
    Stack,
    Code,
    Heap,
}

#[derive(Debug, PartialEq)]
struct Context {
    section_code: VecDeque<Block>,
    section_stack: VecDeque<Block>,
    section_heap: VecDeque<Block>,
    code_length: usize,
}

impl Context {
    pub fn into_blocks(self) -> VecDeque<Block> {
        let mut blocks = self.section_stack;
        blocks.extend(self.section_code);
        blocks
    }
}

pub struct Mmu<'a> {
    pid: Option<usize>,
    send_event: Option<Sender<Event>>,
    contexts: HashMap<usize, Context>,
    ram: &'a mut Ram,
    block_pointer: RamAddressBusSize,
    freed_blocks: VecDeque<Block>,
}

impl<'a> Debug for Mmu<'a> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.ram)
    }
}

impl<'a> Mmu<'a> {
    pub fn new(ram: &'a mut Ram, tx: Option<Sender<Event>>) -> Self {
        Mmu {
            pid: None,
            contexts: Default::default(),
            ram,
            block_pointer: 0,
            freed_blocks: Default::default(),
            send_event: tx,
        }
    }

    fn send_event(&self, event: Event) -> eyre::Result<()> {
        if let Some(sender) = self.send_event.as_ref() {
            sender.send(event).wrap_err("Unable to send event")?
        }
        Ok(())
    }

    pub fn send_heap_state(&mut self, pid: usize) -> eyre::Result<()> {
        let old_pid = self.pid;
        self.set_context(pid);

        let blocks = self.scan_heap()?;

        self.send_event(Event::Process {
            pid,
            event: ProcessManagement::Heap(HeapEvent::HeapState(blocks)),
        })?;

        self.pid = old_pid;

        Ok(())
    }

    pub fn scan_block(
        &mut self,
        header: &Header,
        block_address: RamAddressBusSize,
    ) -> eyre::Result<Vec<HeapAddress>> {
        let mut addresses = vec![];

        for i in 0..header.size - 1 {
            let addr = block_address + i as RamAddressBusSize;
            let data = self.load_or_init(&addr, &Section::Heap, 0)?;
            let address_data = HeapAddress {
                address: addr,
                data: *data,
            };
            addresses.push(address_data)
        }

        Ok(addresses)
    }

    pub fn scan_heap(&mut self) -> eyre::Result<Vec<HeapBlock>> {
        let mut block = 0;
        let mut blocks = vec![];
        let mut counter = 0;

        loop {
            let raw_header = *self.load_or_init(&block, &Section::Heap, 0)?;
            if raw_header == 0 {
                break;
            }
            let header: Header = raw_header.into();

            let addresses = self.scan_block(&header, block + 1)?;

            let heap_block = HeapBlock {
                id: counter,
                state: header.state,
                size: header.size.to_usize(),
                addresses,
            };

            blocks.push(heap_block);

            block += header.size as RamAddressBusSize;
            counter += 1;
        }
        Ok(blocks)
    }

    fn get_context(&self) -> eyre::Result<&Context> {
        self.contexts
            .get(&self.pid.ok_or(eyre!("Unable to get pid context"))?)
            .wrap_err(eyre!("Unable to get context for pid {:?}", self.pid))
    }

    fn get_mut_context(&mut self) -> eyre::Result<&mut Context> {
        self.contexts
            .get_mut(&self.pid.ok_or(eyre!("Unable to get pid context"))?)
            .wrap_err(eyre!("Unable to get context for pid {:?}", self.pid))
    }

    pub fn get_real_address(
        &self,
        address: &RamAddressBusSize,
        section_kind: &Section,
    ) -> eyre::Result<RamAddressBusSize> {
        let address = *address;
        let context = self.get_context().wrap_err("get real address")?;

        let (section, address) = match section_kind {
            Section::Stack => (&context.section_stack, address),
            Section::Code => (&context.section_code, address),
            Section::Heap => {
                let heap_size = context.section_heap.len() as RamAddressBusSize * BLOCK_SIZE;
                let virtual_address = heap_size - address - 1;
                trace!("{virtual_address}");

                (&context.section_heap, virtual_address)
            }
        };

        let index = address / BLOCK_SIZE;
        let offset = address % BLOCK_SIZE;

        let address = offset
            + section
                .get(index.to_usize())
                .ok_or(eyre!(
                    "Unable to get block #{index} for section {section_kind:?}"
                ))?
                .physical_address;

        Ok(address)
    }

    pub fn load_or_init(
        &mut self,
        address: &RamAddressBusSize,
        section: &Section,
        default: RamDataBusSize,
    ) -> eyre::Result<&RamDataBusSize> {
        if let Section::Heap = section {
            if self.get_context()?.section_heap.is_empty() {
                return Err(eyre!("Unable to read data from zero heap size"));
            }
        }

        let real = self.get_real_address(address, section)?;

        warn!("Load address with default value {address} => {real}");
        Ok(self.ram.load_or_init(&real, default))
    }

    pub fn load(
        &self,
        address: &RamAddressBusSize,
        section: &Section,
    ) -> eyre::Result<&RamDataBusSize> {
        debug!("Loading address @{address:04x} for {section:?} section");
        if let Section::Heap = section {
            if self.get_context()?.section_heap.is_empty() {
                return Err(eyre!("Unable to read data from zero heap size"));
            }
        }

        let real = self.get_real_address(address, section)?;

        warn!("Load address {address} => {real}");
        self.ram.load(&real).wrap_err(eyre!(
            "Loading section {section:?} at address {address:04x}"
        ))
    }

    pub fn store(
        &mut self,
        address: RamAddressBusSize,
        data: RamDataBusSize,
        section: &Section,
    ) -> eyre::Result<()> {
        debug!("Trying to store data {data:032b} at address @{address:04x}");

        if let Section::Heap = section {
            if self.get_context()?.section_heap.is_empty() {
                return Err(eyre!("Unable to store data into zero heap size"));
            }
        }

        self.ram
            .store(self.get_real_address(&address, &section)?, data);

        Ok(())
    }

    pub fn set_context(&mut self, pid: usize) {
        self.pid = Some(pid)
    }

    pub fn clear_context(&mut self) {
        self.pid = None
    }

    fn get_stack_section_bottom(&self) -> eyre::Result<RamAddressBusSize> {
        // Get context
        let context = self.get_context()?;
        // Get the highest stack address
        let last = context
            .section_stack
            .iter()
            .last()
            .ok_or(eyre!("The stack section can't be empty"))?;

        Ok(last.virtual_address + BLOCK_SIZE)
    }

    pub fn get_heap_size(&self) -> eyre::Result<RamAddressBusSize> {
        let context = self.get_context()?;
        Ok(context.section_heap.len() as RamAddressBusSize * BLOCK_SIZE)
    }

    fn register_blocks(
        &mut self,
        amount: usize,
        lowest_address: RamAddressBusSize,
        reverse: bool,
    ) -> eyre::Result<VecDeque<Block>> {
        let mut nb_reserved_blocks = 0;
        let mut required = amount;
        let mut blocks = VecDeque::new();

        // Try to use already defined block if any
        for _i in 0..amount {
            // Get first block available
            if let Some(mut block) = self.freed_blocks.pop_front() {
                // Update the virtual address to fit with the new section context
                block.virtual_address = lowest_address + nb_reserved_blocks * BLOCK_SIZE;
                blocks.push_back(block);
                // decrement still needed blocks
                required -= 1;
                // increment already reserved blocks
                nb_reserved_blocks += 1;
            } else {
                // as soon as all already defined blocks are exhausted
                // quit the loop
                break;
            }
        }

        // and try to reserve new ones
        for _i in 0..required {
            // Create a new block
            let block = Block::new(
                lowest_address + nb_reserved_blocks * BLOCK_SIZE,
                self.block_pointer * BLOCK_SIZE,
            );
            // increment the next block pointer in the physical memory
            self.block_pointer += 1;
            // increment already reserved blocks
            nb_reserved_blocks += 1;

            blocks.push_back(block);
        }

        if reverse {
            blocks = blocks.into_iter().rev().collect();
        }

        Ok(blocks)
    }

    pub fn add_context(&mut self, pid: usize, code_length: usize) -> eyre::Result<()> {
        let section_code_length = code_length.div_ceil(BLOCK_SIZE.to_usize());

        let section_stack = self.register_blocks(STACK_SECTION_LENGTH as usize, 0, false)?;
        let last = section_stack
            .iter()
            .last()
            .ok_or(eyre!("The stack section can't be empty"))?;
        let end_stack = last.virtual_address + BLOCK_SIZE;
        let section_code = self.register_blocks(section_code_length, end_stack, false)?;

        let context = Context {
            section_stack,
            section_code,
            section_heap: VecDeque::new(),
            code_length,
        };

        self.contexts.insert(pid, context);
        Ok(())
    }

    pub fn send_initialisation_events(&self) -> eyre::Result<()> {
        let context = self.get_context()?;

        if let Some(pid) = self.pid {
            self.send_event(Event::Process {
                pid,
                event: ProcessManagement::Mmu(MmuEvent::Reserve {
                    section: Section::Stack,
                    blocks: context.section_stack.clone(),
                }),
            })?;

            self.send_event(Event::Process {
                pid,
                event: ProcessManagement::Mmu(MmuEvent::Reserve {
                    section: Section::Code,
                    blocks: context.section_code.clone(),
                }),
            })?;
        }
        Ok(())
    }

    pub fn remove_context(&mut self, pid: usize) -> eyre::Result<()> {
        // Try to get context to free
        let context = self
            .contexts
            .remove(&pid)
            .ok_or(eyre!("Unable to remove context for pid {pid}"))?;

        // Get blocks from freed context
        let freed_context_blocks = context.into_blocks();

        // Add freed context blocks to priority list
        self.freed_blocks.extend(freed_context_blocks);

        Ok(())
    }

    pub fn grow(&mut self, amount: usize) -> eyre::Result<()> {
        debug!("Try to grow heap by an amount of {amount}");
        // Get the bottom address of the stack
        let stack_bottom = self.get_stack_section_bottom()?;

        let pid = self.pid.ok_or(eyre!("Unable to get pid context"))?;
        let blocks = self.register_blocks(amount, stack_bottom, true)?;
        let context = self.get_mut_context()?;

        // Update existing blocks with the delta
        let delta = amount as RamAddressBusSize * BLOCK_SIZE;
        context
            .section_heap
            .iter_mut()
            .for_each(|block| block.virtual_address += delta);
        context
            .section_code
            .iter_mut()
            .for_each(|block| block.virtual_address += delta);

        // Prepend existing heap's blocks with reserved blocks
        for block in blocks.into_iter() {
            context.section_heap.push_front(block)
        }
        debug!("context : {context:?}");
        let blocks_heap = context.section_heap.clone();
        let blocks_code = context.section_code.clone();

        let sections = HashMap::from([(Section::Heap, blocks_heap), (Section::Code, blocks_code)]);

        self.send_event(Event::Process {
            pid,
            event: ProcessManagement::Mmu(MmuEvent::Update { sections }),
        })?;
        Ok(())
    }

    pub fn shrink(&mut self, amount: usize) -> eyre::Result<()> {
        let context = self.get_mut_context()?;

        for _i in 0..amount {
            context
                .section_heap
                .pop_front()
                .ok_or(eyre!("Unable to shrink heap section"))?;
        }

        let delta = amount as RamAddressBusSize * BLOCK_SIZE;
        context
            .section_heap
            .iter_mut()
            .for_each(|block| block.virtual_address -= delta);
        context
            .section_code
            .iter_mut()
            .for_each(|block| block.virtual_address -= delta);
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::allocator::Allocator;
    use crate::data::memory::{HeapAddress, HeapBlock};
    use crate::mmu::{Block, Context, Mmu, Section};
    use crate::ram::Ram;
    use std::collections::{HashMap, VecDeque};

    #[test]
    fn test_update_contexts() {
        let mut ram = Ram::new();
        let mut mmu = Mmu::new(&mut ram, None);

        mmu.add_context(1, 28).expect("Must add a context");
        mmu.add_context(2, 55).expect("Must add a context");
        mmu.remove_context(1).expect("Must remove a context");
        mmu.add_context(3, 76).expect("Must add a context");

        let expected = HashMap::from([
            (
                2,
                Context {
                    section_code: VecDeque::from([Block::new(128, 288), Block::new(160, 320)]),
                    section_stack: VecDeque::from([
                        Block::new(0, 160),
                        Block::new(32, 192),
                        Block::new(64, 224),
                        Block::new(96, 256),
                    ]),
                    section_heap: VecDeque::new(),
                    code_length: 55,
                },
            ),
            (
                3,
                Context {
                    section_code: VecDeque::from([
                        Block::new(128, 128),
                        Block::new(160, 352),
                        Block::new(192, 384),
                    ]),
                    section_stack: VecDeque::from([
                        Block::new(0, 0),
                        Block::new(32, 32),
                        Block::new(64, 64),
                        Block::new(96, 96),
                    ]),
                    section_heap: VecDeque::new(),
                    code_length: 76,
                },
            ),
        ]);
        assert_eq!(mmu.contexts, expected);
    }

    #[test]
    fn test_real_address() {
        // Test flow
        // 1 - Create a context of 28 lines of code -> 4 blocks for stack and 1 for code : real 0   -> 160
        // 2 - Create a context of 55 lines of code -> 4 blocks for stack and 2 for code : real 192 -> 384
        // 3 - Remove the first context -> free 5 blocks : real 0   -> 160
        // 4 - Create a context of 76 lines of code -> 4 blocks for stack and 3 for code : 0 -> 160 + 416 -> 480

        // --------------------- 0
        //      stack (p3)
        //--------------------- 128
        //      code(p3) part 1
        //--------------------- 160
        //     stack (p2)
        //--------------------- 288
        //     code (p3)
        //--------------------- 352
        //     code(p3) part 2
        //--------------------- 416
        //     heap(p3)
        //--------------------- 448

        // env_logger::init();

        let mut ram = Ram::new();
        let mut mmu = Mmu::new(&mut ram, None);

        mmu.add_context(1, 28).expect("Must add a context");
        mmu.add_context(2, 55).expect("Must add a context");
        mmu.remove_context(1).expect("Must remove a context");
        mmu.add_context(3, 76).expect("Must add a context");

        mmu.set_context(3);
        mmu.grow(3).expect("Must grow the heap by 3");

        let result = mmu
            .get_real_address(&4, &Section::Code)
            .expect("Should get address");
        assert_eq!(result, 132);

        // the second block of code of pid 3 is after the last block of code pid 2
        let result = mmu
            .get_real_address(&33, &Section::Code)
            .expect("Should get address");
        assert_eq!(result, 353);

        mmu.set_context(2);

        let result = mmu
            .get_real_address(&7, &Section::Stack)
            .expect("Should get address");
        assert_eq!(result, 167);

        mmu.set_context(3);

        let result = mmu
            .get_real_address(&0, &Section::Heap)
            .expect("Should get address");
        assert_eq!(result, 511);

        let result = mmu
            .get_real_address(&1, &Section::Heap)
            .expect("Should get address");
        assert_eq!(result, 510);
    }

    #[test]
    fn test_reserve() {
        let mut ram = Ram::new();
        let mut mmu = Mmu::new(&mut ram, None);

        mmu.add_context(1, 28).expect("Must add a context");
        mmu.set_context(1);
        mmu.grow(2).expect("Should reserve a block");

        let expected = HashMap::from([(
            1,
            Context {
                section_code: VecDeque::from([Block::new(192, 128)]),
                section_stack: VecDeque::from([
                    Block::new(0, 0),
                    Block::new(32, 32),
                    Block::new(64, 64),
                    Block::new(96, 96),
                ]),
                section_heap: VecDeque::from([Block::new(128, 160), Block::new(160, 192)]),
                code_length: 28,
            },
        )]);
        assert_eq!(expected, mmu.contexts);

        mmu.shrink(1).expect("Should shrink heap section");

        let expected = HashMap::from([(
            1,
            Context {
                section_code: VecDeque::from([Block::new(160, 128)]),
                section_stack: VecDeque::from([
                    Block::new(0, 0),
                    Block::new(32, 32),
                    Block::new(64, 64),
                    Block::new(96, 96),
                ]),
                section_heap: VecDeque::from([Block::new(128, 192)]),
                code_length: 28,
            },
        )]);
        assert_eq!(expected, mmu.contexts);
    }

    #[test]
    fn test_grow_shrink_multiple() {
        let mut ram = Ram::new();
        let mut mmu = Mmu::new(&mut ram, None);

        mmu.add_context(1, 28).expect("Must add a context");
        mmu.set_context(1);
        mmu.grow(4).expect("Should reserve a block");

        let expected = HashMap::from([(
            1,
            Context {
                section_code: VecDeque::from([Block::new(256, 128)]),
                section_stack: VecDeque::from([
                    Block::new(0, 0),
                    Block::new(32, 32),
                    Block::new(64, 64),
                    Block::new(96, 96),
                ]),
                section_heap: VecDeque::from([
                    Block::new(128, 160),
                    Block::new(160, 192),
                    Block::new(192, 224),
                    Block::new(224, 256),
                ]),
                code_length: 28,
            },
        )]);
        assert_eq!(expected, mmu.contexts);

        mmu.shrink(3).expect("Should shrink heap section");

        let expected = HashMap::from([(
            1,
            Context {
                section_code: VecDeque::from([Block::new(160, 128)]),
                section_stack: VecDeque::from([
                    Block::new(0, 0),
                    Block::new(32, 32),
                    Block::new(64, 64),
                    Block::new(96, 96),
                ]),
                section_heap: VecDeque::from([Block::new(128, 256)]),
                code_length: 28,
            },
        )]);
        assert_eq!(expected, mmu.contexts);
    }

    fn read_data(mmu: &mut Mmu) {
        // Retrieve data from heap
        let value = mmu
            .load(&1, &Section::Stack)
            .expect("Unable to get data from stack");
        assert_eq!(value, &0b10101);

        // Retrieve data from heap
        let value = mmu
            .load(&15, &Section::Heap)
            .expect("Unable to get data from heap");
        assert_eq!(value, &0xff);

        // Read instruction from code section
        let value = mmu
            .load(&4, &Section::Code)
            .expect("Unable to get data from heap");
        assert_eq!(value, &666);
    }

    #[test]
    fn test_store_and_retrieve_at_address_0() {
        let mut ram = Ram::new();
        let mut mmu = Mmu::new(&mut ram, None);

        // Register a new context
        mmu.add_context(1, 28).expect("Must add a context");
        mmu.set_context(1);
        mmu.grow(1).expect("Must grow the heap");

        // Store code instruction
        mmu.store(0, 666, &Section::Code)
            .expect("Unable to store data in code section");

        mmu.store(0, 0b10101, &Section::Stack)
            .expect("Unable to store data in stack section");

        mmu.store(0, 0x111, &Section::Heap)
            .expect("Unable to store data in stack section");

        mmu.store(1, 0x112, &Section::Heap)
            .expect("Unable to store data in stack section");

        // Read instruction from code section
        let value = mmu
            .load(&0, &Section::Code)
            .expect("Unable to get data from code section");
        assert_eq!(value, &666);

        // Retrieve data from stack
        let value = mmu
            .load(&0, &Section::Stack)
            .expect("Unable to get data from stack");
        assert_eq!(value, &0b10101);

        // Read instruction from code section
        let value = mmu
            .load(&0, &Section::Heap)
            .expect("Unable to get data from heap");
        assert_eq!(value, &0x111);

        // Read instruction from code section
        let value = mmu
            .load(&1, &Section::Heap)
            .expect("Unable to get data from heap");
        assert_eq!(value, &0x112);
    }

    #[test]
    fn test_store_and_retrieve_resized_heap() {
        let mut ram = Ram::new();
        let mut mmu = Mmu::new(&mut ram, None);

        // Register a new context
        mmu.add_context(1, 28).expect("Must add a context");
        mmu.set_context(1);

        // Store code instruction
        mmu.store(4, 666, &Section::Code)
            .expect("Unable to store data in code section");

        mmu.store(1, 0b10101, &Section::Stack)
            .expect("Unable to store data in stack section");

        // Grow the heap
        mmu.grow(1).expect("Should reserve a block");

        // Store data into heap
        mmu.store(15, 0xff, &Section::Heap)
            .expect("Unable to store data in heap");

        // try to read data
        read_data(&mut mmu);

        // Grow the heap with 3 blocks
        mmu.grow(3).expect("Should reserve a block");

        // Retrieve data from heap
        let value = mmu
            .load(&15, &Section::Heap)
            .expect("Unable to get data from heap");
        assert_eq!(value, &0xff);

        // Read instruction from code section after heap allocation
        let value = mmu
            .load(&4, &Section::Code)
            .expect("Unable to get data from heap");
        assert_eq!(value, &666);

        mmu.shrink(2).expect("Unable to shrink by 2 the heap");

        // try to read data
        read_data(&mut mmu);

        mmu.shrink(2).expect("Unable to shrink by 2 the heap");

        let result = mmu.load(&15, &Section::Heap);
        assert!(result.is_err());
    }

    #[test]
    fn test_scan() {
        // env_logger::init();

        let mut ram = Ram::new();
        let mut mmu = Mmu::new(&mut ram, None);

        // Register a new context
        mmu.add_context(1, 28).expect("Must add a context");
        mmu.set_context(1);
        mmu.grow(4).expect("Should grow the heap");

        let allocator = Allocator::new();

        allocator
            .allocate(&mut mmu, 4)
            .expect("Should allocate block");
        let address2 = allocator
            .allocate(&mut mmu, 1)
            .expect("Should allocate block");
        allocator
            .allocate(&mut mmu, 2)
            .expect("Should allocate block");
        allocator
            .free(&mut mmu, &address2)
            .expect("Should free block");

        let expected = vec![
            HeapBlock {
                id: 0,
                state: true,
                size: 5,
                addresses: vec![
                    HeapAddress {
                        address: 1,
                        data: 00000000_00000000_00000000_00000000,
                    },
                    HeapAddress {
                        address: 2,
                        data: 00000000_00000000_00000000_00000000,
                    },
                    HeapAddress {
                        address: 3,
                        data: 00000000_00000000_00000000_00000000,
                    },
                    HeapAddress {
                        address: 4,
                        data: 00000000_00000000_00000000_00000000,
                    },
                ],
            },
            HeapBlock {
                id: 1,
                state: false,
                size: 2,
                addresses: vec![HeapAddress {
                    address: 6,
                    data: 00000000_00000000_00000000_00000000,
                }],
            },
            HeapBlock {
                id: 2,
                state: true,
                size: 3,
                addresses: vec![
                    HeapAddress {
                        address: 8,
                        data: 00000000_00000000_00000000_00000000,
                    },
                    HeapAddress {
                        address: 9,
                        data: 00000000_00000000_00000000_00000000,
                    },
                ],
            },
        ];

        let result = mmu.scan_heap().expect("Should scan the heap");
        assert_eq!(expected, result);
    }
}
