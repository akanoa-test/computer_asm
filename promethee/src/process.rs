use crate::cpu::Flags;
use crate::data::constants::{RamAddressBusSize, RamDataBusSize, NUMBER_OF_REGISTERS};
use crate::data::instruction::Instruction;
use serde::Serialize;
use std::collections::{HashMap, HashSet};
use std::fmt::{Debug, Formatter};

enum ProcessusState {
    Start,
    Pause,
    Finished,
}

pub struct Process {
    pub(crate) registers: [RamDataBusSize; NUMBER_OF_REGISTERS],
    pub(crate) flags: Flags,
    pub(crate) execution_pointer: RamAddressBusSize,
    pub(crate) name: String,
    pub(crate) pid: usize,
    pub(crate) breakpoints: HashSet<RamAddressBusSize>,
}

impl Debug for Process {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let n = self.name.to_string();
        let b = format!("Execution pointer : {}", self.execution_pointer);
        let c = format!("Breakpoints: {:?}", self.breakpoints);

        write!(f, "\n{n}\n  {b}\n  {c}\n")
    }
}

#[derive(Serialize, Clone, Debug)]
pub struct Marker {
    start: usize,
    end: usize,
}

#[derive(Serialize, Clone, Debug)]
pub struct ProcessDebugData {
    lines: Vec<String>,
    markers: HashMap<usize, Marker>,
    program_identifier: String,
}

pub fn define_debug_symbols(instructions: &[Instruction], name: &str) -> ProcessDebugData {
    let mut markers: HashMap<usize, Marker> = HashMap::new();
    let mut lines: Vec<String> = vec![];
    let mut cursor_postion = 0;

    for (index, instruction) in instructions.iter().enumerate() {
        let str_instruction = instruction.to_string();
        let marker = Marker {
            start: cursor_postion,
            end: cursor_postion + str_instruction.len() + 1,
        };
        cursor_postion = marker.end;
        markers.insert(index + 1, marker);
        lines.push(str_instruction)
    }

    ProcessDebugData {
        markers,
        lines,
        program_identifier: name.to_string(),
    }
}
