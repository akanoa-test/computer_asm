// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

use crossbeam_channel::{unbounded, Sender};
use futures_lite::StreamExt;
use once_cell::sync::OnceCell;
use serde_json::json;
use std::collections::HashSet;
use std::pin::pin;
use tauri::Manager;

use promethee::commands::{Command, ExecutionCommand};
use promethee::computer::Computer;
use promethee::data::constants::RamAddressBusSize;
use promethee::events::{
    Event, HeapEvent, MmuEvent, PeripheralEvent, ProcessManagement, ProgramManagement, StackEvent,
};

static SENDER: OnceCell<Sender<Command>> = OnceCell::new();

#[tauri::command]
fn set_frequency(frequency: usize) {
    send_command(Command::SetFrequency(frequency))
}

#[tauri::command]
fn pause() {
    send_command(Command::Execution {
        pid: 0,
        command: ExecutionCommand::Pause,
    })
}

#[tauri::command]
fn resume() {
    send_command(Command::Execution {
        pid: 0,
        command: ExecutionCommand::Resume,
    })
}

#[tauri::command]
fn stop(pid: usize) {
    send_command(Command::Execution {
        pid,
        command: ExecutionCommand::Stop,
    })
}

#[tauri::command]
fn restart(name: String) {
    send_command(Command::Execution {
        pid: 0,
        command: ExecutionCommand::Restart(name),
    })
}

#[tauri::command]
fn get_heap_content(pid: usize) {
    send_command(Command::HeapContent(pid))
}

#[tauri::command]
fn update_breakpoints(breakpoints: HashSet<RamAddressBusSize>, pid: usize) {
    send_command(Command::UpdateBreakpoints { pid, breakpoints })
}

#[tauri::command]
fn run_program(uuid: String) {
    send_command(Command::RunProgram { uuid })
}

#[tauri::command]
fn update_program(uuid: String, sources: String) {
    send_command(Command::UpdateProgram { uuid, sources })
}

fn send_command(command: Command) {
    let _ = SENDER.get().expect("Unable to get sender").send(command);
}

macro_rules! with_pid {
    ($event: expr, $pid: expr) => {
        json!({"event" : $event, "pid": $pid})

    };
}

fn send_event<R: tauri::Runtime>(event: Event, manager: &impl Manager<R>) -> tauri::Result<()> {
    match &event {
        event @ Event::Tick(_) => manager.emit_all(event.clone().as_ref(), event),
        Event::SwitchContext(pid) => manager.emit_all(
            "switch-context",
            json!({
                "pid": pid
            }),
        ),
        Event::Peripheral(peripheral_event) => match peripheral_event {
            PeripheralEvent::RegisterUpdate { register, value } => manager.emit_all(
                "disk-drive_register-update",
                json!({
                    "register": register,
                    "value": value
                }),
            ),
            PeripheralEvent::DiskWrite { address, data } => manager.emit_all(
                "disk-drive_write-data",
                json!({
                    "address": address,
                    "data": data
                }),
            ),
        },
        Event::Program(program_event) => match program_event {
            ProgramManagement::Init { programs } => manager.emit_all("programs-init", programs),
        },
        Event::Process {
            pid,
            event: process_event,
        } => match process_event {
            ProcessManagement::Add(process_add_event) => {
                manager.emit_all("process-add", with_pid!(process_add_event, pid))
            }
            ProcessManagement::Execution(execution_pointer) => {
                manager.emit_all("process-execution", with_pid!(execution_pointer, pid))
            }
            ProcessManagement::Flags { negative, zero } => manager.emit_all(
                "update-flags",
                with_pid!(
                    json!({
                        "zero": zero,
                        "negative": negative
                    }),
                    pid
                ),
            ),
            ProcessManagement::Killed => manager.emit_all("process-killed", with_pid!({}, pid)),
            ProcessManagement::Stack(stack_event) => match stack_event {
                StackEvent::Push(value) => manager.emit_all("stack-push", with_pid!(value, pid)),
                StackEvent::Pop => manager.emit_all("stack-pop", with_pid!((), pid)),
                StackEvent::Update { index, value } => manager.emit_all(
                    "stack-update",
                    with_pid!(
                        json!({
                            "index": index,
                            "value": value
                        }),
                        pid
                    ),
                ),
            },
            ProcessManagement::Register { register, value } => manager.emit_all(
                "register",
                with_pid!(
                    json!({
                        "register": register,
                        "value": value
                    }),
                    pid
                ),
            ),
            ProcessManagement::Mmu(mmu_event) => match mmu_event {
                MmuEvent::Reserve { section, blocks } => manager.emit_all(
                    "mmu-reserve",
                    with_pid!(
                        json!({
                            "section": section,
                            "blocks": blocks
                        }),
                        pid
                    ),
                ),
                MmuEvent::Update { sections } => manager.emit_all(
                    "mmu-update-sections",
                    with_pid!(
                        json!({
                            "sections": sections
                        }),
                        pid
                    ),
                ),
            },
            ProcessManagement::Heap(heap_event) => match heap_event {
                HeapEvent::HeapState(state) => manager.emit_all(
                    "heap-update",
                    with_pid!(
                        json!({
                            "blocks" : state
                        }),
                        pid
                    ),
                ),
            },
        },
    }
}

fn main() {
    env_logger::init();

    tauri::Builder::default()
        .invoke_handler(tauri::generate_handler![
            set_frequency,
            pause,
            resume,
            stop,
            restart,
            get_heap_content,
            update_breakpoints,
            run_program,
            update_program,
        ])
        .setup(|app| {
            #[cfg(debug_assertions)] // n'incluez ce code que sur les versions de débogage
            {
                let window = app.get_window("main").unwrap();
                //window.open_devtools();
                //window.close_devtools();
            }

            let app_handle = app.handle();

            let (send_command_to_computer, receive_command_from_backend) = unbounded::<Command>();

            SENDER
                .set(send_command_to_computer)
                .expect("Unable to set sender");

            let (mut computer, join_handle) = Computer::new(receive_command_from_backend);

            tauri::async_runtime::spawn(async move {
                let stream = computer.run();
                let mut stream = pin!(stream);

                while let Some(event) = stream.next().await {
                    let _ = send_event(event, &app_handle);
                }

                let a = join_handle.join();
                match a {
                    Ok(Err(err)) => {
                        println!("{:#}", err)
                    }
                    _ => {}
                }
            });

            Ok(())
        })
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
