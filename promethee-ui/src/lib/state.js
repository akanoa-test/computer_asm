import {ref} from "vue";
import {listen} from "@tauri-apps/api/event";

let current_pid = ref(null);

await listen("switch-context", event => {
    current_pid.value = event.payload.pid
})

export {
    current_pid
}