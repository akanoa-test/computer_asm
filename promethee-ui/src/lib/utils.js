export const sortObject = unordered => {
    return Object.keys(unordered).sort().reduce((obj, key) => {
        obj[key] = unordered[key];
        return obj
    }, {})
}

export const sortPrograms = unordered => {
    return Object.entries(unordered).sort(([uuid_1, program_1], [uuid_2, program_2]) => {
        let program_1_data = Object.assign({}, program_1);
        let program_2_data = Object.assign({}, program_2);
        let comparator = new Intl.Collator('fr', {numeric: true});
        return comparator.compare(program_1_data.name, program_2_data.name)
    })
}

export const guard = (my_pid, raw_event, f) => {
    let {pid, event} = raw_event.payload;

    if (parseInt(my_pid) !== pid) {
        return
    }

    return f(event)
}