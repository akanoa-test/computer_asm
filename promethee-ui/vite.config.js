import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import babel from 'vite-plugin-babel';

// https://vitejs.dev/config/
export default defineConfig(async () => ({
  plugins: [vue(), babel({
    babelConfig: {
      babelrc: false,
      configFile: false,
      plugins: [
        [
          "@babel/plugin-proposal-decorators",
          { loose: true, version: "2022-03" },
        ],
      ],
    },
  }),],

  // Vite options tailored for Tauri development and only applied in `tauri dev` or `tauri build`
  //
  // 1. prevent vite from obscuring rust errors
  clearScreen: false,
  // 2. tauri expects a fixed port, fail if that port is not available
  server: {
    port: 1420,
    strictPort: true,
  },
  // 3. to make use of `TAURI_DEBUG` and other env variables
  // https://tauri.studio/v1/api/config#buildconfig.beforedevcommand
  envPrefix: ["VITE_", "TAURI_"],
}));
