/*
fn main() {
    a = 1;
    z = 0xff;
    b = f1(a);
    c = a + b;
    return;
}

fn f1(a) {
    b = 2;
    f2();
    f2();
    return a + b;
}

fn f2() {
    a = 0x42;
    b = 0x12;
}

*/
    MOV R1, 1         // a = 1
    MOV R2, 0xff      // z = 0xff
    MOV R10, R1       // set a as f1 param

    CALL 'f1, R1, R2  // f1(a)

    MOV R3, R15       // b = f1(a)
    MOV ACC, R1
    ADD R3            // c = a + b
    HLT
f1: MOV ACC, 2        // b = 2
    MOV R1, 0xff      // side effect !!
    MOV R2, 0         // side effect !!

    CALL 'f2, R1, R2, ACC
    CALL 'f2, R1, R2, ACC

    ADD R10           // a + b
    MOV R14, 0x01     // direct value
    MOV R15, ACC      // return a + b
    RET
f2: MOV R1, 0x42
    MOV R2, 0x12
    RET