/*
    write 0x1 at address @666 in write mode High Bits
*/
    MOV R2, 666
    OUT R2, #0x00_01
    MOV R2, 0x1
    OUT R2, #0x00_02
    MOV R2, 0b110
    OUT R2, #0x00_00
    STP
