/*
fn main() {
    a = 100
    f(&a) // a = 77
}

fn f(&a) {
   b = 12
   *a -= 11 - b
}
*/
    MOV R1, 100         // a = 100
    MOV R10, SP
    CALL 'f, R1         // f(&a)
    HLT                 // a = 77
f:  MOV R2, 12          // b = 12
    LOD ACC, @R10 // ref_a = 100
    SUB 11              // ref_a -= 11
    SUB R2              // ref_a -= b
    STD ACC, @R10  // *a = ref_a
    RET