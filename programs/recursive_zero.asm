/*
fn main() {
    a = 3;
    f(a);
}

fn f(a) {
  if a == 0 {
    return
  }
  f(a - 1)
}
*/
     MOV R10, 3
     CALL 'f
     HLT
f:   JMZ R10, 'end
     MOV ACC, R10
     MOV R1, 1
     SUB R1
     MOV R10, ACC
     CALL 'f
end: RET