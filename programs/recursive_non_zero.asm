/*
fn main() {
    a = 3;
    f(a);
}

fn f(a) {
  if a == 0 {
    return
  }
  f(a - 1)
}
*/
     MOV R10, 3
     CALL 'f
     MOV R1, 0xff
     STP
f:   JNZ R10, 'rec
     RET
rec: MOV ACC, R10
     SUB 1
     MOV R10, ACC
     CALL 'f